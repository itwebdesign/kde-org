---
title: "XPDF Integer Overflow"
date: 2005-10-12 19:17:55 +0200
---

KOffice 1.3 (including betas) to 1.3.4 have an integer overflow vulnerability in KWord's PDF import filter.

## References

+ <a href="http://kde.org/info/security/advisory-20041021-1.txt"> the corresponding security advisiory for KDE.</a>

## How to fix source code?

### KOffice 1.3 (including betas) to 1.3.3

<a href="http://www.koffice.org/releases/security/xpdf_security_integer_overflow.diff">
    A patch for the source package is available.
</a> (Patch updated the 2004-10-30 16:15 UTC)

(MD5 sum: <tt>b681bc6746c31f3410f20315b0075b25</tt> xpdf_security_integer_overflow.diff)

The patch applies to the directory koffice/filters/kword/pdf/xpdf/xpdf.

To patch, do: (please adjust paths)

```
cd koffice/filters/kword/pdf/xpdf/xpdf<br />
patch -p0 &lt; xpdf_security_integer_overflow.diff
```


### KOffice 1.3.4

KOffice 1.3.4 has an integer overflow vulnerability fix in KWord's PDF import filter
which is weak against compiler optimization.

<a href="http://www.koffice.org/releases/security/koffice_1_3_4_xpdf_security_integer_overflow.diff">
A patch for the source package is available.
</a>

The patch applies to the directory koffice/filters/kword/pdf/xpdf/xpdf .

To patch, do: (please adjust paths)

```
cd koffice/filters/kword/pdf/xpdf/xpdf<br />
patch -p0 &lt; koffice_1_3_4_xpdf_security_integer_overflow.diff
```

## See Also

+ <a href="2004_xpdf_integer_overflow_2.php">The second xpdf integer overflow vulnerability</a>
