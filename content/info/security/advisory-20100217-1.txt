KDE Security Advisory: KRunner lock module race condition
Original Release Date: 2010-02-17
URL: http://www.kde.org/info/security/advisory-2010-02-17-1.txt

0. References
        oCert: N/A
        CVE: N/A

1. Systems affected:

        KDE 4.4.0 is the only version affected.

2. Overview:

        KRunner lock module race condition:
        The KRunner lock module forks a process to perform password
        validation. This forking process does not properly check whether an
        existing process has already been forked, creating a race condition if
        the Enter key is pressed after a process has been forked but before
        the password has been validated. In this case, multiple processes can
        be forked, causing a socket listener on the parent to point to either
        an incorrect process that has not received the password data,
        resulting in a hang, or in some cases cause the socket notifier to
        point to an already-deleted object, resulting in a crash that brings
        the user to the desktop and circumvents the lock mechanism.

        Note: the locking is actually performed by KRunner, which is part of
        the kdebase module, and not KScreenSaver; even when no screen saver
        is being used an affected system can still be compromised.

3. Impact:

        This is a critical vulnerability that is easily exploited if physical
        access to a machine is granted, and can result in an attacker gaining
        full privileges of the user whose screen is currently locked.

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. OS vendors / binary package providers have been
        contacted and in most cases a patched version should already be
        available. Contact your OS vendor / binary package provider for
        information about how to obtain updated binary packages or for
        information about which version of the package contains the patch.

5. Patch:

        Patches for KDE 4.4.0 have been committed to Subversion, and are
        available from websvn.kde.org:
        http://websvn.kde.org/?view=rev&revision=1089241

        A patch may also be obtained directly from the Subversion repository
        (no checkout needed) with the following command:
        svn diff -r 1089240:1089241 \
svn://anonsvn.kde.org/home/kde/branches/KDE/4.4/kdebase/workspace/krunner/lock

