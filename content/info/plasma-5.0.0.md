---
version: "5.0.0"
title: "Plasma 5.0.0 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.0_Errata
    name: 5.0 Errata
type: info/plasma5
---


This is a major release of Plasma, for new features read the [Plasma 5.0 announcement](/announcements/plasma5.0/).
