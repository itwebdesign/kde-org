---
version: "5.3.95"
title: "Plasma 5.3.95 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.4_Errata
    name: 5.4 Errata
type: info/plasma5
unstable: true
---

This is a beta release of Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the <a
href="/announcements/plasma-5.3.95">Plasma 5.3.95 announcement</a>.</p>
