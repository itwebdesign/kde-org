---
title: KDE Plasma 5.6.4, bugfix Release for May
release: "plasma-5.6.4"
description: KDE Ships Plasma 5.6.4.
date: 2016-05-10
layout: plasma
changelog: plasma-5.6.3-5.6.4-changelog
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma-5.6/plasma-5.6.png" alt="KDE Plasma 5.6 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.6">}}

Tuesday, 10 May 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.6.4. <a href='https://www.kde.org/announcements/plasma-5.6.0.php'>Plasma 5.6</a> was released in March with many feature refinements and new modules to complete the desktop experience.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Make sure kcrash is initialized for discover. <a href="http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=63879411befc50bfd382d014ca2efa2cd63e0811">Commit.</a>
- Build Breeze Plymouth and Breeze Grub tars from correct branch
- [digital-clock] Fix display of seconds with certain locales. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a7a22de14c360fa5c975e0bae30fc22e4cd7cc43">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/127623">#127623</a>
