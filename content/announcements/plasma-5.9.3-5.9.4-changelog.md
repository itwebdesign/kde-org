---
title: Plasma 5.9.4 Complete Changelog
version: 5.9.4
hidden: true
plasma: true
type: fulllog
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Simplify the ReviewDelegate. <a href='https://commits.kde.org/discover/dd946b4c0996162b7f108c0ad24ec21ea4e148d9'>Commit.</a>
- Fix initialization of the ApplicationPage. <a href='https://commits.kde.org/discover/1348200185aa7f19451614e33e2308b431bfecf2'>Commit.</a>
- Adapt to API changes in Kirigami 2.0. <a href='https://commits.kde.org/discover/7aa5c5640b411a4544875b4c782e68f1ad499c2a'>Commit.</a>
- Use correct reference to the review model. <a href='https://commits.kde.org/discover/e1c5ddf7f0028c0f21f990f2c8af91d79c562bbb'>Commit.</a>
- Minor improvements to review delegate. <a href='https://commits.kde.org/discover/9ee288b1f772df4912a2f379a1a1d0cf04cbca0b'>Commit.</a>
- Define variable. <a href='https://commits.kde.org/discover/717b1caa185ed79b0f2f619289369898fa7931f7'>Commit.</a>
- Make sure we don't call cleaned up streams. <a href='https://commits.kde.org/discover/cb2f989331684e80369991e753b756e070bc1337'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376162'>#376162</a>
- Fix enabling/disabling sources. <a href='https://commits.kde.org/discover/7bdaa6d2f478be5422d4ef002518f2eabb1961dc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377327'>#377327</a>
- Account for requestComments sometimes returning a nullptr. <a href='https://commits.kde.org/discover/1cb2ba38a6e843502e582e4d8f4df5b9cd702e56'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377314'>#377314</a>
- Fix source removal. <a href='https://commits.kde.org/discover/96abf86c62945e004652eff55c5e435a9e4b531e'>Commit.</a>

### <a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a>

- Fix merge. <a href='https://commits.kde.org/kscreenlocker/b3c23f1088013b3c67ebf42c390c0f2864889e41'>Commit.</a>
- Implement manual focus on click. <a href='https://commits.kde.org/kscreenlocker/f8043de10b5dd94b9b931a92f3aa7167188786c9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348789'>#348789</a>. Fixes bug <a href='https://bugs.kde.org/374289'>#374289</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4821'>D4821</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Move the view at the correct index at startup. <a href='https://commits.kde.org/kwin/892f398b10abd5bc1c3105e4b917cc2c7b396990'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4703'>D4703</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [Kicker] Fix highlighting favorites. <a href='https://commits.kde.org/plasma-desktop/db297ab5acb93f88c238778e8682effe3032bf4f'>Commit.</a> See bug <a href='https://bugs.kde.org/377652'>#377652</a>. Phabricator Code review <a href='https://phabricator.kde.org/D5064'>D5064</a>
- Fix invalid reference of toolTipArea. <a href='https://commits.kde.org/plasma-desktop/2f5dbfbc15608f083898b0a690a565a5e4d467df'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4996'>D4996</a>
- [Folder View] Explicitly set prefix as empty in normal state. <a href='https://commits.kde.org/plasma-desktop/b7ce1a4bc1d050ee03837bac77d1a31c3b84c175'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377441'>#377441</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4998'>D4998</a>
- Do apply margin if in right-to-left mode. <a href='https://commits.kde.org/plasma-desktop/d37b57dae09d1d9d0e2b26101514ad216139c063'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376529'>#376529</a>
- [AppletAppearance] Silence warning. <a href='https://commits.kde.org/plasma-desktop/02ccf21432f58836df72105a39aa0db2287b62cc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4854'>D4854</a>
- [Folder View] Fix action button hover and pressed state. <a href='https://commits.kde.org/plasma-desktop/5b9985501cb67107bd329a04b6558397ba0b78ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4915'>D4915</a>
- Disable editing of 'Global Shortcuts' component names. <a href='https://commits.kde.org/plasma-desktop/cafc7d5a448a19047762af3eb0d5e5a495ef58ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376935'>#376935</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4905'>D4905</a>
- Remove accidentally commited .directory file. <a href='https://commits.kde.org/plasma-desktop/d34275c934ed465557918578ea0f641be3f6f8da'>Commit.</a>
- Fix switching categories via the filter listview on touchscreens. <a href='https://commits.kde.org/plasma-desktop/d85dd61627bfa2e2e906fddb16d7a9bee71375f8'>Commit.</a>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

- [QDBusMenuBar] Connect to popupRequested signal. <a href='https://commits.kde.org/plasma-integration/aef74e97e2ed462a3f23d8773452b3d49230050f'>Commit.</a> See bug <a href='https://bugs.kde.org/376726'>#376726</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4955'>D4955</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- WiredSetting: Speed has to be set when auto-negotiation is off. <a href='https://commits.kde.org/plasma-nm/427320a5d629022d2e2e228fc6f63a7f279e2b5a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376018'>#376018</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Do not add debugger button unless it is meant to be visible. <a href='https://commits.kde.org/plasma-workspace/f99d50a82028a6a58aff5e544a24e81c00186c34'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5080'>D5080</a>
- Manage mouse events when out of the window. <a href='https://commits.kde.org/plasma-workspace/7f2dceb95c5812207c87f4b61d5a90e0306603d6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377545'>#377545</a>
- Don't emit availableScreenRectChanged when quitting. <a href='https://commits.kde.org/plasma-workspace/721fcd3d309171fde2dc37b84939f7657db91de3'>Commit.</a>
- Try proper order of corona()->availableScreenRect(). <a href='https://commits.kde.org/plasma-workspace/bcb6b86d3ab8bd196877473af343b72c53e4f932'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377298'>#377298</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4991'>D4991</a>
- [Notifications] Keep popup opened during drag. <a href='https://commits.kde.org/plasma-workspace/a2685f717e89ae562587118ccf89a92801eac6a0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4976'>D4976</a>
- Ignore NoDisplay=true .desktop entries when matching by Name. <a href='https://commits.kde.org/plasma-workspace/8028a500e3b21ec96751b73e402df2f5be459e9e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4928'>D4928</a>
- Preview Centered wallpaper as PreserveAspectFit. <a href='https://commits.kde.org/plasma-workspace/b6d088d9e6e7239d503cfc9eb281458956877e15'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375675'>#375675</a>. Phabricator Code review <a href='https://phabricator.kde.org/D4840'>D4840</a>
- Only activate kded Appmenu signals if menus are in the window decoration. <a href='https://commits.kde.org/plasma-workspace/907296e8d30b1cc61353b9c03a1971166ac956fc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4833'>D4833</a>
- Open the correct submenu in kwin menu when activating the Appmenu with a shortcut. <a href='https://commits.kde.org/plasma-workspace/9343bc2bb835374997ad1779be7d117f645b573d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4832'>D4832</a>
- Try harder to make the selected wallpaper visible. <a href='https://commits.kde.org/plasma-workspace/ff602030827dcada48f10693970c020e14369e11'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D4648'>D4648</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Set the correct desktop file name when in a KDE session. <a href='https://commits.kde.org/systemsettings/f61f9d8c100fe94471b1a8f23ac905e9311b7436'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D5006'>D5006</a>
