---
title: KDE Plasma 5.12.7, Bugfix Release for September
release: plasma-5.12.7
version: 5.12.7
description: KDE Ships 5.12.7
date: 2018-09-25
changelog: plasma-5.12.6-5.12.7-changelog
---

{{%youtube id="xha6DJ_v1E4"%}}

{{<figure src="/announcements/plasma-5.12/plasma-5.12.png" alt="KDE Plasma 5.12 LTS Beta " class="text-center" width="600px" caption="KDE Plasma 5.12 LTS Beta">}}

Tuesday, 25 September 2018.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.12.7." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.12" "February" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "three months" %}}

- Prevent paste in screen locker. <a href="https://commits.kde.org/kscreenlocker/1638db3fefcae76f27f889b3709521b608aa67ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/388049">#388049</a>. Phabricator Code review <a href="https://phabricator.kde.org/D14924">D14924</a>
- Improve arrow key navigation of Kicker search results. <a href="https://commits.kde.org/plasma-desktop/1692ae244bc5229df78df2d5ba2e76418362cb50">Commit.</a> Fixes bug <a href="https://bugs.kde.org/397779">#397779</a>. Phabricator Code review <a href="https://phabricator.kde.org/D15286">D15286</a>
- Fix QFileDialog not remembering the last visited directory. <a href="https://commits.kde.org/plasma-integration/4848bec177b2527662098f97aa745bb839bc15e3">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D14437">D14437</a>
