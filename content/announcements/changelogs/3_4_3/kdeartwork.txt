2005-07-24 13:45 +0000 [r438142]  mlaurent

	* branches/KDE/3.4/kdeartwork/IconThemes/kdeclassic/16x16/apps/kig.png:
	  Fix it. a png file is not a html file. => use kig.png from kde3.5
	  branch

2005-08-31 19:36 +0000 [r455542]  mueller

	* branches/KDE/3.4/kdeartwork/kscreensaver/kdesavers/rotation.h,
	  branches/KDE/3.4/kdeartwork/kscreensaver/kdesavers/sspreviewarea.h,
	  branches/KDE/3.4/kdeartwork/kscreensaver/kdesavers/pendulum.h:
	  fixing compilitatation

2005-10-02 18:27 +0000 [r466461]  coolo

	* branches/KDE/3.4/kdeartwork/kscreensaver/kdesavers/Makefile.am:
	  backporting Makefile fix

2005-10-05 13:46 +0000 [r467513]  coolo

	* branches/KDE/3.4/kdeartwork/kdeartwork.lsm: 3.4.3

