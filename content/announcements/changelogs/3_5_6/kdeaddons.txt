2006-10-09 00:13 +0000 [r593766]  tyrerj

	* branches/KDE/3.5/kdeaddons/konq-plugins/webarchiver/plugin_webarchiver.cpp:
	  BUG:135130 Changing default save location to: "documentPath()"

2006-10-16 22:56 +0000 [r596240]  jriddell

	* branches/KDE/3.5/kdeaddons/konq-plugins/arkplugin/arkplugin.cpp:
	  Fix 7zip support by Anthony Mercatante <tonio@ubuntu.com>

2006-11-02 15:35 +0000 [r601222]  mlaurent

	* branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kdirmenu.cpp:
	  Display folder icons

2006-11-05 18:51 +0000 [r602351-602350]  mkoller

	* branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kdirmenu.cpp: Init
	  the QMap so that the CICON macro does not crash

	* branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kcmkuick/kcmkuickdialog.ui,
	  branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kcmkuick/kcmkuick.cpp,
	  branches/KDE/3.5/kdeaddons/konq-plugins/kuick/kmetamenu.cpp: BUG:
	  129524 - Use correct config group to get/set user defined max
	  entries in popup menu - Increase the maximum number defineable to
	  20 (instead 6)

2006-11-10 11:52 +0000 [r603819]  osterfeld

	* branches/KDE/3.5/kdeaddons/konq-plugins/akregator/konqfeedicon.cpp:
	  Do not crash when the statusbar is deleted before the feed icon
	  plugin BUG:134929

2006-11-25 21:00 +0000 [r607813]  mkoller

	* branches/KDE/3.5/kdeaddons/kfile-plugins/html/kfile_html.cpp:
	  BUG: 100126 Handle charset definition to correctly display the
	  HTML page title

2007-01-06 16:08 +0000 [r620556]  mkoller

	* branches/KDE/3.5/kdeaddons/konq-plugins/kimgalleryplugin/imgalleryplugin.cpp:
	  BUG: 132407 Don't trim filename extensions to avoid filename
	  clash with two files having the same name but different filetypes
	  (extension) Patch provided by Olivier Trichet

2007-01-06 18:55 +0000 [r620617]  mkoller

	* branches/KDE/3.5/kdeaddons/renamedlgplugins/audio/audiopreview.cpp:
	  BUG: 106069 make sure that long meta-info is squeezed by using
	  KSqueezedTextLabel

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

