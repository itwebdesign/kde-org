------------------------------------------------------------------------
r877981 | rjarosz | 2008-10-30 21:20:02 +0000 (Thu, 30 Oct 2008) | 4 lines

Backport commit 877980.
Move idle out of the debug ifdef


------------------------------------------------------------------------
r877993 | gkiagia | 2008-10-30 22:07:04 +0000 (Thu, 30 Oct 2008) | 5 lines

Backport r877986.
Ignore msn error code 712. It's very annoying to see a modal messagebox
every once in a while saying "unhandled msn error code 712".
CCBUG: 168545

------------------------------------------------------------------------
r880256 | mueller | 2008-11-05 00:53:00 +0000 (Wed, 05 Nov 2008) | 3 lines

fix linking. this was failing for several months already
in the KDE dashboard

------------------------------------------------------------------------
r881699 | rjarosz | 2008-11-08 19:33:11 +0000 (Sat, 08 Nov 2008) | 6 lines

Revert commit 869875 and backport correct fix for crash during KDE logout.
Please test if the crash is gone as I can't reproduce it here.

CCBUG: 172011


------------------------------------------------------------------------
r881947 | uwolfer | 2008-11-09 12:27:01 +0000 (Sun, 09 Nov 2008) | 5 lines

Backport:
SVN commit 881945 by uwolfer:

Allow KRDC to quit at system logout.
CCBUG:173860
------------------------------------------------------------------------
r882759 | casanova | 2008-11-11 12:06:25 +0000 (Tue, 11 Nov 2008) | 3 lines

 * Don't close file descriptor in kDebug().
CCBUG:172043

------------------------------------------------------------------------
r883106 | scripty | 2008-11-12 08:14:24 +0000 (Wed, 12 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r883616 | pino | 2008-11-13 11:27:54 +0000 (Thu, 13 Nov 2008) | 2 lines

backport: remove a space to be the same as defined in the .desktop of the plugin

------------------------------------------------------------------------
r884076 | scripty | 2008-11-14 07:30:40 +0000 (Fri, 14 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r889151 | scripty | 2008-11-26 08:15:03 +0000 (Wed, 26 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r891885 | scripty | 2008-12-03 08:22:24 +0000 (Wed, 03 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r892741 | scripty | 2008-12-05 07:38:27 +0000 (Fri, 05 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r892988 | wstephens | 2008-12-05 17:12:29 +0000 (Fri, 05 Dec 2008) | 3 lines

Backport r892985 (Fix kopete toolbar location save/restore)
CCBUG:155853

------------------------------------------------------------------------
r893180 | scripty | 2008-12-06 08:26:19 +0000 (Sat, 06 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r893691 | uwolfer | 2008-12-07 09:45:22 +0000 (Sun, 07 Dec 2008) | 5 lines

Backport:
SVN commit 893689 by uwolfer:

Set more reasonable minimum, maximum and steps in network settings spinboxes.
CCBUG:176840
------------------------------------------------------------------------
r893698 | uwolfer | 2008-12-07 10:10:49 +0000 (Sun, 07 Dec 2008) | 1 line

SVN_SILENT: Fix broken backport.
------------------------------------------------------------------------
r893723 | rjarosz | 2008-12-07 11:20:15 +0000 (Sun, 07 Dec 2008) | 5 lines

Backport fix for bug 175532 Outgoing AIM messages have HTML line break prepended.

CCBUG: 175532


------------------------------------------------------------------------
r893734 | rjarosz | 2008-12-07 12:05:52 +0000 (Sun, 07 Dec 2008) | 6 lines

Backport fix for bug 174390: kopete crashes while opening the configuration dialog.
Not tested but should work, if anybody can test it please do.

CCBUG: 174390


------------------------------------------------------------------------
r901622 | rjarosz | 2008-12-26 10:50:17 +0000 (Fri, 26 Dec 2008) | 4 lines

Backport fix for bug 175566 Wrong russian encoding (windows-1251) in incoming messages
CCBUG: 175566


------------------------------------------------------------------------
r902813 | rjarosz | 2008-12-29 09:51:36 +0000 (Mon, 29 Dec 2008) | 6 lines

Backport commit 902518.
Fix status restoring.

CCBUG: 174527


------------------------------------------------------------------------
r904388 | scripty | 2009-01-02 07:49:10 +0000 (Fri, 02 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r906468 | mueller | 2009-01-06 11:48:52 +0000 (Tue, 06 Jan 2009) | 2 lines

bump version for KDE 4.1.4

------------------------------------------------------------------------
