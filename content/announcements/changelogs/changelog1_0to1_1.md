---
title: "KDE 1.0 to 1.1 Changelog"
hidden: true
---

<CENTER>
  <TABLE CELLSPACING=0 CELLPADDING=0 WIDTH="570" >
    <tr>
      <td> <font size="+1"><i>Stephan Kulow</i>, Master of the CVS,
    wearing <i>ad interim</i> the togue of the Master of the Great Release,
    announced today, <b><i>6 February 1999 at 21h17 (CET)</i></b>, that
    the <b>latest and greatest KDE version, KDE-1.1,</b> is released:
    <i>"OK, I tagged</i> [the CVS repository] <i>with 1_1_RELEASE and put
    that tagged version on upload.kde.org"</i>. Wait for
    <a href="ftp://ftp.kde.org/pub/kde/stable/1.1/distribution">binary package
    sets to appear</a> any time now for the different Unix platforms.<br><br>
    Again, at this moment of celebration, our gratitude is addressed
    to the excellent community of KDE
    developers for their selfless work and to the millions of users which,
    by constant support and
    encouragement, by bug reports and by features suggestions, made KDE
    possible, and the nice, more than two years long, history of our project,
    a glorious one. Thank you all!</font><br><br>
ATTENTION: this Change Log is nowhere near complete. The changes descriptions were kept brief. There are many applications for which the authors didn't have the time (or found it less essential than coding) to submit a list of changes. These applications are not present in this page. However, there is very little code in KDE which wasn't revisited since KDE-1.0. The main result is a much more stable and a faster version, the already well known KDE-1.1. Thanks to all involved. Thanks to the users for their great moral support.
<p>Also, please note that thanks to the excellent team of artists which help KDE look beautiful, a new, improved, nicer set of icons, backgrounds, textures, are available in KDE-1.1. <a href="&#x6d;&#x61;il&#116;o:&#x74;&#x6f;&#114;sten&#x40;&#00107;&#100;&#00101;.o&#114;&#103;">Torsten Rahn</a> kindly provided the beatiful KDE logo which now appears on the panel and in the help and web pages.
        <br>
        <br>
      </td>
    </tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdeadmin
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD>
      <TABLE BORDER=0 CELLSPACING=0 WIDTH="570">
        <TR>
          <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
          <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

    	    kdat

    	  </font></B></TD>
          <TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sean Vyain</font></b></i></td>
        </tr>
            <TR >
          <td colspan="3" align="LEFT" valign="CENTER">
    	<p>A backup tool with GUI
    	<ul>
    	  <li>Uses tar Unix tool
    	  <li>Some more functionality has to be added
    	</ul>
          </TD>
        </TR>
      </TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ksysv

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Peter Putzer</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>complete rewrite</b>
<li><b>documentation</b> now available
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<a name="kuser
"></a>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kuser

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Denis Pershin</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>better support for NIS configurations</b>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdebase
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kappfinder

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Hoelzer</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>new icons</b>
<li><b>more non-KDE applications supported</b>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

KPlayAudio

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>New non-GUI audio playback program to playback audio files
using the regular KDE sound system.</b>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcheckpass

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>This new tool assures complete independence of the usual
KDE applications from the need to be run SUID. This
achievement, due to Christian Esken and a few colaborators,
has a major importance, allowing to close much of the potential
security problems characteristic to SUID programs.

<ul>
            <li><b>plain password, PAM and shadow</b> security models supported
            </ul></TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcontrol

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>collective</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>new info modules</b>: devices, DMA-chanels, Interrupts
...
<li><b>keyboard shortcuts configuration</b>
<li><b>international keyboard configuration</b>
<li><b>KPanel configuration</b>
<li><b>much of KWM's options now supported</b>
<li><b>KFM's configuration</b>, now detached from the browser,
is split in "File Manager" and "Browser" configuration
<li><b>much improved display configuration</b> with
support for random backgrounds, background image cache
configuration and more
<li><b>new support for style configuration</b> - it is an
incipient form of the much enhanced style/themes support
to be brought later (KDE-2.0)
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdehelp

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Martin Jones</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>Find</b> - a <i>find in the page</i> function, with case
sensitiveness
<li><b>RosettaMan</b> - support for the excellent man page translator
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Steffen Hansen, Matthias Ettrich</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>many bug fixes</b>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kfm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Torben Weis (maintained by David Faure)</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>Find</b> - a <i>find in the page</i> function, with case
sensitiveness
<li><b>per-URL settings saving</b> - configurable
<li><b>more charsets support</b>
<li><b>charset support in frames</b>
<li><b>history (Up/Down buttons) for frames</b> - frames browsing
kept into account
<li><b>detached configuration GUI</b> - use `kcmkfm` or the K Control Panel
<li><b>short view</b>
<li><b>"Tree View" syncronizes with browsing</b>
<li><b>icon title colors</b> - now configurable through the "Configure"
dialog
<li><b>fusion of global & local mimetypes directories</b> - (same for applications)
<li><b>Key navigation</b> - enter the first letters of the filename
<li><b>Copy/Paste for text</b> - HTML pages and edit fields
<li><b>Support for cookies</b>
<li><b>Reduced memory usage</b>
<li><b>Improved rendering</b>
<li><b>Support for PNG</b>
<li><b>Better handling of frames</b>
<li><b>Lots of small fixes</b>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kikbd

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Alexander Budnik (maintained by C. Tibirna)</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p> International Keyboard selector/configurator (author <i>Alexander
Budnik</i>, many collaborators)
<ul>
<li><b>select keyboard at runtime</b>
<li><b>saves keyboard settings</b> - per-application, or per window
class, or globally
<li><b>docked in the panel</b>
<li><b>highly configurable</b>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmenuedit

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christoph Neerfeld</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>many improvements</b> in the user interface metaphor
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

konsole

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Lars Doelle</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p> Unix terminal emulator for X. Aimed to replace <b>kvt</b>
<ul>
<li><b>highly configurable</b> - through menus or a configuration
applet
<li><b>pixmapped background</b>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kpanel

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich and collab.</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>added Disk Navigator to KPanel</b> for filesystem navigation from the KPanel menu
<li><b>hide animation</b> - configurable speed
<li><b>left <i>and</i> right hide/show handles</b>
<li><b>autohide for panel and taskbar</b>
<li><b>'Edit Menus' menu entry</b>
<li><b>right-mouse-button contextual menu</b> aside launch buttons
<li><b>panel at display's right side</b> possible now
<li><b>faster menu loading</b> by load-on-demand
<li><b>creation and loading of menus on demand, i.e. faster startup
with less disk access</b>
<li><b>more drag- and drop functionality</b>
<li><b>more configuration options</b>
<li><b>generic KPanel improvements</b>
<li><b>added the KDiskNavigator tab to the KPanel configuration
module</b>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

krootwm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich and collab.</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>MacOS-like menubar</b> support
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kscreensaver

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>collective</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>Screensavers have been reworked. There is no need to run
them SUID root anymore, enhancing security greatly. A new authentication
helper application is used instead. </b>(kcheckpass)
<li><b>new screensavers</b>: science, bsod ...
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kstart

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p> Replaces KStartOnDesk application. Serves as KDE-compatible
application launcher, allowing to indicate launching options specific
to the KDE desktop. Options
<ul>
<li><b>-sticky</b>
<li><b>-on-desktop #</b> where # is the desired desktop
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kvt

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>The author of this terminal emulator considers it almost
obsolete and encourages the use of <b>konsole</b> instead.
<ul>
<li>however<b>many security and sesion management fixes</b> have
been done
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kwm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>configurable <b>key and mouse bindings</b>
<li><b>smart (de-)iconify animation</b> together with the taskbar
<li>autoadjust of maximized windows
<li>support for a <b>common menubar</b> similar to MacOS
<li><b>more visual configuration options</b> for theme designers
<li>more tweaking to deal with <b>low end graphic cards and incapable
X-servers.</b>
<li><b>kstart utility</b> to launch legacy applications with certain
window properties applied
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kwmcom

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>new command options</b> most notable being support for the Window Maker windowmanager.
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>
    
    <TR>
      <TD>
        <TABLE BORDER=0 CELLSPACING=0 WIDTH="570"  >
          <TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>kpager</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Antonio Larrosa</font></b></i></td></tr>
          </TR>
          <TR >
            <TD ></TD>
            <TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
              <p> Another KWM-compatible desktop pager.
              <ul>
                <li><b>desktop mini-view</b> reading kbgndwm settings
                <li><b>move windows across desktops</b>
                <li><b>on-pager window resize</b>
                <li><b>global desktop</b> for defining sticky windows
              </ul>
              </TD>
          </TR>
        </TABLE>
      </td>
    </TR>
    <tr>
      <TD>&nbsp;</TD>
    </tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdegames
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>
<tr><td>All the games were upgraded to KDE-1.1. There are no significant additions, but a lot of bugfixes were performed</td></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdegraphics
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kview

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj Singh Kang</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>KView was completely rewritten and now has a better interface,
including new features like good configurability, a full-screen
mode and support for TIFF, PNG and XVPICs image formats.
</TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdelibs
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kab

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Mirko Sucker</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>This is the start for a common addressbook library which will
be used soon by all KDE applications who need such a library. Initial use
of this library is available in KOrganizer.
</TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kfile

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stephan Kulow</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>much improved keyboard navigation in the filedialog</li>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
<TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
<TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kimgio

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj Singh Kang</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>This is the first version of KDE shipped with the KDE Image IO
library, which adds KDE-wide support for additional image
formats. New formats with this release are: PNG, TIFF and XVPICs.
</TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdemultimedia
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

<TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
<TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
<TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmid

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Antonio Larrosa</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>ChannelView</b> to <b>see the notes</b> played by
each instrument and <b>change the instrument</b> used in each channel
<li><b>Volume slider</b>
<li>Tempo is modifiable to <b>play songs faster or slower</b>
<li><b>Better support for really corrupted midi files</b>
<li><b>Fixed an important bug</b> related to songs
losing the rhythm and playing at a changing tempo which many users
reported
<li>Beginnings of SoftOSS support

            </ul></TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>



            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmix

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li><b>fixes regarding session management</b> and more
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdenetwork
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmail

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stefan Taferner, Markus Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Many bug fixes. A few highly requested features have been also added (like displaying "Sender" field in incoming folders). The main accent was on increasing the stability.
</TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

knu

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bertrand Leconte</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li> <i>mtr</i> support added. <i>mtr</i> is a tool that can make a traceroute
to a host and make a ping for each hop found.
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kppp

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Mario Weilguni, Harri Porten, Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>throughput graph
<li>debugging aid
<li>enhanced security - separated GUI frontend and SUID backend
<li>Quickhelp feature
<li>new and updated rule files
<li>many usability improvements and bugfixes
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

krn

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Roberto Alsina</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>The mandatory bugfixes were applied.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ksirc

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Andrew Stanley-Jones</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>KSirc improved a lot and has consolidated its position
as one of the best IRC clients available for Unix GUI.
<p>It also served as a trial field for a series of KDE inventions and inovations (a memory alloc checker for C++, a dynamic shortcut configurator for menus etc.)

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ktalkd

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>David Faur&eacute;</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Now in kdenetwork, ktalkd is an enhanced talk daemon - a
program to handle incoming talk requests,
announce them and allow you to respond to it using a
talk client.
It features :
<ul>
<li>Answering machine
<li>Sound playing with the announcement
<li>Multiple displays annoucement
<li>Support for otalk and ntalk
<li>Forwarding capability
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdenonbeta
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>
<tr><td>This section isn't yet released with the others KDE packages. Although, many distributors prefer to distribute this little gem too, given the excellent code found in there. The applications included will hopefully reach a more stable status soon, and thus will be included in the main packages.
</td></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcharselect

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Reginald Stadlbauer</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Nice character selector with X-Window copy/paste support</TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcmbind

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Karl Backstr&ouml;m, Olof Wolgast</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A GUI configurator for BIND. Compatible with KControl

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcmdhcpd

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Karl Backstr&ouml;m</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A GUI configurator for DHCPd. Compatible with KControl

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcmkpanel2

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Pietro Iglio</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>The next generation panel configuration. Already used in post-1.1 KDE development versions.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcrontab

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Michael Bialas</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Nice crontab GUI. Ever struggled with crontab syntax? It's finished now.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kexpress

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Peter Wichert</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>An easy-to-use Newsreader, now with lots of fixed bugs 8-)<ul>
<li>offline read/write
<li>more than one news-server, tree view
<li>full threading
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kglchess

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Johannes Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>3D chessboard.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kgnuplot

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Johannes Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A better X interface for gnuplot.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmail2

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stefan Taferner</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>The next generation KMail. Still in alpha phase.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

knetmon

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Alexander Neundorf</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A network monitor with support for Samba. Allows a nice integration in a windows network.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

korn

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj Singh Kang</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A much better korn. Very usable. Still, the author wants to work it more before releasing it.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kpackage

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Toivo Pedaste</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>The much praised KDE package manager. Supports very well RPM and DEB formats. In post-1.1 releases, it's already included in the kdeadmin package.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kpanel2

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Pietro Iglio</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>

<ul>
            <li>KPanel2 is a major clean up of KPanel source code. Among the
            added features there is <b>full drag'n'drop support</b> (user can
            drag icons from the K-menu to the panel, move and delete menu
            entries, move/copy files using KDiskNavigator, etc.) and
            <b>contextual menus</b> on menu entries.
            </ul></TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kthememgr

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stefan Taferner</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Want installing KDE themes with a click of a button? You have it. The author wants to add saving support. Also, support for the new theme-ing features in Qt-2.0 will be added soon.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kticker

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Paul Dwerryhouse</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>News ticker. Supports, among others, Freshmeat and Slashdot.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ktop

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>collective, Chris Schlaeger</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p> The new KDE Task Manager.
<p><ul>
<li>Complete re-write of the code.
<li>Qt 1.4x and KDE 1.x compatible.
<li>Several bugs fixed. A lot more user friendly.
<li>Easier to port to other flavours of UNIX.
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

libkmedia

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>New and improved media library. Hopefully, will contain support for network shared multimedia.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

libkmsg

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stefan Taferner</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>The KDE's mailing library. To be used in the future by all the KDE applications which will need e-mail support. Now it's only used by kmail2.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

maudio2

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>New (better) version of the maudio tool.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

webmaker

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Alexei Dets</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>HTML editor for KDE.
<ul>
<li>nice GUI interface;
<li>menus, toolbar and dialogs for tag editing - like HomeSite and asWedit;
<li>multiple windows support;
<li>full HTML 4.0 support;
<li>preview for <B>&lt;IMG&gt;</B> tag;
<li>color selectors for bgcolor and other color attributes;
<li>color syntax highlighting;
<li>preview with external browser (Netscape);
<li>ability to filter editor content through any external program
that support stdin/stdout interface;
<li>customisable toolbars.
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdesdk
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kappgen

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Hoelzer</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Automatic applications generator with a GUI. The immediate result is a compilable application, complete with configure and makefile scripts. The user only has to add some amount of specific code

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kapptemplate

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Kurt Granroth</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Another KDE application generator, working only in console mode. Preferred by some for its ease of use.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdedoc

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj S. Kang, Torben Weis</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Documentation tools

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdepalettes

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Daniel M. Duley</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>The officially approved color palettes to be used by graphic artists and authors when writing KDE-related things. Respecting these palettes assures a small color hog of KDE over X-Window.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdesgmltools

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Panayotis Vryonis, Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Documentation tools. An adapted sgml tools version. Generates KDE-specific formatted documentation from SGML code.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kexample

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font></font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A live example (and template) of a KDE application.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ktranslator

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Andrea Rizzi</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Translation tool. Allows automation of GUI translation tasks.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

scripts

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj Singh Kang</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Valuable scripts like cvs2pack and cxxmetrics.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdesupport
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

QwSpriteField

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Warwick Allison</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A highly performant canvas widget. Considered by testers much more efficient than Tk canvas or other similar offers on the free software scene.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdetoys
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmoon

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stephan Kulow</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A moon phase watcher. Just sits on the panel and stares at you :-). Especially valuable for those with a near-to-nature soul.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kworldwatch

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias H&ouml;lzer-Kl&uuml;pfel</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>The time, the day and the night all over our small planet. For an international project, like KDE, this is a nice tool to have :-)

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

mouse

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Armen Nakashian</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Computer mice are long lasting runners. This is a pedometer for your beloved mouse.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdeutils
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ark

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Robert Palmbos</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Replaces the old KZip program. The change was needed because of
a name conflict with a system tool on FreeBSD

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kab

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Mirko Sucker</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

karm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj Singh Kang</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>Added configurable keybindings.</li>
<li>i18n and Solaris support.</li>
<li>Some bugfixes.</li>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcalc

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A notable fix for statistic calculations was made.

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kedit

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Update to KDE-1.1, bug fixes, maintainance (it was already very stable in KDE-1.0).

</TD></TR></TABLE>
            </td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

klipper

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font></font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>An easy to use <b>cut &amp; paste history</b>, accessible via
KPanel or a configurable shortcut</b>
</TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kwrite

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Jochen Wilhelmy</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Nice, color-syntax capable, advanced editor
<ul>
<li>programming languages support: C/C++, HTML, Python and many others
<li>unlimited undo
<li>many more
</ul>
</TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
klyx
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

&nbsp;

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich and collab.</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>many bugs fixes and improvements were added.
<li>there is an effort in course, to align KLyX to LyX-1.0
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
korganizer
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

            <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
            <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
            <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

&nbsp;
</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Preston Brown and collab.</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>a large amount of functionality was added
<li>rock solid and stable. Many bugs squashed :-)
<li>support for PalmPilot conduits.
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

    </TABLE></CENTER>
