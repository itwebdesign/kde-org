------------------------------------------------------------------------
r1004203 | rbitanga | 2009-07-29 15:29:25 +0000 (Wed, 29 Jul 2009) | 2 lines

Backport fix to allow updating of status via microblog plasmoid

------------------------------------------------------------------------
r1004800 | sebas | 2009-07-30 22:37:50 +0000 (Thu, 30 Jul 2009) | 11 lines

Use the smallestReadableFont instead of some hand-rolled size

- Dont display the logo in the panel when there's not enough space
- Also save the feedList after a feed has been dropped. In this case, it's
  OK to call configNeedsSaving().

CCBUG:184017

backported from trunk


------------------------------------------------------------------------
r1005287 | darioandres | 2009-07-31 18:35:48 +0000 (Fri, 31 Jul 2009) | 8 lines

Backport to 4.3:
SVN commit 1005285 by darioandres:

Use CSS for global formatting to avoid affecting children QFont.
Review by Sebas

CCBUG: 201982

------------------------------------------------------------------------
r1005774 | scripty | 2009-08-02 03:03:25 +0000 (Sun, 02 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1006283 | mart | 2009-08-03 12:17:23 +0000 (Mon, 03 Aug 2009) | 2 lines

backport removal of news.css, broken with air

------------------------------------------------------------------------
r1006290 | mart | 2009-08-03 12:41:21 +0000 (Mon, 03 Aug 2009) | 2 lines

add news.css again

------------------------------------------------------------------------
r1006404 | aseigo | 2009-08-03 17:43:18 +0000 (Mon, 03 Aug 2009) | 3 lines

backport: this method gets called from a signal from the job which is part of the service, so deleteLater the service otherwise we delete the job in the middle of one of its methods!
BUG:202364

------------------------------------------------------------------------
r1008028 | pdamsten | 2009-08-06 18:02:16 +0000 (Thu, 06 Aug 2009) | 1 line

Backport support for translatable labels as requested by i18n people. Adds support for modifying svg texts to lcd widget.
------------------------------------------------------------------------
r1008116 | aseigo | 2009-08-06 22:27:52 +0000 (Thu, 06 Aug 2009) | 3 lines

use https
CCBUG:202825

------------------------------------------------------------------------
r1008158 | bjacob | 2009-08-07 00:39:21 +0000 (Fri, 07 Aug 2009) | 3 lines

backport r1008125: fix plasma freezer


------------------------------------------------------------------------
r1008190 | scripty | 2009-08-07 03:05:41 +0000 (Fri, 07 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1008267 | ivan | 2009-08-07 08:37:28 +0000 (Fri, 07 Aug 2009) | 4 lines

Optimizations and bug fix for Kopete model
bug:196809


------------------------------------------------------------------------
r1008495 | sebas | 2009-08-07 16:30:53 +0000 (Fri, 07 Aug 2009) | 8 lines

Fix dropping of content and creating the previewer

We should not just create the applet, but also pass in the filename so it gets loaded, otherwise it would be like clicking on a file, opening the default application for that file-type, but not actually loading the file.

Backported from trunk.

BUG:202948

------------------------------------------------------------------------
r1008840 | aacid | 2009-08-08 13:30:10 +0000 (Sat, 08 Aug 2009) | 2 lines

fix Comment line, acked by notmart

------------------------------------------------------------------------
r1008841 | aacid | 2009-08-08 13:32:49 +0000 (Sat, 08 Aug 2009) | 2 lines

better wording by notmart

------------------------------------------------------------------------
r1009064 | scripty | 2009-08-09 04:18:40 +0000 (Sun, 09 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1009338 | mart | 2009-08-09 19:05:45 +0000 (Sun, 09 Aug 2009) | 4 lines

backport the activity engine fix
the id is still a bit fake, probably the fetch of the proper id will
need to be backported as well (once implemented)

------------------------------------------------------------------------
r1009454 | scripty | 2009-08-10 03:11:48 +0000 (Mon, 10 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1009592 | mart | 2009-08-10 12:06:01 +0000 (Mon, 10 Aug 2009) | 3 lines

backport the use of the proper id for the activity (coming from the
server)

------------------------------------------------------------------------
r1009723 | aacid | 2009-08-10 18:18:38 +0000 (Mon, 10 Aug 2009) | 2 lines

correctly format time

------------------------------------------------------------------------
r1009873 | scripty | 2009-08-11 03:18:10 +0000 (Tue, 11 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1010264 | scripty | 2009-08-12 03:19:26 +0000 (Wed, 12 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1010642 | scripty | 2009-08-13 03:17:47 +0000 (Thu, 13 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011228 | scripty | 2009-08-14 03:22:09 +0000 (Fri, 14 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011563 | scripty | 2009-08-15 03:06:33 +0000 (Sat, 15 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011647 | aacid | 2009-08-15 10:52:57 +0000 (Sat, 15 Aug 2009) | 2 lines

backport i18n fix, approved by the i18n teams

------------------------------------------------------------------------
r1011849 | scripty | 2009-08-16 03:15:58 +0000 (Sun, 16 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1011970 | ivan | 2009-08-16 13:47:50 +0000 (Sun, 16 Aug 2009) | 3 lines

backport the compositing feature reporting


------------------------------------------------------------------------
r1012081 | gkiagia | 2009-08-16 18:58:24 +0000 (Sun, 16 Aug 2009) | 4 lines

Backport r1011983 to the 4.3 branch.
Minimize the dependencies. XComposite and XDamage are not really used, only XRender is used.
CCMAIL: ivan.cukic@gmail.com

------------------------------------------------------------------------
r1013292 | ewoerner | 2009-08-19 13:04:25 +0000 (Wed, 19 Aug 2009) | 3 lines

Don't query for nearby users unless an account has already been set up, backport of r1013288
This eliminates the annoying password dialog at applet creation time

------------------------------------------------------------------------
r1013503 | kbal | 2009-08-20 08:31:15 +0000 (Thu, 20 Aug 2009) | 3 lines

Make the unitconverter applet use translations from libconversion.mo.
Patch inspired by a similar one from Anrew Coles to libplasmaweather.
Will check it in trunk too.
------------------------------------------------------------------------
r1013882 | scripty | 2009-08-21 03:01:26 +0000 (Fri, 21 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1014363 | darioandres | 2009-08-22 15:38:48 +0000 (Sat, 22 Aug 2009) | 38 lines

Backport to 4.3(tested) of:
SVN commit 1014360 by darioandres:

- Reverting the last change to use CSS to global formatting, as it has major
problems:
  - All the text is saved as html and that includes the proper formatting as
html/css; so
  setting a global CSS style will not affect future changes. ("local css" >
"global css" in priority)
  - It also conflicts with the way KRichTextEdit works

- Set all the font changes using the KTextEdit "set*()" methods, but previously
selecting all the text
  using QTextCursor (and then restoring the old cursor (restore text
selection))

- On updateTextGeometry(), only the font family and size changes are applied.
This are non-destructive changes
  as the user has no other way to set this settings beside using the
Configuration Dialog; so we can do them
  at any time

- Bold and italic settings are destructive (as setting a global bold/italic
will override the custom
  formatting the user could have done when manually changing them using the
Notes controls at the bottom)
  - So, we are going to apply this destructive global changes only if needed
(if any of them changed in the
  configuration dialog)

- Small fix on load: Do not load the font properties on the Configuration
Dialog as the font() property
  of the textedit. This is not true, we should use our stored var "m_font"
which properly loads the settings
  from the config file

CCBUG: 204308

------------------------------------------------------------------------
r1014389 | darioandres | 2009-08-22 18:08:57 +0000 (Sat, 22 Aug 2009) | 31 lines

Backport to 4.3 of:
SVN commit 1014374 by darioandres:

- When the text is deleted the format will always reverted to zero; as
KTextEdit calls only work on
  local/selected text, but they won't set a "default" format.
  - To fix this, I adapted the patch by anmma at
http://reviewboard.kde.org/r/679/
    - On "lineChange", detect if the character entered is the first one
      (that means the previous text (if any) was deleted) and set the default
char text format
      to use the defined font style/size/family and foreground color.
  So, the global format/style settings will be always honored

- Do not change the QPalette of the KTextEdit as this is not needed anymore
(was it needed before?)

and:
SVN commit 1014384 by darioandres:

- Simplify the code to selectAll+change some text property

- On "updateTextGeometry" it is not necessary to set the font family again.
  - Set the font family only on "configAccepted" (if the font was changed)

- Fix a bug in the following workflow:
  - Text written, deleted, widget resized, text written again will have the previous size
  (calculated before the "updateTextGeometry" call). That is why we need to call "lineChanged" again

CCBUG: 176266

------------------------------------------------------------------------
r1014400 | darioandres | 2009-08-22 18:48:59 +0000 (Sat, 22 Aug 2009) | 10 lines

Backport to 4.3branch of:
- Set the font family on init, as the config file could have been edited from the outside.

- Only recalculate the default/initial font size (when there is no text) if the font size setting is 
  based on scales. (m_autoFont is true)

- Remove the "m_font.setPointSize(fontSize())" calls on "configAccepted". If any of the font size 
  settings changed, "updateTextGeometry" will be called anyways.


------------------------------------------------------------------------
r1014685 | darioandres | 2009-08-23 15:34:41 +0000 (Sun, 23 Aug 2009) | 3 lines

- Do not use the deprecated "KUrl KUrl::fromPathOrUrl"


------------------------------------------------------------------------
r1014870 | scripty | 2009-08-24 03:34:32 +0000 (Mon, 24 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1015642 | scripty | 2009-08-26 02:52:22 +0000 (Wed, 26 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1015861 | kbal | 2009-08-26 13:14:04 +0000 (Wed, 26 Aug 2009) | 1 line

forgot two
------------------------------------------------------------------------
r1016201 | scripty | 2009-08-27 03:38:01 +0000 (Thu, 27 Aug 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
