2008-04-03 17:47 +0000 [r793345]  wstephens

	* branches/KDE/3.5/kdeaccessibility/kmag/kmagzoomview.cpp: Fix
	  https://bugzilla.novell.com/show_bug.cgi?id=365026 (KMag crash on
	  exit) - Fix thanks to SadEagle.

2008-06-23 12:45 +0000 [r823451]  pino

	* branches/KDE/3.5/kdeaccessibility/kttsd/kcmkttsmgr/kcmkttsd.desktop,
	  branches/KDE/3.5/kdeaccessibility/ksayit/src/ksayit.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsmgr/kttsmgr.desktop:
	  desktop validation fixes: - remove deprecated Encoding key -
	  remove deprecated %m placeholder in Exec keys - fixup Categories
	  - remove empty deprecated MimeTypes key patch by Ana Guerrero,
	  thanks!

2008-06-29 22:37 +0000 [r826168]  anagl

	* branches/KDE/3.5/kdeaccessibility/kttsd/plugins/festivalint/kttsd_festivalintplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/akodeplayer/kttsd_akodeplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/freetts/kttsd_freettsplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/hadifix/kttsd_hadifixplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/app-plugins/kate/ktexteditor_kttsd.desktop,
	  branches/KDE/3.5/kdeaccessibility/kmag/kmag.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/command/kttsd_commandplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/artsplayer/kttsd_artsplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/xmltransformer/kttsd_xmltransformerplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/stringreplacer/kttsd_stringreplacerplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kmousetool/kmousetool/kmousetool.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/talkerchooser/kttsd_talkerchooserplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsjobmgr/kttsjobmgr.desktop,
	  branches/KDE/3.5/kdeaccessibility/kbstateapplet/kbstateapplet.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/compat/interfaces/kspeech/dcoptexttospeech.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/libkttsd/kttsd_synthplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/gstplayer/kttsd_gstplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kcmkttsmgr/kcmkttsd_testmessage.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/flite/kttsd_fliteplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/sbd/kttsd_sbdplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/kttsd/kttsd.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/players/alsaplayer/kttsd_alsaplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/plugins/epos/kttsd_eposplugin.desktop,
	  branches/KDE/3.5/kdeaccessibility/kmouth/kmouth.desktop,
	  branches/KDE/3.5/kdeaccessibility/kttsd/filters/kttsd_filterplugin.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-08-19 19:46 +0000 [r849591]  coolo

	* branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm: update
	  for 3.5.10

