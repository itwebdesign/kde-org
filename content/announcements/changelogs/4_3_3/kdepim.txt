------------------------------------------------------------------------
r1030630 | helio | 2009-10-02 20:00:09 +0000 (Fri, 02 Oct 2009) | 1 line

- Icons already included in oxygen tarball
------------------------------------------------------------------------
r1031050 | winterz | 2009-10-03 20:48:44 +0000 (Sat, 03 Oct 2009) | 11 lines

Backport r1031046 by winterz from trunk to the 4.3 branch:

we set KMAIL_SUPPORTED if we are building KMail.  which is good.
however we never added KMAIL_SUPPORTED to the defined compiler macros.  which is bad.

Now that we define KMAIL_SUPPORTED, we can actually see a list of accounts to synchronize
in the "Sync All" tool submenu.

MERGE: e4,4.3


------------------------------------------------------------------------
r1031051 | winterz | 2009-10-03 20:51:50 +0000 (Sat, 03 Oct 2009) | 4 lines

backport SVN commit 1030927 by winterz:

fix data corruption for recurring events

------------------------------------------------------------------------
r1031052 | winterz | 2009-10-03 20:54:44 +0000 (Sat, 03 Oct 2009) | 4 lines

backport SVN commit 1028614 by winterz:

don't show events that are already over. such events cannot be considered "upcoming".

------------------------------------------------------------------------
r1031053 | winterz | 2009-10-03 20:55:45 +0000 (Sat, 03 Oct 2009) | 4 lines

backport SVN commit 1028896 by winterz:

properly handle recurring events when checking if an event has already occurred during the day.

------------------------------------------------------------------------
r1031056 | winterz | 2009-10-03 21:00:11 +0000 (Sat, 03 Oct 2009) | 6 lines

backport SVN commit 1029390 by winterz:

Fix a data corruption bug caused by cloning of recurring events.
Now we print the first upcoming event of the recurring series, along with
the datetime of the next one after that.

------------------------------------------------------------------------
r1031132 | scripty | 2009-10-04 03:01:47 +0000 (Sun, 04 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1031368 | aacid | 2009-10-04 19:55:21 +0000 (Sun, 04 Oct 2009) | 4 lines

Add the libkdepim catalog
CCMAIL: djarvie@kde.org
BUGS: 209401

------------------------------------------------------------------------
r1031532 | cgiboudeaux | 2009-10-05 11:34:28 +0000 (Mon, 05 Oct 2009) | 4 lines

Backport r1031526 from trunk to 4.3:
Add the KMAIL_SUPPORTED definition only if KMail is compiled.


------------------------------------------------------------------------
r1031533 | cgiboudeaux | 2009-10-05 11:37:31 +0000 (Mon, 05 Oct 2009) | 4 lines

Backport r1031527 from trunk to 4.3:
The definition is now set in the main CMakeLists.txt


------------------------------------------------------------------------
r1031538 | lueck | 2009-10-05 11:41:15 +0000 (Mon, 05 Oct 2009) | 1 line

load catalog with translations for kdatepickerpopup
------------------------------------------------------------------------
r1031560 | winterz | 2009-10-05 12:55:15 +0000 (Mon, 05 Oct 2009) | 3 lines

backport parts of r1022930 so that compiling the dataloss fix works.


------------------------------------------------------------------------
r1031716 | lueck | 2009-10-05 20:16:22 +0000 (Mon, 05 Oct 2009) | 2 lines

load catalog with translations for kdatepickerpopup
CCMAIL:dev@staerk.de
------------------------------------------------------------------------
r1032418 | djarvie | 2009-10-07 19:19:57 +0000 (Wed, 07 Oct 2009) | 4 lines

Fix recurring date-only alarm triggering repeatedly and eating up CPU, when the
start of day time is after midnight and the alarm is due, but the current time of
day in UTC is earlier than the start-of-day time of day in the alarm's time zone.

------------------------------------------------------------------------
r1032446 | sengels | 2009-10-07 20:58:04 +0000 (Wed, 07 Oct 2009) | 1 line

templates can't be exported
------------------------------------------------------------------------
r1032449 | sengels | 2009-10-07 21:01:22 +0000 (Wed, 07 Oct 2009) | 1 line

grp2 and grp3 are used by mingw already
------------------------------------------------------------------------
r1032499 | djarvie | 2009-10-07 23:30:47 +0000 (Wed, 07 Oct 2009) | 3 lines

Update date-only alarm trigger times when user changes the start-of-day time.
Don't write start-of-day time into calendar, to avoid clashes if it is shared.

------------------------------------------------------------------------
r1032802 | sengels | 2009-10-08 14:42:17 +0000 (Thu, 08 Oct 2009) | 1 line

fix missing rename
------------------------------------------------------------------------
r1032874 | dfaure | 2009-10-08 19:40:58 +0000 (Thu, 08 Oct 2009) | 2 lines

Backport: Make kontact the imap backend for the kolab resource.

------------------------------------------------------------------------
r1033121 | tmcguire | 2009-10-09 14:32:39 +0000 (Fri, 09 Oct 2009) | 20 lines

Backport r1029796 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1027247 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1027247 | winterz | 2009-09-23 18:49:31 +0200 (Wed, 23 Sep 2009) | 10 lines
  
  Merged revisions 1027235 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1027235 | winterz | 2009-09-23 12:22:11 -0400 (Wed, 23 Sep 2009) | 3 lines
    
    in setHolidaysMasks(), avoid a crash if the selecteddates QList is empty.
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1033122 | tmcguire | 2009-10-09 14:35:34 +0000 (Fri, 09 Oct 2009) | 20 lines

Backport r1029799 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1027946 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1027946 | tmcguire | 2009-09-25 10:36:37 +0200 (Fri, 25 Sep 2009) | 10 lines
  
  Merged revisions 1027157 via svnmerge from 
  svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1027157 | tmcguire | 2009-09-23 14:26:40 +0200 (Wed, 23 Sep 2009) | 3 lines
    
    Always set the identity when answering an invitation, as we need that to figure things
    like the transport out.
  ........
................


------------------------------------------------------------------------
r1033302 | tmcguire | 2009-10-09 21:15:14 +0000 (Fri, 09 Oct 2009) | 14 lines

Backport r1033249 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1029699 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

........
  r1029699 | tmcguire | 2009-09-30 17:08:52 +0200 (Wed, 30 Sep 2009) | 3 lines
  
  Don't crash when an LDAP entry has no mail address.
  Part of kolab/issue1499
........


------------------------------------------------------------------------
r1033303 | tmcguire | 2009-10-09 21:15:58 +0000 (Fri, 09 Oct 2009) | 18 lines

Backport r1033251 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1029749,1030448 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

........
  r1029749 | tmcguire | 2009-09-30 18:46:48 +0200 (Wed, 30 Sep 2009) | 3 lines
  
  Report the object class again, so we again detect distribution lists and can add domain names
  to the entries, for the email address.
........
  r1030448 | tmcguire | 2009-10-02 12:06:40 +0200 (Fri, 02 Oct 2009) | 1 line
  
  No need to add the object class here, that is done in setAttrs() already.
........


------------------------------------------------------------------------
r1033304 | tmcguire | 2009-10-09 21:17:09 +0000 (Fri, 09 Oct 2009) | 20 lines

Backport r1033258 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1030068 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1030068 | winterz | 2009-10-01 15:36:24 +0200 (Thu, 01 Oct 2009) | 10 lines
  
  Merged revisions 1030061 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1030061 | winterz | 2009-10-01 09:25:53 -0400 (Thu, 01 Oct 2009) | 3 lines
    
    another crash guard for passing invalid dates to setHolidayMasks()
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1033308 | tmcguire | 2009-10-09 21:21:59 +0000 (Fri, 09 Oct 2009) | 20 lines

Backport r1033242 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1029632 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1029632 | winterz | 2009-09-30 14:29:22 +0200 (Wed, 30 Sep 2009) | 9 lines
  
  Merged revisions 1029609 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1029609 | tmcguire | 2009-09-30 06:42:06 -0400 (Wed, 30 Sep 2009) | 1 line
    
    Work around a problem with buildObjectTree() modifing parent nodes by calling that before setting the parent.
  ........
................


------------------------------------------------------------------------
r1033311 | tmcguire | 2009-10-09 21:23:49 +0000 (Fri, 09 Oct 2009) | 23 lines

Backport r1033245 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1029649 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1029649 | winterz | 2009-09-30 14:50:51 +0200 (Wed, 30 Sep 2009) | 12 lines
  
  Merged revisions 1029626 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1029626 | mutz | 2009-09-30 08:00:03 -0400 (Wed, 30 Sep 2009) | 4 lines
    
    ISubject: iterate over a copy of the observerlist
    
    This prevents corruption of the iterators when attach() or detach() is called
    (indirectly) through one of our Observer::update() calls
  ........
................


------------------------------------------------------------------------
r1033312 | tmcguire | 2009-10-09 21:26:53 +0000 (Fri, 09 Oct 2009) | 22 lines

Backport r1033259 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1030121 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1030121 | winterz | 2009-10-01 17:59:09 +0200 (Thu, 01 Oct 2009) | 11 lines
  
  Merged revisions 1030101 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1030101 | mutz | 2009-10-01 11:23:09 -0400 (Thu, 01 Oct 2009) | 3 lines
    
    KMReaderWin::objectTreeToDecryptedMsg: fix for PGP/MIME messages
    
    (and probably S/MIME message with mp/encrypted, but I haven't seen those anywhere yet - KMail certainly doesn't create them)
  ........
................


------------------------------------------------------------------------
r1033313 | tmcguire | 2009-10-09 21:30:52 +0000 (Fri, 09 Oct 2009) | 21 lines

Backport r1033264 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1030122 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1030122 | winterz | 2009-10-01 18:00:24 +0200 (Thu, 01 Oct 2009) | 11 lines
  
  Merged revisions 1030105 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1030105 | mutz | 2009-10-01 11:26:23 -0400 (Thu, 01 Oct 2009) | 3 lines
    
    KMReaderWin::objectTreeToDecryptedMsg: fix S/MIME signatures being invalidated by the process of stripping the encryption
    
    This is very likely an incomplete fix.
  ........
................


------------------------------------------------------------------------
r1033320 | tmcguire | 2009-10-09 21:40:02 +0000 (Fri, 09 Oct 2009) | 22 lines

Backport r1033272 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1030133 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1030133 | winterz | 2009-10-01 18:34:33 +0200 (Thu, 01 Oct 2009) | 11 lines
  
  Merged revisions 1030111 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1030111 | mutz | 2009-10-01 11:29:37 -0400 (Thu, 01 Oct 2009) | 3 lines
    
    KMReaderWin fix the trigger conditions for the store-unencrypted feature
    
    The old trigger guard was invalidated when the explicit-decrypt feature made every mail be already marked as read when this function was finally called.
  ........
................


------------------------------------------------------------------------
r1033327 | djarvie | 2009-10-09 21:56:27 +0000 (Fri, 09 Oct 2009) | 2 lines

Ensure all-day is set consistently for recurrences

------------------------------------------------------------------------
r1033333 | djarvie | 2009-10-09 22:06:01 +0000 (Fri, 09 Oct 2009) | 1 line

Nitpick
------------------------------------------------------------------------
r1033377 | djarvie | 2009-10-09 23:55:49 +0000 (Fri, 09 Oct 2009) | 1 line

Don't waste processing time calculating next trigger time for archived alarms
------------------------------------------------------------------------
r1033731 | djarvie | 2009-10-10 21:54:52 +0000 (Sat, 10 Oct 2009) | 2 lines

Fix whatsthis not appearing

------------------------------------------------------------------------
r1034078 | mkoller | 2009-10-11 21:51:24 +0000 (Sun, 11 Oct 2009) | 7 lines

Backport r1034077 by mkoller from trunk to the 4.3 branch:

CCBUG: 160215

On smart-reply, use the ReplyAll template only when it's a reply to a mailing list


------------------------------------------------------------------------
r1035444 | scripty | 2009-10-15 03:17:14 +0000 (Thu, 15 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1035759 | tstaerk | 2009-10-15 20:08:08 +0000 (Thu, 15 Oct 2009) | 3 lines

Show menues again in ktimetracker.
BUGS:209570

------------------------------------------------------------------------
r1035835 | djarvie | 2009-10-15 23:44:01 +0000 (Thu, 15 Oct 2009) | 2 lines

Bug 210552: fix crash when calendars are updated at login, during session restoration.

------------------------------------------------------------------------
r1035862 | scripty | 2009-10-16 03:17:36 +0000 (Fri, 16 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1036733 | tnyblom | 2009-10-17 17:25:44 +0000 (Sat, 17 Oct 2009) | 7 lines

Backport r1036720
Make kmail caseinsensitive when processing mailto urls also enable "to" to be
specified in the url explisitly.

CCBUG 108974


------------------------------------------------------------------------
r1036844 | djarvie | 2009-10-17 22:05:50 +0000 (Sat, 17 Oct 2009) | 2 lines

Improve error feedback to user when choosing file to display

------------------------------------------------------------------------
r1037271 | djarvie | 2009-10-18 18:46:17 +0000 (Sun, 18 Oct 2009) | 2 lines

Interpret '~' properly in entered file names.

------------------------------------------------------------------------
r1038317 | scripty | 2009-10-21 03:09:04 +0000 (Wed, 21 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1039041 | djarvie | 2009-10-22 15:48:49 +0000 (Thu, 22 Oct 2009) | 1 line

Add missing 16x16 icon
------------------------------------------------------------------------
r1039594 | djarvie | 2009-10-23 22:13:09 +0000 (Fri, 23 Oct 2009) | 2 lines

Fix dialogue closing after displaying an error message

------------------------------------------------------------------------
r1040916 | scripty | 2009-10-27 04:36:20 +0000 (Tue, 27 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1041099 | winterz | 2009-10-27 11:27:17 +0000 (Tue, 27 Oct 2009) | 8 lines

Backport r1041097 by winterz from trunk to the 4.3 branch:

merge SVN commit 1041096 by sebsauer from the akonadi-ports branch:

Finally fixed the jumping items i9n the monthview. Guess that bug is present in all KOrganizer versions. The problem is, that the case that 2 incidences which are both scheduled for a whole day are randomly sorted. Now we are using greaterThanFallback which also compares times and falls back to the id's. That way we are sure the sorting is stable.



------------------------------------------------------------------------
r1041157 | tmcguire | 2009-10-27 14:12:31 +0000 (Tue, 27 Oct 2009) | 20 lines

Backport r1040986 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1033123 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1033123 | winterz | 2009-10-09 16:39:33 +0200 (Fri, 09 Oct 2009) | 10 lines
  
  Merged revisions 1033101 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1033101 | tmcguire | 2009-10-09 09:34:51 -0400 (Fri, 09 Oct 2009) | 3 lines
    
    Don't allow selecting entries without email addresses.
    Fixes kolab/issue1499
  ........
................


------------------------------------------------------------------------
r1041158 | tmcguire | 2009-10-27 14:13:07 +0000 (Tue, 27 Oct 2009) | 21 lines

Backport r1040990 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1033126 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1033126 | winterz | 2009-10-09 16:45:22 +0200 (Fri, 09 Oct 2009) | 10 lines
  
  Merged revisions 1033109 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1033109 | tmcguire | 2009-10-09 09:47:11 -0400 (Fri, 09 Oct 2009) | 3 lines
    
    Use the KPIM namespace here, to avoid symbol clashes with the class with the
    same name in kaddressbook
  ........
................


------------------------------------------------------------------------
r1041159 | tmcguire | 2009-10-27 14:14:37 +0000 (Tue, 27 Oct 2009) | 16 lines

Backport r1041056 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1034833 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

........
  r1034833 | winterz | 2009-10-13 18:45:57 +0200 (Tue, 13 Oct 2009) | 5 lines
  
  do not print the timezone countrycode in the tooltip if that value is empty.
  part of the fix for kolab/issue3493
  
  MERGE: trunk,4.3
........


------------------------------------------------------------------------
r1041160 | tmcguire | 2009-10-27 14:15:28 +0000 (Tue, 27 Oct 2009) | 23 lines

Backport r1041058 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1035201 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1035201 | winterz | 2009-10-14 16:29:06 +0200 (Wed, 14 Oct 2009) | 12 lines
  
  Merged revisions 1035198 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1035198 | winterz | 2009-10-14 10:20:57 -0400 (Wed, 14 Oct 2009) | 5 lines
    
    a possible fix for the Marcus Bains line QTimer interfering with
    the KCheckAccelerator QTimer.  This is hard to reproduce so I'm not sure.
    kolab/issue3903
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1041161 | tmcguire | 2009-10-27 14:16:36 +0000 (Tue, 27 Oct 2009) | 22 lines

Backport r1041060 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1035645 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1035645 | winterz | 2009-10-15 17:23:00 +0200 (Thu, 15 Oct 2009) | 11 lines
  
  Merged revisions 1035643 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1035643 | winterz | 2009-10-15 11:19:55 -0400 (Thu, 15 Oct 2009) | 5 lines
    
    revert part of the kolab/issue: give the marcusbains QTimer a parent.
    fingers-crossed. for kolab/issue3903
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1041162 | tmcguire | 2009-10-27 14:22:03 +0000 (Tue, 27 Oct 2009) | 25 lines

Backport r1041074 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1036130 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1036130 | winterz | 2009-10-16 18:53:22 +0200 (Fri, 16 Oct 2009) | 14 lines
  
  Merged revisions 1036104 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1036104 | winterz | 2009-10-16 12:10:07 -0400 (Fri, 16 Oct 2009) | 7 lines
    
    Do not set any buttons as the default and put the initial focus
    in the event viewer area. This makes it harder for the user
    to inadvertantly press <enter> and dismiss a reminder.
    kolab/issue3807
    
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1041164 | tmcguire | 2009-10-27 14:23:28 +0000 (Tue, 27 Oct 2009) | 21 lines

Backport r1041051 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1034385 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1034385 | winterz | 2009-10-12 17:16:20 +0200 (Mon, 12 Oct 2009) | 10 lines
  
  Merged revisions 1034349 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1034349 | winterz | 2009-10-12 10:03:39 -0400 (Mon, 12 Oct 2009) | 3 lines
    
    also don't load the calendar in these summaries.
    MERGE: e4,trunk,4.3 (the sdsummarywidget fix only)
  ........
................


------------------------------------------------------------------------
r1041165 | tmcguire | 2009-10-27 14:26:47 +0000 (Tue, 27 Oct 2009) | 5 lines

Backport r1040468 by mlaurent from trunk to the 4.3 branch:

Fix mem leak


------------------------------------------------------------------------
r1041167 | tmcguire | 2009-10-27 14:27:58 +0000 (Tue, 27 Oct 2009) | 22 lines

Backport r1041068 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1035635 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1035635 | winterz | 2009-10-15 16:54:57 +0200 (Thu, 15 Oct 2009) | 11 lines
  
  Merged revisions 1035625 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1035625 | winterz | 2009-10-15 10:32:38 -0400 (Thu, 15 Oct 2009) | 4 lines
    
    With fancy headers, deal nicely with very long attachment names or descriptions.
    fixed kolab/issue3908
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1041168 | tmcguire | 2009-10-27 14:28:44 +0000 (Tue, 27 Oct 2009) | 5 lines

Backport r1041108 by mlaurent from trunk to the 4.3 branch:

Fix config name error


------------------------------------------------------------------------
r1041425 | djarvie | 2009-10-28 00:03:10 +0000 (Wed, 28 Oct 2009) | 2 lines

Disable 'New Alarm from Template' action when no alarm templates exist

------------------------------------------------------------------------
r1041772 | mkoller | 2009-10-28 13:45:57 +0000 (Wed, 28 Oct 2009) | 3 lines

backport bug 211327
correctly save the last selected message when leaving a folder 

------------------------------------------------------------------------
r1041911 | djarvie | 2009-10-28 20:03:25 +0000 (Wed, 28 Oct 2009) | 1 line

Clarification
------------------------------------------------------------------------
r1041973 | tmcguire | 2009-10-28 22:06:07 +0000 (Wed, 28 Oct 2009) | 5 lines

Backport r1041850 by tmcguire from trunk to the 4.3 branch:

Deal with broken HTML mails produced by apple mail.


------------------------------------------------------------------------
r1041976 | tmcguire | 2009-10-28 22:13:30 +0000 (Wed, 28 Oct 2009) | 9 lines

Backport r1041974 by tmcguire from trunk to the 4.3 branch:

Upps, don't hide attachments for broken mails from Apple Mail when prefering
plain text.
Now you see both plain text and HTML code, but there is no easy workaround for
that, for those broken mails.
When prefering HTML, they are displayed fine.


------------------------------------------------------------------------
r1042075 | scripty | 2009-10-29 04:21:10 +0000 (Thu, 29 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1042474 | cgiboudeaux | 2009-10-29 21:38:48 +0000 (Thu, 29 Oct 2009) | 1 line

Bump version.
------------------------------------------------------------------------
r1042477 | cgiboudeaux | 2009-10-29 21:41:11 +0000 (Thu, 29 Oct 2009) | 1 line

Bump version.
------------------------------------------------------------------------
r1042480 | cgiboudeaux | 2009-10-29 21:43:40 +0000 (Thu, 29 Oct 2009) | 1 line

Bump version.
------------------------------------------------------------------------
r1042487 | cgiboudeaux | 2009-10-29 21:53:31 +0000 (Thu, 29 Oct 2009) | 1 line

Bump version.
------------------------------------------------------------------------
r1042513 | djarvie | 2009-10-29 23:53:27 +0000 (Thu, 29 Oct 2009) | 3 lines

Bug 211696: in a dual screen system, show alarm window in other screen if the
active window is full screen.

------------------------------------------------------------------------
r1042553 | scripty | 2009-10-30 04:04:20 +0000 (Fri, 30 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
