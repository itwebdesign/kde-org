---
title: "KDE 1.1 to 1.1.1 Changelog"
hidden: true
---

<CENTER>
<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH="570" >
<tr>
<td> <font size="+1"> <!--VAR COLS=60 ROWS=4--><!--EMPTY--><!--/VAR--> </font>
This page tries to present as much as possible of the very nummerous problem
corrections and feature additions occurred in KDE between version 1.1 and the
current, 1.1.1. The changes descriptions were kept brief. The main
characteristic of the present release is stability, rock solid stability.
Thanks to all involved. Thanks to the users for their great moral support.

<br />
<br />
</td>
</tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdeadmin
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>
<!--

    <TR><TD>

<TABLE BORDER=0 CELLSPACING=0 WIDTH="570">
<TR>
  <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
  <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdat

</font></B></TD>

  <TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sean Vyain</font></b></i></td>
</tr>
    <TR >
  <TD></TD>
  <td colspan="2" align="LEFT" valign="CENTER">
<p>A backup tool with GUI
<ul>

</ul>
  </TD>
</TR>
</TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ksysv

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Peter Putzer</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

-->
<a name="kuser
"></a>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kuser

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Denis Pershin</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>
Removed because of misleading messages possibly leading to system damage.
Fixing the messages would have required every translator to update the translated messages,
which was not possible due to lack of time.
</TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdebase
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>
<!--

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kappfinder

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Hoelzer</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

KPlayAudio

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcheckpass

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<ul>

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

-->

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcontrol

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>collective</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>geometry layout improved in kcmkwm/Buttons tab
<li>fix an enabling handle on kcmkwm/Advanced tab
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdehelp

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Martin Jones</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p><ul>

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>
-->

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Steffen Hansen, Matthias Ettrich</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>fixed X vs. gpm race condition
<li>allow \$'s in show/non-show user lists
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kfm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Torben Weis, David Faure, ...</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li> Security fix when starting applications (making sure standard file descriptors aren't closed) (Thanks to Dirk A. Mueller &lt;dmuell@gmx.net&gt;)
<li> Support for <b>IE favourites files</b> in the bookmark <br />(Thanks to Sven Dowideit &lt;svenud@ozemail.com.au&gt;)
<li> <b>"Volume Management" support for Solaris 2.x</b> <br />(Thanks to Torsten Kasch &lt;tk@Genetik.Uni-Bielefeld.DE&gt;)<br />
Usage : place a Device.kdelnk on your desktop with device entries like /dev/diskette0, /dev/diskette1,
/dev/dsk/c0t6d0s2 (for SCSI CD-ROM drives at controller 0, SCSI-ID 6)
<li> <b>Passive mode transfer</b> used for FTP, if available (enables FTP for users behind a firewall) <br />(Thanks to Peter Gruber &lt;peter.gruber@stud.uni-regensburg.de&gt;)
<li> Support for <b>Windows NT FTP server</b> (IIS), any NT version. <br />(Thanks to Jens Kristian S&oslash;gaard &lt;jk@soegaard.net&gt;)
<li> Fixed encoded paths for FTP with FTP proxy
<li> Detect if kfm is already running, to prevent bugs happening with two kfm running
<li> Fixed files not being removed from /tmp in some cases <br />(Thanks to Dirk A. Mueller &lt;dmuell@gmx.net&gt;)
<li> Fixed cache bugs (duplicates, warning on startup)
<li> Fixed mime detection on very small files<br />(Thanks to Stan Covington &lt;stan@calderasystems.com&gt;)
<li> Fixed popupmenu displaying both entries when a global and a local kdelnk exist for an application
(and allow to disable a global applnk bound to some mimetypes)
<li> Some menu items moved, no more cache menu : options in the options menu, show history and show cache
moved with Applications and Mimetypes.
<li> Fixed 'Show HTML' setting not restored (by session management and local properties)
<li> Fixed "renaming a link to a directory renamed the directory itself" (!)
<li> Fixed another dangerous bug : moving something and having no right to write to the destination, the source was deleted anyway (if selecting "Continue")
<li> Fixed red menu bar (for root) turning back to gray
<li> Fixed device icon not updated when device mounted/unmounted (if icon in a kfm window)
<li> (HTML widget) ; fixed displaying of entities missing trailing ';' (bad html)
<li> Fixed crash on redirection to invalid URL
<li> Fix for users setting \$HOME with a trailing slash
<li> Re-enable output of launched apps to be seen in the terminal (was a mis-guided attempt at fixing the security bug explained in the first item)
<li> Removed debug output (as was done in the rest of KDE)
<li> Fixed User-Agent handling problems.
<li> Fixed some problems related to cookies.
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kikbd

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Alexander Budnik (maintained by C. Tibirna)</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN=3>
<p><ul>
<li>The following keyboards were added: International English
(all accented characters on a normal US keyboard),
Icelandic, French Swiss, German Swiss, Dvorak, Croatian,
Italian, Spanish-Latin American, Thai, Romanian,
Portuguese, Portuguese with supplementary accents,
Hellenic-Latin1, Hellenic-Latin7. Many
thanks to the numerous willful contributors
<li>The window creation event grab (which was buggy thus
rendered kikbd unusual occasionally) is fixed now. Big
big gratitude to Dimitrios Bouras for tracking down the
bug and then fixing it.
<li>added support for extended Compose key, necessary
for a few complex key maps (like greek-latin7). Thanks
to Dimitrios Bouras
<li>occasional crash on Solaris caused by event handling
problems is fixed
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmenuedit

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christoph Neerfeld</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p><ul>

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

konsole

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Lars Doelle</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

-->

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kpanel

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich and collab.</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>background pixmap automatically rotated 90 degrees when
panel changes orientation
<li>Panel clock is now more accurate and reliable
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

krootwm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich and collab.</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p><ul>

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>
-->

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kscreensaver

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Martin Jones and collab.</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>Area under password dialog redrawn/cleared to background color
<li>Small screensaver window bug fixed
<li>Detects large variation in time and resets timeout (e.g. APM suspends).
<li>Screensaver does not lock if the mouse or keyboard has been grabbed.
<li>Screensaver window will always stay on top.
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kstart

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>
-->

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kvt

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Maintainer: <b><font>Leon Widdershoven</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
Some bugs have been fixed, especially GUI related bugs. Since this program is being replaced by <em>konsole</em>, no
new features have been added.
</TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kwm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>manual placement now places the window under the mouse
cursor and doesn't warp cursor anymore.
<li>Bugfix for GTK 1.2.x windows pop up iconified (i.e. Gimp)
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kwmcom

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p><ul>

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>


<TR>
<TD>
<TABLE BORDER=0 CELLSPACING=0 WIDTH="570"  >
  <TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>kpager</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Antonio Larrosa</font></b></i></td></tr>
  </TR>
  <TR >
    <TD ></TD>
    <TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

      </ul>
      </TD>
  </TR>
</TABLE>
</td>
</TR>
<tr>
<TD>&nbsp;</TD>
</tr>


<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdegames
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>
<tr><td>All the games were upgraded to KDE-1.1. There are no significant additions, but a lot of bugfixes were performed</td></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdegraphics
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kview

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj Singh Kang</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>


<tr><TD>&nbsp;</TD></tr>
-->

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdelibs
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kprocess

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Waldo Bastian</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul><li>Sometimes the termination of a child process was missed: Fixed.
<li>restore SIGPIPE handler in spawned child processes.
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kquickhelp

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Mario Weilguni</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>Fixed crash on HP-UX and NetBSD systems
</ul></TD></TR></TABLE>
</td></TR>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

mediatool library

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font
color="#ffffff">Christian Esken</font></b>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>Fixed a security problem - a temp race
that allowed overwriting other users files.
Thanks to
<a href=mailto:typo@inferno.tusculum.edu>Paul Boehm</a> of the
<a href="http://www.hert.org">HERT</a> group,
who reported this issue to KDE.
</ul></TD></TR></TABLE>
</td></TR>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

khtmlw

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Martin Jones and Waldo Bastain</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul><li>Various rendering improvements and bug fixes
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdemultimedia
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmid

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Antonio Larrosa</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li>Support for 2.2.x Linux kernels sound drivers
<li>Fixes for a too high pitch of AWE cards
<li>Some workarounds for compiler bugs
<li>Better support for broken karaokes
<li>Other bugfixes
<li>Raised version to 1.7
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>
-->

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmidi

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<li>Bugfix for Prev and Next buttons (thanks to John Birch <jb.nz@writeme.com>)
<li>Timidity fix from Martin Weghofer

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kscd

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<li>Fixed length of last title
<li>SCO Openserver 5.0 and SONY CDU-76S support contributed by Dag Nygren <dag@newtech.fi>
<li>Prevent crash if CDDB directory is empty (thanks to Petter Reinholdtsen)

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmix

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p><ul>

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

-->
<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdenetwork
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmail

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stefan Taferner, Markus Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Changes since 1.1<p>
<li> Displays number of unread messages in each folder (DS)
<li> You can view image attachments inline (SR)
<li> You can view (non-inline) attachments directly in kmail (for message, image,
text and html types) (SR)
<li> Encrypted pop password in config file (bug 884) (ST, from MR)
<li> Enhanced interaction with kfm (JK)
<li> Multipart/Alternative handling (not perfect, still) (SR)
<li> Updated documentation (ST)
<li> Improved readability when using a light text on dark background colour
scheme (DS)
<li> Improved return and arrow key navigation/completion in composer (SR)
<li> HTML widget honours the selected font size (it uses it as base font size) (SR)
<li> Context sensitive menus in message list and folder list. (bug 1016) (SR)
<li> KMail removes Bcc header field before sending messages (and adding it afterwards). (ST)

<p>Bugfixes since 1.1<p>
<li> Inline html attachments: strip &lt;/body&gt; and &lt;/html&gt; from end of html attachment
to allow optional further attachments to be displayed. (SR)
<li> PGP memory leak fixed (WA)
<li> Fixed broken composer: message bodies did not show up upon reply. (ST)
<li> Fix against empty header fields (ST)
<li> More robust behavior in case of disaster: Never send signal to pid 0 = don't
crash whole KDE. (bug 1140) (SR)
<li> Fixed Drag and Drop to folders (bugs  614, 1019 and 941) (DS)
<li> Ignore any sub-directory found in Mail directory. (SR)
<li> Use paths.h if found, and delete critical lock files with unlink. (SR, thanks to Harri P)
<li> No more messages that stubbornly remain unread (bug 656) (DS)
<li> Stopped the checking mail dialog from taking the focus. (DS)
<li> Stopped the current message from getting scrolled to the top when (interval) mail
checking fires. (DS)
<li> Sort the current folder when new mail arrives, so that new mail isn't always
appended to the end of the current folders message list window. (DS)
<li> Bugfix against pgp-thinks-very-long-before-starting-bug (LK)
<li> Fixed #179. KMail checks now if message is delivered to the folder
      before deleting it from the spool/pop server. (SR)
        <li> Added a semaphore to not start interval checking when already an interval check is running. (ST)
  </TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

knu

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bertrand Leconte</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p><ul>
    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>
-->

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kppp

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Mario Weilguni, Harri Porten, Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
<li> fixed helper process crash occurring when binaries were
compiled with the NDEBUG switch (SuSE, Caldera).
<li> fixed bug causing occasional "Timeout expired" errors
(thanks to Ian Gordon).
<li> solved alignment problems on Alpha architectures (thanks
to Martin Ostermann).
<li> improved parsing of modem data and therefore the detection
of the connect rate.
<li> support of pppd 2.3.6 and pppd 2.3.7.
<li> `lock' in /etc/ppp/options will be ignored.
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

krn

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Roberto Alsina</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ksirc

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Andrew Stanley-Jones</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ktalkd

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>David Faur&eacute;</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdenonbeta
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>
<tr><td>This section isn't yet released with the others KDE packages. Although, many distributors prefer to distribute this little gem too, given the excellent code found in there. The applications included will hopefully reach a more stable status soon, and thus will be included in the main packages.
</td></tr>


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcharselect

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Reginald Stadlbauer</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>




    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcmbind

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Karl Backstr&ouml;m, Olof Wolgast</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcmdhcpd

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Karl Backstr&ouml;m</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcmkpanel2

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Pietro Iglio</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcrontab

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Michael Bialas</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kexpress

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Peter Wichert</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kglchess

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Johannes Wuebben</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kgnuplot

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Johannes Wuebben</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmail2

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stefan Taferner</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p>The next generation KMail. Still in alpha phase.
</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

knetmon

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Alexander Neundorf</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

korn

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj Singh Kang</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kpackage

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Toivo Pedaste</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kpanel2

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Pietro Iglio</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p>

<ul>
    <li>KPanel2 is a major clean up of KPanel source code. Among the
    added features there is <b>full drag'n'drop support</b> (user can
    drag icons from the K-menu to the panel, move and delete menu
    entries, move/copy files using KDiskNavigator, etc.) and
    <b>contextual menus</b> on menu entries.
    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kthememgr

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stefan Taferner</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kticker

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Paul Dwerryhouse</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

libkmedia

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

libkmsg

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stefan Taferner</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

maudio2

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Christian Esken</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

webmaker

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Alexei Dets</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdesdk
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kappgen

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Hoelzer</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kapptemplate

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Kurt Granroth</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdedoc

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj S. Kang, Torben Weis</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p>Documentation tools
</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdepalettes

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Daniel M. Duley</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kdesgmltools

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Panayotis Vryonis, Bernd Wuebben</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kexample

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font></font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ktranslator

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Andrea Rizzi</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

scripts

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj Singh Kang</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdesupport
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

QwSpriteField

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Warwick Allison</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>


<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdetoys
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kmoon

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Stephan Kulow</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kworldwatch

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias H&ouml;lzer-Kl&uuml;pfel</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>



    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

mouse

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Armen Nakashian</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr> -->

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
kdeutils
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>
<!--

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ark

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Robert Palmbos</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>Replaces the old KZip program. The change was needed because of
a name conflict with a system tool on FreeBSD

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kab

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Mirko Sucker</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

karm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Sirtaj Singh Kang</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p><ul>
</ul></TD></TR></TABLE>
</td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kcalc

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<p>A notable fix for statistic calculations was made.

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>
-->

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kedit

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<li>Use "Save As..." on "Untitled" documents (was broken in
non-English mode)
<li>Fixed background colors of dialogs (thanks to Alex Hayward)
<li>Fixed printing of documents with spaces in their file name

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

knotes

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Bernd Wuebben</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<li>fixed crashes and possible data loss
<li>docking is optional
<li>fixed session management

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

klipper

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font></font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    </TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>
-->

<a name="kpm"</a>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kpm

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Mario Weilguni</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
<li>It's again possible to dock it into panel
<li>Fix crash when using Linux >= 2.2.2
<li>Fix for showing wrong values for process time sometimes
<li>Fix for crash when right-clicking into window

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>New</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

ktop

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>collective, Chris Schlaeger</font></b></i></td></tr>

<TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">

KTop is the new KDE Task Manager. It displays the processes running on
a computer in list and tree form. Processes can be killed and various
other signals can be send. It also features a processor load and
memory usage graph. On Linux SMP systems a load graph will be
displayed for each processor.

</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<!--


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

kwrite

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Jochen Wilhelmy</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
</TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
klyx
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>


    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

&nbsp;

</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Matthias Ettrich and collab.</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p><ul>
    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>

<tr><TD>&nbsp;</TD></tr>

<TR><TD align="center" BGCOLOR="#000000" ><b><font color="white">
korganizer
</font></b></td></TR><tr><TD>&nbsp;</TD></tr>

    <TR><TD><TABLE BORDER=0 CELLSPACING=0 WIDTH="570"><TR>
    <TD WIDTH="100" BGCOLOR="#6495ED" ><I>Update</I></TD>
    <TD WIDTH="170" BGCOLOR="#6495ED" ><B><font>

&nbsp;
</font></B></TD><TD WIDTH="300" BGCOLOR="#6495ED"><i>Author: <b><font>Preston Brown and collab.</font></b></i></td></tr>
    <TR><TD ALIGN=LEFT VALIGN=CENTER COLSPAN="3">
    <p><ul>
    </ul></TD></TR></TABLE>
    </td></TR><tr><TD>&nbsp;</TD></tr>
-->

</TABLE></CENTER>
