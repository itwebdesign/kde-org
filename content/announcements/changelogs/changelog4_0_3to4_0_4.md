---
title: "KDE 4.0.4 Changelog"
hidden: true
---

  <h2>Changes in KDE 4.0.4</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_0_4/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Optimizations:</em><ul>
        <li class="optimize">Make KAnimatedButton (used for e.g. Konqueror cogwheel) cache and reuse pixmaps for every
            frame, to avoid Qt keeping hundreds of duplicates of each in the global pixmap cache, reducing X pixmap
            usage considerably.  See SVN commit <a href="http://websvn.kde.org/?rev=793672&amp;view=rev">793672</a>. </li>
      </ul>
      </div>
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Rework our form widgets' baselines to align on the text content, rather than on the widget boundaries. See SVN commit <a href="http://websvn.kde.org/?rev=799845&amp;view=rev">799845</a>. </li>
          <li class="improvement">Improve CSS3 UI cursor support. See SVN commit <a href="http://websvn.kde.org/?rev=794168&amp;view=rev">794168</a>. </li>
          <li class="improvement">Support DOM3 wholeText and replaceWholeText(). See SVN commit <a href="http://websvn.kde.org/?rev=799256&amp;view=rev">799256</a>. </li>
      </ul>
      <em>Optimizations:</em><ul>
        <li class="optimize">Optimized implementation of textarea's setText for common case where  new content begins with a substring of the old content. See SVN commit <a href="http://websvn.kde.org/?rev=799843&amp;view=rev">799843</a>. </li>
      </ul>
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Rework the tracking of stylesheets' loaded state, preventing it from losing synchronization in some cases involving @import. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=155493">155493</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=793180&amp;view=rev">793180</a>. </li>
        <li class="bugfix ">Properly close DOM-created nodes. Fixes CNN.com election tracker, and 3 Acid3 tests. See SVN commit <a href="http://websvn.kde.org/?rev=792683&amp;view=rev">792683</a>. </li>
        <li class="bugfix ">Make searching within textareas work again. See SVN commit <a href="http://websvn.kde.org/?rev=794973&amp;view=rev">794973</a>. </li>
        <li class="bugfix ">Fix accesskey activation, and autoscroll suspension. See SVN commit <a href="http://websvn.kde.org/?rev=794169&amp;view=rev">794169</a>. </li>
        <li class="bugfix ">Make vertical alignment of inline-tables and inline-blocks comply with requirements of CSS 2.1 - 10.8.1. See SVN commit <a href="http://websvn.kde.org/?rev=799842&amp;view=rev">799842</a>. </li>
        <li class="bugfix ">Fix absolutely positioned static elements being misplaced and drawn on top of the border when they are the only child of a block. See SVN commit <a href="http://websvn.kde.org/?rev=799844&amp;view=rev">799844</a>. </li>
        <li class="bugfix ">Fix solving of percentage height for positioned elements, in light of the nota bene in CSS 2.1-10.5. Satisfyingly, the same code path can also be used to solve percentage intrinsic height (10.6.2) Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160371">160371</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=799846&amp;view=rev">799846</a>. </li>
        <li class="bugfix ">Fix some errors in the implicit height code, that wasn't converting to
 content height. See SVN commit <a href="http://websvn.kde.org/?rev=799846&amp;view=rev">799846</a>. </li>
        <li class="bugfix ">Make sure we always use the padding edge when calculating
the width of a positioned object's containing block. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=137606">137606</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=799848&amp;view=rev">799848</a>. </li>
        <li class="bugfix ">Fix createDocument() to work as specified in DOM Level 2: Use the user-supplied doctype and set its Node.ownerDocument attribute to the document being created. See SVN commit <a href="http://websvn.kde.org/?rev=793896&amp;view=rev">793896</a>. </li>
        <li class="bugfix ">Let a redirect from within a javascript: frame source win out over the text. See SVN commit <a href="http://websvn.kde.org/?rev=794482&amp;view=rev">794482</a>. </li>
        <li class="bugfix ">Properly update currentTarget for events in capture phase. See SVN commit <a href="http://websvn.kde.org/?rev=792734&amp;view=rev">792734</a>. </li>
        <li class="bugfix ">Fix handling of events with custom names. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=133887">133887</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=800710&amp;view=rev">800710</a>. </li>
        <li class="bugfix ">Fix update of :empty pseudo-class when text content changes. See SVN commit <a href="http://websvn.kde.org/?rev=799272&amp;view=rev">799272</a>. </li>
        <li class="bugfix ">Properly fall back to alternate content when doing delayed part creation
due to needing to lookup the mimetype. See SVN commit <a href="http://websvn.kde.org/?rev=794519&amp;view=rev">794519</a>. </li>
        <li class="bugfix ">Fix handling of data: URLs in object/embed nests. See SVN commit <a href="http://websvn.kde.org/?rev=792682&amp;view=rev">792682</a>. </li>
        <li class="bugfix ">Fix return value of replaceChild in the ES bindings. See SVN commit <a href="http://websvn.kde.org/?rev=791907&amp;view=rev">791907</a>. </li>
        <li class="bugfix ">Fix up the full-blown Attr codes to properly take care of their kids. See SVN commit <a href="http://websvn.kde.org/?rev=791908&amp;view=rev">791908</a>. </li>
        <li class="bugfix ">Rework sizing of textarea so that it works in all styles. See SVN commit <a href="http://websvn.kde.org/?rev=799843&amp;view=rev">799843</a>. </li>
        <li class="bugfix ">Fix a bug in cost accounting in the tile caches. See SVN commit <a href="http://websvn.kde.org/?rev=794790&amp;view=rev">794790</a>. </li>
        <li class="bugfix ">Don't crash when dispatching some mouse events on documents created via createDocument(). See SVN commit <a href="http://websvn.kde.org/?rev=799153&amp;view=rev">799153</a>. </li>
      </ul>
      </div>
      <h4><a name="kjs">kjs</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Activate JS compat mode in upcoming PCRE 7.7, when present. See SVN commit <a href="http://websvn.kde.org/?rev=796160&amp;view=rev">796160</a>. </li>
        <li class="bugfix ">Provide a bit of file and line number with RegExp parse errors.  See SVN commit <a href="http://websvn.kde.org/?rev=798738&amp;view=rev">798738</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_0_4/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Correctly enable the "Print" menu action. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160173">160173</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=792339&amp;view=rev">792339</a>. </li>
        <li class="bugfix ">Return the correct tooltip after the title of a bookmark is edited. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160189">160189</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=792389&amp;view=rev">792389</a>. </li>
        <li class="bugfix ">Update accordingly when a bookmark for the currently open document is removed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160190">160190</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=792392&amp;view=rev">792392</a>. </li>
        <li class="bugfix ">Do not consider annotation comments as potentially rich text. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160306">160306</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=793231&amp;view=rev">793231</a>. </li>
        <li class="bugfix ">Do not construct highlight annotations with no text inside. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160502">160502</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=794918&amp;view=rev">794918</a>. </li>
        <li class="bugfix ">Do not end up in an infinite loop trying to get the best size. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160628">160628</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=795884&amp;view=rev">795884</a>. </li>
        <li class="bugfix ">Do not unexpectly switch to the following page when setting a new page via click on link in presentation mode (make the page view smarter in detecting events). See SVN commit <a href="http://websvn.kde.org/?rev=797550&amp;view=rev">797550</a>. </li>
        <li class="bugfix ">Small HIG fixes in the annotation properties dialog. See SVN commit <a href="http://websvn.kde.org/?rev=798697&amp;view=rev">798697</a>. </li>
        <li class="bugfix ">Comicbook backend: correctly free the resources when closing a document, so we do not crash when opening particular sequences of comicbooks (eg cbz -&gt; cbr -&gt; cbz). See SVN commit <a href="http://websvn.kde.org/?rev=798869&amp;view=rev">798869</a>. </li>
        <li class="bugfix ">ODT, FictionBook and Plucker backends: clear document information and TOC, so opening a new document does not show possible information of the previous. See SVN commits <a href="http://websvn.kde.org/?rev=798891&amp;view=rev">798891</a>, <a href="http://websvn.kde.org/?rev=798892&amp;view=rev">798892</a>, <a href="http://websvn.kde.org/?rev=798898&amp;view=rev">798898</a> and <a href="http://websvn.kde.org/?rev=799078&amp;view=rev">799078</a>. </li>
        <li class="bugfix ">Calculate correctly the amount of memory, so the preloading should work again. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=153675">153675</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=801781&amp;view=rev">801781</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeedu"><a name="kdeedu">kdeedu</a><span class="allsvnchanges"> [ <a href="4_0_4/kdeedu.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://edu.kde.org/klettres" name="klettres">KLettres</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix Arabic support. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160482">160482</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=794517&amp;view=rev">794517</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_0_4/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://www.konqueror.org/" name="konqueror">konqueror</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix shellcmdplugins and remoteencoding plugins. Make it work with dolphinpart. See SVN commit <a href="http://websvn.kde.org/?rev=797245&amp;view=rev">797245</a>. </li>
      </ul>
      </div>
      <h4><a name="klipper">Klipper</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix crash at logout in some cases. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160634">160634</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=796437&amp;view=rev">796437</a>. </li>
      </ul>
      </div>
      <h4><a name="ksmserver">ksmserver</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Unbreak suspend/hibernate on logout (rh#442559). See SVN commit <a href="http://websvn.kde.org/?rev=800176&amp;view=rev">800176</a>. </li>
      </ul>
      </div>
      <h4><a name="ksystraycmd">ksystraycmd</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Clicking on tray icon hides/show application. See SVN commit <a href="http://websvn.kde.org/?rev=800238&amp;view=rev">800238</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="allsvnchanges"> [ <a href="4_0_4/kdegames.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://games.kde.org" name="libkdegames">libkdegames</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Use KDE username in KScoreDialog if no default is provided. See SVN commit <a href="http://websvn.kde.org/?rev=802171&amp;view=rev">802171</a>. </li>
        <li class="bugfix ">Allow negative scores in KScoreDialog. See SVN commit <a href="http://websvn.kde.org/?rev=802175&amp;view=rev">802175</a>. </li>
        <li class="bugfix ">Correctly use the last used username in KScoreDialog. See SVN commit <a href="http://websvn.kde.org/?rev=802177&amp;view=rev">802177</a>. </li>
        <li class="bugfix ">Provide an 'Ok' button in KScoreDialog to confirm name entry. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=157973">157973</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=802177&amp;view=rev">802177</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_0_4/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kopete">Kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Create a user writable directory for chat style installation if none exists yet to enable installation of custom chat styles. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160590">160590</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=798901&amp;view=rev">798901</a>. </li>
        <li class="bugfix ">Ensure that the text input field has focus in the chat window so that e.g. pasting text always works when the chat window has focus. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=158371">158371</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=799085&amp;view=rev">799085</a>. </li>
        <li class="bugfix ">Enable optional saving of a password before connecting. This way the repeated questions for a password (if set incorrectly in the configuration dialog) can be avoided as well. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=157697">157697</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=799108&amp;view=rev">799108</a>. </li>
        <li class="bugfix ">Fix the latex script for dash based systems (like *buntu). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=135997">135997</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=799199&amp;view=rev">799199</a>. </li>
      </ul>
      </div>
      <h4><a name="krdc">KRDC</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Respect the screen resolution of the screen KRDC is running, not the first one. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160853">160853</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=797826&amp;view=rev">797826</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_0_4/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Improvements:</em>
      <ul>
          <li class="improvement">Make 'Search' the default action in keyserver dialog instead of 'Close'. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=160930">160930</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=803050&amp;view=rev">803050</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebindings"><a name="kdebindings">kdebindings</a><span class="allsvnchanges"> [ <a href="4_0_4/kdebindings.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="pykde">PyKDE</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fixes the problem where menu item text doesn't appear when using the Oxygen style. See SVN commit <a href="http://websvn.kde.org/?rev=797452&amp;view=rev">797452</a>. </li>
        <li class="bugfix ">Updated the sip files; fixes crashes with KConfig and related classes. See SVN commit <a href="http://websvn.kde.org/?rev=793509&amp;view=rev">793509</a>. </li>
      </ul>
      </div>
    </div>
