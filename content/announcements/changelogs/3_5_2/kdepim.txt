2006-01-20 17:56 +0000 [r500607]  tilladam

	* branches/KDE/3.5/kdepim/kontact/plugins/kpilot/kpilot_plugin.cpp:
	  Forward port of: SVN commit 500605 by tilladam: Make sure the
	  translation catalogue is loaded, by making sure we pass our
	  correct name to the Kontact::Plugin base class. (proko2 issue
	  1087)

2006-01-21 10:26 +0000 [r500763]  tilladam

	* branches/KDE/3.5/kdepim/kmail/configuredialog.cpp: Backport of:
	  SVN commit 500626 by tilladam: Make sure that latin1 is selected
	  as a default if we find no matching locale for the fallback
	  codec, and replace "iso " with "iso-" so we have a better chance
	  of finding the locale.

2006-01-21 12:34 +0000 [r500799]  tilladam

	* branches/KDE/3.5/kdepim/kmail/configuredialog.cpp: It's called
	  latin9, not latin1. Thanks to Nicolas Goutte for pointing out the
	  error of my ways.

2006-01-22 14:14 +0000 [r501239]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/eventlistviewbase.cpp,
	  branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/kalarm.h: Bug 120539: Fix column
	  widths when main window is resized, if columns have been
	  reordered

2006-01-22 15:01 +0000 [r501278]  dkukawka

	* branches/KDE/3.5/kdepim/kmail/kmmainwin.rc,
	  branches/KDE/3.5/kdepim/kmail/kmail_part.rc: backported from
	  trunk: * removed 'import/KMailCVT' entry from the tool menu.
	  After discussion with Ingo Klöcker this should be only in 'File'
	  menu

2006-01-22 16:34 +0000 [r501321]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/Changelog: Add KDE 3.5.0 comment

2006-01-24 21:55 +0000 [r502116-502113]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/kalarm.h: Increment version number

	* branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/kalarmd/alarmdaemon.cpp: Fix
	  kalarmd hang when triggering late alarm and KAlarm run mode is
	  on-demand

2006-01-24 22:00 +0000 [r502119]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/mainwindow.cpp: Use isEmpty()
	  instead of !count()

2006-01-27 18:03 +0000 [r502982]  lunakl

	* branches/KDE/3.5/kdepim/korganizer/kodialogmanager.cpp: - Add a
	  new TODO. - Click Select Categories - Click Edit Categories - Hit
	  Esc - focus goes to the mainwindow instead of the previous
	  dialog, because this dialog is created at startup with the
	  mainwindow as the parent and reused, for a reason I fail to see.
	  So work it around by manually resetting the parent window hint
	  and depending on KWin guessing what the parent is.

2006-01-29 21:59 +0000 [r503679]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/alarmevent.h,
	  branches/KDE/3.5/kdepim/kalarm/daemon.cpp,
	  branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/alarmcalendar.cpp,
	  branches/KDE/3.5/kdepim/kalarm/daemon.h,
	  branches/KDE/3.5/kdepim/kalarm/functions.cpp,
	  branches/KDE/3.5/kdepim/kalarm/kalarmapp.cpp,
	  branches/KDE/3.5/kdepim/kalarm/kalarmd/alarmdaemoniface.h,
	  branches/KDE/3.5/kdepim/kalarm/kalarmd/adcalendar.cpp,
	  branches/KDE/3.5/kdepim/kalarm/kalarmd/alarmdaemon.cpp,
	  branches/KDE/3.5/kdepim/kalarm/kalarmapp.h,
	  branches/KDE/3.5/kdepim/kalarm/alarmevent.cpp,
	  branches/KDE/3.5/kdepim/kalarm/kalarmd/adcalendar.h,
	  branches/KDE/3.5/kdepim/kalarm/kalarmd/alarmdaemon.h: Notify
	  daemon by DCOP that alarm has been processed: to prevent alarm
	  loss, and to prevent defunct kalarm processes when run mode is
	  on-demand.

2006-01-31 17:05 +0000 [r504290]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/document.cpp,
	  branches/KDE/3.5/kdepim/akregator/ChangeLog: RSS parser: ignore
	  unknown or invalid version attribute value in the <rss> tag and
	  just assume RSS 2.0. The older formats are compatible to 2.0, so
	  this should work. (at least better than refusing to parse the
	  feeds) BUG: 118793

2006-02-02 15:25 +0000 [r504925]  dfaure

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp: Ensure
	  layouts get activated, the safe way.

2006-02-03 19:21 +0000 [r505380]  jriddell

	* branches/KDE/3.5/kdepim/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdepim
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdepim

2006-02-04 11:04 +0000 [r505555]  vkrause

	* branches/KDE/3.5/kdepim/knode/articlewidget.cpp: Backport SVN
	  commit 505553 by vkrause for KDE 3.5.2: Don't skip empty lines
	  when displaying PGP signed messages. BUG: 121239

2006-02-05 09:51 +0000 [r505887]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp: Patch
	  by David Faure. Disconnect scrollbar timer to avoid the
	  scrollview to overwrite our control of the scrollbars. This
	  enables us to get rid of the dangerous qapp->processEvents()
	  call.

2006-02-05 09:56 +0000 [r505888]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttView.cpp,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttMinimizeSplitter.h,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttView.h: Make sizeHint()
	  and minimumSizeHint() virtual so that subclasses can reimplement.
	  Note that KDGantView::sizeHint() will now be called, AFAICS it
	  was never called earlier. (This is needed by kplato to avoid
	  koshell to have it's minimumSize set to an intollerable high
	  value.)

2006-02-06 19:49 +0000 [r506425]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/kalarmui.rc: Remove clutter from
	  toolbar

2006-02-07 08:33 +0000 [r506600]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/templatedlg.cpp,
	  branches/KDE/3.5/kdepim/kalarm/daemon.cpp,
	  branches/KDE/3.5/kdepim/kalarm/eventlistviewbase.cpp,
	  branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/eventlistviewbase.h,
	  branches/KDE/3.5/kdepim/kalarm/mainwindow.cpp: Add Select All and
	  Deselect actions for main window and template dialogue

2006-02-07 18:09 +0000 [r506840]  winterz

	* branches/KDE/3.5/kdepim/kontact/plugins/korganizer/summarywidget.cpp:
	  Fix sorting in the appointments summary. This bug was already
	  fixed in trunk, but I forgot to backport. KDE 3.5.2 will have the
	  fix. BUGS: 120765

2006-02-07 20:50 +0000 [r506895]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/kalarm.h: Update version number

2006-02-08 07:34 +0000 [r507012]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.h,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp: Use a
	  singleshot timer for scrollbar update like QScrollView.

2006-02-08 15:39 +0000 [r507113]  toivo

	* branches/KDE/3.5/kdepim/kresources/newexchange/exchangeconvertercalendar.cpp:
	  Fix simple alarms when using exchange backend

2006-02-08 17:09 +0000 [r507159]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.h,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttView.cpp,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp: We
	  need to update the horizontal scrollbar ourselves now when we
	  have disconnected the QScrollView scrollbar timer. (To get rid of
	  the prosessEvents() call)

2006-02-09 07:07 +0000 [r507346]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp: Limit
	  'autoscroll' to vertical scrollbar.

2006-02-09 12:13 +0000 [r507507]  jriddell

	* branches/KDE/3.5/kdepim/korganizer/korgac/korgac.desktop: Add
	  OnlyShowIn=KDE; to autostart files that won't interest non-KDE
	  users

2006-02-11 21:39 +0000 [r508481]  winterz

	* branches/KDE/3.5/kdepim/libkpimexchange/core/utils.cpp: apply
	  Ryan Fowler's patch (thanks Ryan!) that we hope fixes the
	  Exchange Calendar Plugin Time Offset bug. CCMAIL:
	  rwfowler@gmail.com

2006-02-11 22:39 +0000 [r508499]  burghard

	* branches/KDE/3.5/kdepim/kmail/kmfoldercachedimap.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmfolderimap.cpp: It is always nice
	  to initialize variables. This one might help with bugs like
	  121698.

2006-02-11 22:53 +0000 [r508503]  burghard

	* branches/KDE/3.5/kdepim/kmail/searchjob.cpp: Always construct
	  valid searches

2006-02-13 16:17 +0000 [r509069]  winterz

	* branches/KDE/3.5/kdepim/kresources/Makefile.am: Remove the
	  blogging resource, which was experimental anyway. approved by
	  Reinhold.

2006-02-13 17:38 +0000 [r509083]  tilladam

	* branches/KDE/3.5/kdepim/kmail/callback.cpp: Backport patch by
	  Philippe Rigault, which makes it possible to accept invitations
	  if one is not in the invitations To: list, now that the string
	  freeze is lifted somewhat. CCMAIL: 106594@bugs.kde.org

2006-02-13 19:27 +0000 [r509134]  tilladam

	* branches/KDE/3.5/kdepim/korganizer/kogroupware.cpp: Backport of:
	  SVN commit 506589 by tilladam: Don't circumvent the scheduler
	  when handling cancel messages, because otherwise proper
	  schedulingID conversion does not happen. (proko2 issue1109)

2006-02-13 19:39 +0000 [r509141]  tilladam

	* branches/KDE/3.5/kdepim/libkcal/scheduler.cpp: Backport of: SVN
	  commit 506590 by tilladam: Don't attempt to fake the schedulingID
	  in temporary scheduling messages, rather rely on the detection of
	  existing incidences which works ok, if the calling code actually
	  uses it, which it didn't, see previous commit, which led me to
	  wrongly do one conversion to many when trying to fix the cancel
	  code path a while back. (proko2 issue1109 second half)

2006-02-13 20:29 +0000 [r509156]  winterz

	* branches/KDE/3.5/kdepim/kmail/kmfoldertree.cpp: Back by popular
	  demand (votes), approved by Ingo and Till, we undo revision
	  450009. The "Delete Folder" menu selection is now restored. Also
	  added a separator above this selection so it stands out a little
	  better, as suggested by Ellen on the usability list. BUGS: 120024

2006-02-13 21:26 +0000 [r509179]  winterz

	* branches/KDE/3.5/kdepim/kmail/kmmainwidget.cpp: Stronger warning
	  messages when deleting folders. As suggested by Ellen and
	  discussed on kdepim-usability.

2006-02-14 18:48 +0000 [r509450]  djarvie

	* branches/KDE/3.5/kdepim/doc/kalarm/index.docbook: Add warning
	  about missing sound file dialog when built without aRts

2006-02-14 21:26 +0000 [r509489]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/prefdlg.cpp: Disable warn when
	  quitting option when its setting is irrelevant

2006-02-14 22:46 +0000 [r509520]  annma

	* branches/KDE/3.5/kdepim/doc/knode/index.docbook,
	  branches/KDE/3.5/kdepim/doc/knode/credits.docbook: fix 112499 in
	  3.5 branch CCBUG=112499

2006-02-15 07:54 +0000 [r509576]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/akregator_view.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/articlelistview.cpp: Fix
	  crash in View::slotNext/PrevUnreadArticle() Combined View: do
	  slotNext/PrevUnreadFeed() in slotNext/PrevUnreadArticle(), as
	  selecting the next article does not make much sense... (at least
	  not now) BUG: 121999

2006-02-15 08:15 +0000 [r509584]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/articlelistview.cpp: bah

2006-02-17 16:54 +0000 [r510640]  wstephens

	* branches/KDE/3.5/kdepim/kresources/groupwise/soap/gwjobs.cpp:
	  backport fix for crash when server didn't return any deltas

2006-02-18 14:44 +0000 [r510938]  toivo

	* branches/KDE/3.5/kdepim/kresources/newexchange/exchangeglobals.cpp:
	  Fix problem when recieving an error in the iterm listing from
	  Exchange

2006-02-20 11:40 +0000 [r511574]  dfaure

	* branches/KDE/3.5/kdepim/kmail/kmedit.cpp: Fix wrong foreground
	  color being used when "use default colors" is checked.

2006-02-20 19:44 +0000 [r511732]  brade

	* branches/KDE/3.5/kdepim/knotes/main.cpp: Now update the version.

2006-02-20 20:12 +0000 [r511743-511741]  brade

	* branches/KDE/3.5/kdepim/kontact/plugins/knotes/Makefile.am,
	  branches/KDE/3.5/kdepim/kontact/plugins/knotes/knotes_part.cpp:
	  Sorry, didn't mean to commit that.

	* branches/KDE/3.5/kdepim/kontact/plugins/knotes/knotes_part.cpp:
	  Now the proper commit: Fix style.

2006-02-20 20:15 +0000 [r511744]  burghard

	* branches/KDE/3.5/kdepim/kmail/kmreaderwin.h,
	  branches/KDE/3.5/kdepim/kmail/kmmainwidget.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmreaderwin.cpp: Do not try to
	  update an attachment when the source should be viewed. *sigh* I
	  know that this mAtmUpdate is an ugly thing.

2006-02-20 20:24 +0000 [r511747]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/knote.cpp: Now also fix #115009
	  for KDE 3.5.2. CCBUG: 115009

2006-02-20 22:18 +0000 [r511791]  kossebau

	* branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp: Fixing some
	  race conditions due to async addressbook loading, triggered by
	  commandline commandos and DCOP calls Okay'ed by Tobias BR:87233

2006-02-21 01:21 +0000 [r511839]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/akregator_part.cpp:
	  backport: fix small memleak

2006-02-21 09:47 +0000 [r511919]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/mk4storage/feedstoragemk4impl.cpp:
	  disable category support in the metakit backend, so it should
	  support about 500 instead of 340 feeds now with the default 1024
	  open files limit on most systems. CCMAIL: akregator-devel@sf.net

2006-02-21 10:15 +0000 [r511929]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp: Print
	  listview headers also.

2006-02-21 14:10 +0000 [r511997]  ewoerner

	* branches/KDE/3.5/kdepim/akregator/src/librss/article.cpp: Set
	  "rel" attribute default to "alternate", as requested by Atom
	  specification BUG:122409

2006-02-21 15:41 +0000 [r512022]  winterz

	* branches/KDE/3.5/kdepim/konsolekalendar/konsolekalendar.cpp: Fix
	  --next option. BUGS: 122370

2006-02-21 22:22 +0000 [r512145]  reitelbach

	* branches/KDE/3.5/kdepim/korganizer/korganizer.kcfg: Prevent
	  double colons in translations. One colon is automatically added
	  into the GUI, so remove the one in the label. This fixes part one
	  of bug 122417 CCBUG:122417

2006-02-22 08:11 +0000 [r512295]  mkelder

	* branches/KDE/3.5/kdepim/korn/kio.cpp,
	  branches/KDE/3.5/kdepim/korn/kio_delete.cpp,
	  branches/KDE/3.5/kdepim/korn/kio_subjects.cpp,
	  branches/KDE/3.5/kdepim/korn/kio.h,
	  branches/KDE/3.5/kdepim/korn/kio_count.cpp,
	  branches/KDE/3.5/kdepim/korn/kio_proto.h: This fixes a crash when
	  fetching messages. This patch removes the defaultPort-function
	  without arguments.

2006-02-23 14:11 +0000 [r512742]  wstephens

	* branches/KDE/3.5/kdepim/kmail/imapaccountbase.cpp: Fix
	  https://bugzilla.novell.com/attachment.cgi?id=32275&action=view -
	  crash when password dialog closed after kmail main window due to
	  wrong parent

2006-02-23 14:32 +0000 [r512746]  wstephens

	* branches/KDE/3.5/kdepim/kmail/imapaccountbase.cpp: create dialog
	  on the stack, as it's exec'ed - lesson #1042 from dfaure's
	  curriculum :)

2006-02-25 09:55 +0000 [r513389]  mkelder

	* branches/KDE/3.5/kdepim/korn/kornaccountcfgimpl.cpp: Disable the
	  interval input field if the selected protocol isn't pollable.
	  CCBUG: 122507

2006-02-25 11:10 +0000 [r513401]  tilladam

	* branches/KDE/3.5/kdepim/kmail/recipientspicker.cpp: Fix
	  regression apparently introduced by Bram's fix for #117118, if
	  the analysis in #121337 is not completely off. Based on a patch
	  by Kanniball <kanniball@zmail.pt>. Thanks. BUG: 121337 CCMAIL:
	  kanniball@zmail.pt CCMAIL: bramschoenmakers@kde.nl

2006-02-25 14:44 +0000 [r513455]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/akregator_part.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/akregator_part.h,
	  branches/KDE/3.5/kdepim/akregator/src/kcursorsaver.h (added):
	  Lock archive to avoid metakit archive corruption due to multiple
	  write access. Usually Akregator being a KUniqueApplication avoids
	  multiple Akregator instances accessing the archive, but there
	  seem to be some cases where KUniqueApp is not safe enough (right
	  after update, multiple KDE sessions, when using --nofork option).
	  So we lock the archive like KMail does: if locking fails, show an
	  error message to the user, giving him the possibility to either
	  continue without archive, or to force access. As this is a severe
	  bug (multiple write access can corrupt archives, which can make
	  Akregator crash at startup), I think making use of the message
	  unfreeze is justified. CCBUG: 116482 CCMAIL:kde-i18n-doc@kde.org
	  CCMAIL:akregator-devel@lists.sf.net

2006-02-26 11:51 +0000 [r513748]  tilladam

	* branches/KDE/3.5/kdepim/kaddressbook/features/resourceselection.cpp:
	  Disconnect by sender, rather than by slot, since that actually
	  works.

2006-02-26 17:58 +0000 [r513843]  tilladam

	* branches/KDE/3.5/kdepim/libkcal/scheduler.cpp: Don't crash when
	  non-existing incidences are canceled.

2006-02-28 09:51 +0000 [r514398]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttView.cpp,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp:
	  Horizontal scrollbar was not always updated correctly after scale
	  and zoom, due to the disconnection of the QScrollView update
	  scrollbar timer.

2006-03-02 07:53 +0000 [r514974]  aseigo

	* branches/KDE/3.5/kdepim/kmail/kmkernel.h,
	  branches/KDE/3.5/kdepim/kmail/kmmainwidget.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmmainwidget.h,
	  branches/KDE/3.5/kdepim/kmail/kmkernel.cpp: fix online/offline
	  status: make it work with multiple windows, harmonize the wording
	  with the buttons in the confirmation dialog

2006-03-02 12:26 +0000 [r515022]  coolo

	* branches/KDE/3.5/kdepim/kmail/kmheaders.cpp: don't mark the
	  thread todo if the thread is collapsed and you want to mark the
	  top message todo

2006-03-02 14:29 +0000 [r515056]  toivo

	* branches/KDE/3.5/kdepim/libkcal/resourcecached.cpp: Make korgac
	  reloading the alarms for cached resources actually usefull

2006-03-02 20:26 +0000 [r515135]  tilladam

	* branches/KDE/3.5/kdepim/kmail/kmcommands.cpp: Fix I had lying
	  around.

2006-03-03 06:27 +0000 [r515218]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttView.cpp: Avoid compiler
	  warning.

2006-03-03 06:33 +0000 [r515219]  deller

	* branches/KDE/3.5/kdepim/kontact/plugins/kmail/Makefile.am: fix
	  build with builddir != srcdir

2006-03-03 11:00 +0000 [r515291]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/loader.cpp: fix
	  weird static QString initialization

2006-03-03 11:04 +0000 [r515292]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/akregator_part.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/mk4storage/storagefactorymk4impl.h,
	  branches/KDE/3.5/kdepim/akregator/src/storagefactorydummyimpl.h,
	  branches/KDE/3.5/kdepim/akregator/src/storagefactory.h: Remove
	  hardcoded "if backend == metakit then lock" and add a
	  allowsMultipleWriteAccess() property to the storage factories
	  instead.

2006-03-03 12:59 +0000 [r515314]  toivo

	* branches/KDE/3.5/kdepim/libkcal/resourcecached.cpp: Revert

2006-03-03 15:00 +0000 [r515376]  winterz

	* branches/KDE/3.5/kdepim/kmail/kmcomposewin.h,
	  branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp: Various bug fixes
	  for Some Quote-Related Bugs, approved by Till. 1. Bug 75520:
	  remove quote does not work with empty lines (20 votes),
	  http://bugs.kde.org/show_bug.cgi?id=75520 2. Bug (no number I can
	  find) Edit->Add Quote Characters and Edit->Remove Quote
	  Characters menu selections were disabled until you selected the
	  Edit menu twice. That's right, you had to select the Edit menu,
	  then select it again in order to activate those menu selections.
	  Since focus is put immediately into the editor, I don't see the
	  need for disabling any of the Edit menu items. 3. Bug 71232:
	  "Clean spaces" command does not properly reformat quoted text (50
	  votes), http://bugs.kde.org/show_bug.cgi?id=71232 In addition,
	  this patch: leaves the signature alone (no spaces cleaned in the
	  signature) implements space cleaning in a marked region 4. Bug
	  52547: Undoing Edit | Clean Spaces wipes quoted text (43 votes),
	  http://bugs.kde.org/show_bug.cgi?id=52547 Ok, the patch doesn't
	  provide a fix for bug 52547. but it does provide an explanation
	  of the problem and why we can't fix it in KDE 3 because a problem
	  with Qt3's QTextEdit widget. Maybe we can fix it in Qt4. I'll
	  look into this bug again when I forward port the patch to trunk.
	  5. Bug 60431: Quote indicator doesn't work correctly (23 votes),
	  http://bugs.kde.org/show_bug.cgi?id=60431 I *think* this one is
	  fixed. BUGS: 75520,71232,52547,60431

2006-03-03 15:42 +0000 [r515388]  toivo

	* branches/KDE/3.5/kdepim/kresources/newexchange/exchangeconvertercalendar.cpp:
	  Fix saving of description, attendees, cc, from fields on exchange
	  server

2006-03-05 15:36 +0000 [r515971]  winterz

	* branches/KDE/3.5/kdepim/libkdepim/ksubscription.cpp: forward port
	  515969: Make sure that the unsub and sub listviews are not
	  updated during loading by resetting the mLoading flag before,
	  well, loading. ;) Note: that this file was relocated from
	  libkdenetwork to libkdepim.

2006-03-05 21:29 +0000 [r516072]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/loader.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/librss/loader.h: use a
	  static QString* instead of static QString

2006-03-05 21:44 +0000 [r516077]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/loader.cpp: eh,
	  actually set userAgent in the setter

2006-03-05 22:00 +0000 [r516080]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/tools_p.cpp: better
	  support for atom:content, copied some code over from
	  libsyndication

2006-03-06 09:32 +0000 [r516198]  onurf

	* branches/KDE/3.5/kdepim/kmail/recipientspicker.cpp: Fix bug
	  #122218 where a shift-select in a filtered recipients picker
	  results in non-visible contacts to be added as recipients as
	  well. BUG: 122218

2006-03-07 14:09 +0000 [r516531]  winterz

	* branches/KDE/3.5/kdepim/kresources/groupwise/soap/stdsoap2.cpp,
	  branches/KDE/3.5/kdepim/kresources/groupwise/soap/stdsoap2.h: fix
	  so compile works on Linux/alpha and Tru64/alpha platforms. BUGS:
	  123214

2006-03-07 14:58 +0000 [r516548]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.h,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttView.cpp,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp: Fix
	  printing of task list. QListView::drawContentsOffset() only draws
	  visible items, we need to draw all items. Also made drawing of
	  list header conditional because it's only room for it if timeline
	  is drawn. It's a hack, but I think we can live with until qt4.

2006-03-08 22:56 +0000 [r516864]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/loader.cpp: If the
	  "Use browser cache" option is enabled (default), use "refresh"
	  mode, not the mode set in konq (as "offline mode" etc. makes no
	  sense in akregator). "Refresh" mode means: "Use entry from cache
	  after verifying with remote server"
	  (kdelibs/kio/DESIGN.metadata). This is what we want. Devels and
	  users, please test if that makes unchecking "Use browser cache"
	  unnecessary for you (if you had to uncheck it until now) CCMAIL:
	  akregator-users@lists.sf.net CCMAIL: akregator-devel@lists.sf.net

2006-03-08 23:53 +0000 [r516877]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/document.cpp,
	  branches/KDE/3.5/kdepim/akregator/ChangeLog,
	  branches/KDE/3.5/kdepim/akregator/src/librss/article.cpp: read
	  summary, link for Atom feeds BUG: 123140

2006-03-09 07:47 +0000 [r516912]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/document.cpp: eh,
	  it's "subtitle"/"tagline", not "summary" for feeds (as opposed to
	  articles) CCBUG: 123140

2006-03-09 16:36 +0000 [r517018]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/akregator_part.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/mk4storage/feedstoragemk4impl.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/mk4storage/storagemk4impl.h,
	  branches/KDE/3.5/kdepim/akregator/src/mk4storage/storagefactorymk4impl.h,
	  branches/KDE/3.5/kdepim/akregator/src/mk4storage/storagemk4impl.cpp:
	  handle tags in metakit backend only if the user has the
	  experimental and unsupported tagging GUI enabled (disabled by
	  default). This reduces the number of opened files per feed from 2
	  to 1, so metakit backend can handle about 1000 feeds now. And
	  1000 feeds should be enough for everyone (TM). BUG: 121818

2006-03-11 19:52 +0000 [r517670]  onurf

	* branches/KDE/3.5/kdepim/kmail/kmfilter.cpp: Fixes the following
	  bug: When an account is removed, the filters related to it get
	  stuck in an infinite loop when KMail is restarted, preventing
	  KMail from starting. BUG: 123342

2006-03-12 01:41 +0000 [r517744]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/articlefilter.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/articlelistview.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/articlefilter.h:
	  optimization: do not call matcher.matches() method for articles
	  if matcher matches all articles anyway

2006-03-12 15:19 +0000 [r517914]  winterz

	* branches/KDE/3.5/kdepim/kode/kxml_compiler/creator.cpp: backport
	  514938 by winterz: Fixes.

2006-03-14 15:44 +0000 [r518585]  onurf

	* branches/KDE/3.5/kdepim/libkcal/libical/src/libicalss/icalclassify.c:
	  Fix crashes when the ical file doesn't have proper organizer or
	  uid fields. Crashes are gone but still, the unknown parameters
	  result in incorrect parsing of the property. Ignoring the
	  parameter looks like a better alternative. See bugs 123620 and
	  117684. BUG: 117684

2006-03-14 18:28 +0000 [r518628]  winterz

	* branches/KDE/3.5/kdepim/kioslaves/imap4/imaplist.cc,
	  branches/KDE/3.5/kdepim/kioslaves/imap4/imapparser.cc,
	  branches/KDE/3.5/kdepim/kioslaves/imap4/imaplist.h: Fix parsing
	  problem with IMAP literals in response to LSUB Patch by Thiago
	  and Till. BUGS: 123562

2006-03-14 20:17 +0000 [r518656]  onurf

	* branches/KDE/3.5/kdepim/kmail/kmreadermainwin.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmcommands.cpp: Fixes the following
	  crash, by adding check for null-pointers: A message part that is
	  not an email by itself is "view"ed. When reply or forward actions
	  are triggered, KMail crashes. It also hides the main toolbar and
	  menu for non-message viewers. BUG: 123316 GUI: Remove menu and
	  toolbar for e-mail parts that are not e-mails themselves.

2006-03-15 04:04 +0000 [r518745]  winterz

	* branches/KDE/3.5/kdepim/korganizer/koagendaitem.cpp: A hack to
	  fix a crash that occurs when KOrganizer is used within Kontact.
	  To reproduce the crash follow these steps: 1. start Kontact with
	  the Calendar as the initial module 2. immediately select the
	  summary (which must include appt and to-do)

2006-03-15 04:07 +0000 [r518747]  winterz

	* branches/KDE/3.5/kdepim/kontact/plugins/newsticker/summarywidget.cpp,
	  branches/KDE/3.5/kdepim/kontact/plugins/newsticker/summarywidget.h:
	  Don't use RichText to display the newsticker summary because too
	  much horizontal space is used between articles. Included in this
	  patch is a URL eventFilter that displays statusbar hint on
	  mouseover. This is done to make the newsticker summary behavior
	  consistent with the other component summaries. BUGS: 120229

2006-03-15 14:19 +0000 [r518857]  winterz

	* branches/KDE/3.5/kdepim/kmail/kmfolderimap.cpp: Thiago's patch to
	  fix the problem of duplicate "inbox" folders for online imap. Not
	  closing #123563 because this patch does not fix the same problem
	  for disconnected imap.

2006-03-15 17:27 +0000 [r518914]  winterz

	* branches/KDE/3.5/kdepim/kontact/plugins/newsticker/summarywidget.cpp:
	  Nicer. plus, this way, we don't have to worry about stripping out
	  funky characters that may be in the URL title.

2006-03-16 12:47 +0000 [r519171]  mueller

	* branches/KDE/3.5/kdepim/kmail/kmmessage.cpp: fix comparison
	  against string literal

2006-03-17 01:16 +0000 [r519392]  mueller

	* branches/KDE/3.5/kdepim/kresources/kolab/kcal/resourcekolab.cpp:
	  fix string comparisons

2006-03-17 08:24 +0000 [r519445]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/akregator/src/pageviewer.cpp: bump
	  version to 1.2.2

2006-03-17 08:28 +0000 [r519448]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/pageviewer.cpp: revert

2006-03-17 09:10 +0000 [r519461]  onurf

	* branches/KDE/3.5/kdepim/akregator/src/pageviewer.cpp: Changes the
	  guitems/shortcuts of back/forward/stop buttons for the page
	  viewer to their KDE "standard" counterparts

2006-03-17 09:46 +0000 [r519473]  vkrause

	* branches/KDE/3.5/kdepim/knode/resource.h: Increment version
	  number.

2006-03-17 21:34 +0000 [r519788]  coolo

	* branches/KDE/3.5/kdepim/kdepim.lsm: tagging 3.5.2

