2007-04-02 14:00 +0000 [r649304]  aacid

	* branches/arts/1.5/arts/mcopidl/mcopidl.cc: Fix arts compilation
	  on GCC 4.3 snapshots
	  http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=417107

2007-05-14 07:15 +0000 [r664515]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdelibs/kdecore/ksycoca.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version numbers for
	  3.5.7

