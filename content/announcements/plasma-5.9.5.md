---
title: KDE Plasma 5.9.5, bugfix Release for April
release: "plasma-5.9.5"
version: "5.9.5"
description: KDE Ships Plasma 5.9.5.
date: 2017-04-25
layout: plasma
changelog: plasma-5.9.4-5.9.5-changelog
---

{{%youtube id="lm0sqqVcotA"%}}

{{<figure src="/announcements/plasma-5.9/plasma-5.9.png" alt="KDE Plasma 5.9 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.9">}}

Tuesday, 25 April 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "bugfix" "5.9.5." %}}

{{% i18n_var `<a href="https://www.kde.org/announcements/plasma-%[1]s.0.php">Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience.` "5.9" "January" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "a month's" %}}

- Plastik window decoration now supports global menu. <a href="https://commits.kde.org/kwin/3e0ddba683ca68cb50b403cb3893fa2fc9d2d737">Commit.</a> See bug <a href="https://bugs.kde.org/375862">#375862</a>. Phabricator Code review <a href="https://phabricator.kde.org/D5131">D5131</a>
- Media Controller can now properly handle and seek long tracks (&gt; 30 minutes). <a href="https://commits.kde.org/plasma-workspace/550860f6366cc99d3f0ff19f74fd3fc3d1bfc0ad">Commit.</a> Fixes bug <a href="https://bugs.kde.org/377623">#377623</a>
- Sort the themes in decoration KCM. <a href="https://commits.kde.org/kwin/f5a43877a9ea6ddad9eaa8d7498c8ea518c29c81">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D5407">D5407</a>
