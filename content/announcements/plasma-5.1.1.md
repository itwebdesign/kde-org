---
title: KDE Ships November Bugfix Release of Plasma 5
date: "2014-11-11"
description: KDE Ships Bugfix Release of Plasma 5.
layout: plasma
---

{{<figure src="/announcements/plasma-5.1/plasma-main.png" class="text-center float-right ml-3" caption="Plasma 5" width="600px" >}}

November 11, 2014.

Today KDE releases a bugfix update to Plasma 5, versioned 5.1.1. <a href='http://kde.org/announcements/plasma-5.1/index.php'>Plasma 5.1</a> was released in October with many feature refinements and streamlining the existing codebase of KDE's popular desktop for developers to work on for the years to come.

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

{{% stop-translate %}}

- Limiting indexing word size in Baloo.
- Don't index dots in Baloo, it's a regular expression character
- Breeze: Do not takeout margins from toolbutton before rendering text
- Breeze: cleanup tab buttons positioning
- Breeze: Fix positioning of cornerwidgets
- Notes widget: Make text color white on black note
- Clock widget: Fix fuzzy clock saying half past seven when it's half past six
- khotkeys: fix loading configuration
- kinfocenter: Set the correct version
- kcm-effects: Use Loader for the Video Item
- Oxygen: margins and RTL fixes
- Plasma Desktop: Validate timezone name before setting
- Plasma Desktop: Backport settings made in the component chooser to kdelibs 4 applications
- Plasma Desktop: make kdelibs 4 apps react to icon theme change
- Plasma Desktop: Cleanup applet configuration scrollbar handling and fix glitching alternatives dialog
- Plasma Desktop: fix emptying the trash from the context menu
- Plasma Desktop: numberous bugfixes
- Plasma Workspace: Remove shutdown option from screen locker
