---
title: KDE 4.1 – Pressemitteilung
date: "2008-07-29"
---

<h3 align="center">
	Die Gemeinschaft der KDE-Entwickler gibt die Veröffentlichung von KDE 4.1.0 bekannt.
</h3>

<p align="justify">
  <strong>
    Das KDE-Projekt veröffentlicht eine verbesserte Arbeitsoberfläche, sowie Anwendungen und widmet diese Uwe Thiem.
  </strong>
</p>

<p align="justify">
Die <a href="http://www.kde.org/">Gemeinschaft der KDE-Entwickler</a> veröffentlichte heute KDE 4.1.0. Diese Version ist die zweite der KDE-4-Entwicklungsreihe, welche neue Funktionen beinhaltet und bringt deshalb neue Anwendungen und neu entwickelte Funktionen – auf der Basis von KDE 4 – mit sich. KDE 4.1 ist die erste KDE-4-Version, welche KDE-PIM – das Anwendungspaket zum persönlichen Informationsmanagement – mit seinem E-Mail-Programm KMail, dem Kalender KOrganizer, Akregator dem RSS-Feedreader, KNode, dem Programm zum Lesen von Newsgroups und zahlreichen weiteren Programmen, welche es in die die Kontact-Oberfläche integriert, beziehungsweise sie mittels dieser zusammenfasst. Abgesehen davon hat Plasma, die neue Arbeitsoberfläche die mit KDE 4.0 eingeführt wurde, den Punkt erreicht, wo sie die alte Obefläche von KDE 3 für die meisten, „normalen" Benutzer ersetzten kann. Wie auch in all unseren vorangegangen, veröffentlichten Versionen, haben wir auch diesmal viel Zeit in die Arbeit an der Entwicklungsumgebung und die zugrundeliegenden Programmbibliotheken – auf welchen KDE aufbaut – investiert.<br />
Dirk Müller, einer der Koordinatoren von KDE in punkto Veröffentlichung von neuen Versionen, spricht von folgenden Zahlen: <em>„In der Zeit von KDE 4.0 bis zu KDE 4.1 wurden 20803 Änderungen am Quelltext, sowie 20803 Änderungen an Übersetzungen auf den Server übertragen. Beinahe 35000 Änderungen wurden in anderen Entwicklungszweigen vorgenommen, einige von ihnen wurden auch in KDE 4.1 übernommen, somit wurden diese nicht einmal mitgezählt."</em>
Müller teilt uns außerdem mit, dass die KDE-Systemadministratoren 166 neue Benutzerkonten für Entwickler auf dem KDE-SVN-Server erstellt haben.

<div class="text-center">
	<a href="/announcements/announce-4.1/desktop.png">
	<img src="/announcements/announce-4.1/desktop_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Die Arbeitsobefläche von KDE 4.1.</em>
</div>
<br/>

</p>
<p>
<strong>Die wesentlichen Verbesserungen in KDE 4.1 sind:</strong>
<ul>
    <li>Das KDE-PIM-Paket ist zurück.</li>
    <li>Plasma wird „erwachsen".</li>
    <li>Viele neue und verbesserte Anwendungen und dergleichen …</li>
</ul>

</p>

<h3>
  <a id="changes">Im Gedenken an: Uwe Thiem</a>
</h3>
<p align="justify">
Die Gemeinschaft der Entwickler von KDE widmet diese Version Uwe Thiem, einem langzeitigen Mitarbeiter im KDE-Projekt, welcher kürzlich an einem plötzlichen Nierenversagen verstarb. Der Tot von Uwe kam vollkommen unerwartet und war wie ein Schock für seine Entwickler-Kollegen. Uwe hat, ziemlich wortwörtlich, bis zu seinen letzen Tagen seines Lebens zu KDE beigetragen, nicht nur in Form von Programmcode. Uwe spielte auch eine wichtige Rolle bei der Informationskampagne für freie Software für Benutzer in Afrika. Mit Uwes plötzlichem Tod hat das KDE-Projekt einen unersetzbaren Teil seiner Gemeinde und einen Freund verloren. Unsere Gedanken sind bei seiner Familie, sowie den Hinterbliebenen.
</p>

<h3>
  <a id="changes">Vergangenheit, Gegenwart und Zukunft</a>
</h3>
<p align="justify">
Obwohl KDE 4.1 die erste Version sein soll, welche für Benutzer die schnell auf die neueste Version umsteigen geeignet ist, sind eine Funktionen welche Sie aus KDE 3.5 kennen noch nicht implementiert. Das KDE-Team arbeitet daran und versucht sie in einer der nächsten Versionen verfügbar zu machen. Obwohl es keine Garantie gibt, dass jede einzelne Funktion von KDE 3.5 implementiert werden wird, bietet KDE 4.1 bereits jetzt einen leistungsfähige und funktionsreiche Arbeitsumgebung.
<br />
Bitte beachten Sie, dass einige Optionen in der Benutzeroberfläche verschoben wurden, so dass sie im Kontext zu den Daten, die sie verarbeiten, stehen. Stellen Sie also sicher, dass Sie sich genau umgesehen haben, bevor Sie etwas im Bereich „Bearbeiten" als fehlend berichten.
<p />
KDE 4.1 ist ein großer Schritt nach vorne in der KDE-4-Entwicklungsreihe und setzt hoffentlich den Grundstein für weitere Entwicklungen. Das Erscheinen von KDE 4.2 ist für Januar 2009 geplant.
</p>

<div class="text-center">
	<a href="/announcements/announce-4.1/kdepim-screenie.png">
	<img src="/announcements/announce-4.1/kdepim-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KDE-PIM ist zurück.</em>
</div>
<br/>

<h3>
  <a id="changes">Verbesserungen</a>
</h3>
<p align="justify">
Während das zugrundeliegende System in KDE 4.1 weiter stabilisiert wurde, legte man auch mehr und mehr Wert auf die für die Benutzer sichtbaren Teile. Eine Liste von Verbesserungen in KDE 4.1 findet sich unten. Detaillierte Informationen diesbezüglich finden Sie in den <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Entwicklungszielen von KDE 4.1</a> oder im umfangreicheren <a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">Plan für Funktionen in KDE 4.1</a>.
</p>

<h4>
  Für Benutzer
</h4>
<p align="justify">

<ul>
    <li>
        <strong>KDE-PIM</strong> kehrt in der Version 4.1, mit all den Anwendungen für Ihr persönliches Informationsmanagement und Ihre Kommunikation, zurück. KMail als E-Mail-Programm, KOrganizer als Kalender, Akregator als RSS-Feedreader und weitere Anwendungen sind jetzt im neuen Aussehen von KDE 4 verfügbar.
    </li>
    <li>
        <strong>Dragon-Player</strong>, ein einfach zu bedienender Video-Spieler feiert sein Debüt.
    </li>
    <li>
        <strong>Okteta</strong> ist der neue und gut integrierte Hex-Editor.
    </li>
    <li>
        <strong>Step</strong>, der Physik-Emulator macht das Erlernen der Physik einfach und unterhaltsam.
    </li>
    <li>
        <strong>KSystemLog</strong>, hilft Ihnen im Überblick zu behalten was auf Ihrem System vor sich geht.
    </li>
    <li>
        <strong>Neue Spiele</strong> so wie KDiamond (ein „Bejeweled“-Klon), Kollision, KBreakOut und Kubrick machen es Ihnen einfach, eine Pause von Ihrer Arbeit einzulegen.
    </li>
    <li>
        <strong>Lokalize</strong>, hilft Übersetzern KDE 4 in Ihrer Sprache verfügbar zu machen (falls sie nicht bereits unter den über 50 Sprachen ist, die KDE 4 bereits unterstützt).
    </li>
    <li>
        <strong>KSCD</strong>, Ihr CD-Spieler am PC, wurde wiederbelebt.
    </li>
</ul>

Antworten auf häufig gestellte Fragen wurden in den
<a href="http://www.kde.org/users/faq">FAQ für KDE-4-Enbenutzer</a> gesammelt,
welche auch lesenswert sind, wenn sie mehr über KDE 4 erfahren möchten.

</p>

<p align="justify">

<div class="text-center">
	<a href="/announcements/announce-4.1/dolphin-screenie.png">
	<img src="/announcements/announce-4.1/dolphin-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Dolphins neuer Auswahlmechanismus.</em>
</div>
<br/>

<ul>
    <li>
        <strong>Dolphin</strong>, KDEs Dateiverwalter hat eine neue Baumansicht in der Hauptansicht, ebenso
		neu ist die Unterstützung von Tabs. Eine neue und innovative Einzel-Klick-Auswahl sollen dem Nutzer
		das Leben erleichtern und die Kontext-Aktionen "Kopiere nach" und "Verschiebe nach" sind jetzt
		einfacher zu erreichen. Natürlich steht Konqueror als eine Alternative zu Dolphin bereit und
		profitiert auch von den erwähnten Funktionen.
		[<a href="#screenshots-dolphin">Screenshots von Dolphin</a>]
    </li>
    <li>
        <strong>Konqueror</strong>, KDEs Internetbrowser kann nun bereits geschlossene Fenster und
		Tabs wieder öffnen. Auch beherrscht er nun weiches scrollen durch Internetseiten.
    </li>
    <li>
        <strong>Gwenview</strong>, der Bildbetrachter von KDE bekam eine neue Vollbildansicht,
		eine Vorschaubildleiste für den einfachen Zugriff auf andere Bilder, eine intelligentes Undo-System
		und Unterstützung für das Bewerten von Bildern.
		[<a href="#screenshots-gwenview">Screenshots von Gwenview</a>]
    </li>
    <li>
        <strong>KRDC</strong>, KDEs Client für entfernte Desktops entdeckt entfernte Rechner im
		lokalen Netzwerk automatisch durch Nutzung des ZeroConf-Protokolls.
    </li>
    <li>
        <strong>Marble</strong>, der Globus für KDE integriert nun
		<a href="http://www.openstreetmap.org/">OpenStreetMap</a> und so können Sie Ihren Weg überall mit
		Free Maps nutzen.
		[<a href="#screenshots-marble">Screenshots von Marble</a>]
    </li>
    <li>
        <strong>KSysGuard</strong> kann jetzt auch die Ausgaben von Prozessen überwachen und Anwendungen starten.
		Dadurch müssen Sie nicht mehr Ihre Programme in einem Terminal starten um Herauszufinden was auf dem
		Rechner vor sich geht.
    </li>
    <li>
        <strong>KWin</strong>, der Fensterverwalter von KDE mit Compositing-Funktionen wurde weiter
		stabilisiert und erweitert. Neue Effekte wie Cover-Wechsel und die bekannten "wabbligen" Fenster wurden
		hinzugefügt.
		[<a href="#screenshots-kwin">Screenshots von KWin</a>]
    </li>
    <li>
        <strong>Plasma</strong>s Einrichtung von Panels wurde erweitert. Der neue Panel-Kontroller
		macht das Anpassen Ihrer Panel zum Kinderspiel und bietet direkte visuelle Rückmeldung.
		Sie können auch Panels hinzufügen und diese in unterschiedlichen Ecken der Arbeitsfläche
		verschieben. Das neue Miniprogramm für die Ordneransicht erlaubt es Ihnen Dateien direkt auf der
		Arbeitsfläche zu speichern. Sie können keine, eine oder mehrere Ordneransichten auf der
		Arbeitsfläche speichern um einfachen Zugriff auf Dateien zu haben, mit denen Sie gerade arbeiten.
        [<a href="#screenshots-plasma">Screenshots von Plasma</a>]
    </li>
</ul>
</p>

<h4>
  Für Entwickler
</h4>
<p align="justify">

<ul>
    <li>
        Das <strong>Akonadi</strong>-PIM-Datenhaltungs-Rahmenwerk bietet einen effizienten Weg zur
		Speicherung und Empfang von E-Mails und Kontaktdaten über Anwendungsgrenzen hinweg.
		<a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> unterstützt die Suche in Daten
		und informiert Anwendungen bei Änderungen.
    </li>
    <li>
        KDE-Anwendungen können u.a. mit Python und Ruby geschrieben werden.
		Diese <strong>Sprachbindungen</strong> sind
        <a href="http://techbase.kde.org/Development/Languages">stabil</a>, ausgereift und passend für
		Anwendungsentwickler.
    </li>
    <li>
        <strong>Libksane</strong> bietet einfachen Zugriff aus Scanprogramme wie das neue Programm Skanlite.
    </li>
    <li>
        Ein gemeinsames <strong>Emoticon</strong>-System, welches bereits von KMail und Kopete genutzt wird.
    </li>
    <li>
        Das neue Multimediasystem <strong>Phonon</strong> für GStreamer, QuickTime und DirectShow9
		verbessert KDEs Multimedia-Unterstützung auf Windows und Mac OS.
    </li>

</ul>
</p>

<h3>
  Neue Plattformen
</h3>
<p align="justify">
<ul>
    <li>
        <a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolaris</strong></a>-Unterstützung
		von KDE wird gerade in Form gebracht. Weitgehend funktioniert KDE unter OSOL, wobei es noch einige
		Stolperfallen gibt.
    </li>
    <li>
        <strong>Windows</strong>-Entwickler können sich Vorschauen von KDE-Anwendungen für ihre Plattform
		<a href="http://windows.kde.org">herunterladen</a>.
		Die Bibliotheken sind bereits relativ stabil, doch sind noch nicht alle Funktionen der kdelibs für
		Windows verfügbar. Einige Anwendungen laufen sehr gut unter Windows, andere noch nicht.
    </li>
    <li>
        <strong>Mac OSX</strong> ist eine weitere Plattform, die KDE betritt.
        <a href="http://mac.kde.org">KDE on Mac</a> ist noch nicht bereit für den produktiven Einsatz.
		Multimedia-Unterstützung durch Phonon ist bereits verfügbar, die Integration von Hardware und Suchfunktion
		ist noch nicht soweit.
    </li>
</ul>
</p>

<a id="screenshots-dolphin"></a>

<h3>
  Bildschirmfotos
</h3>
<p align="justify">

<a id="screenshots-dolphin"></a>

<h4>Dolphin</h4>

<div class="text-center">
	<a href="/announcements/announce-4.1/dolphin-treeview.png">
	<img src="/announcements/announce-4.1/dolphin-treeview_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Dolphins neue Baumansicht gibt Ihnen schnelleren Zugriff über Verzeichnisgrenzen hinweg. In den Standardeinstellungen ist diese Funktion allerdings nicht aktiviert.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/dolphin-tagging.png">
	<img src="/announcements/announce-4.1/dolphin-tagging_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Nepomuk bietet Tagging und Bewertungen in KDE -- und damit auch in Dolphin.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/dolphin-icons.png">
	<img src="/announcements/announce-4.1/dolphin-icons_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Symbolvorschau und Informationsleisten bieten visuelle Rückmeldungen und Übersicht.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/dolphin-filterbar.png">
	<img src="/announcements/announce-4.1/dolphin-filterbar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Finden Sie Ihre Dateien einfacher mit der Filterleiste</em>
</div>
<br/>

<a id="screenshots-gwenview"></a>

<h4>Gwenview</h4>

<div class="text-center">
	<a href="/announcements/announce-4.1/gwenview-browse.png">
	<img src="/announcements/announce-4.1/gwenview-browse_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Sie können mit Gwenview Verzeichnisse nach Bildern durchsuchen.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/gwenview-open.png">
	<img src="/announcements/announce-4.1/gwenview-open_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Das Öffnen von Dateien von Ihrer Festplatte oder dem Netzwerk ist sehr einfach
      dank der KDE-Infrastruktur.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/gwenview-thumbnailbar.png">
	<img src="/announcements/announce-4.1/gwenview-thumbnailbar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Mit der neuen Vorschauleiste kann einfach zwischen Bildern gewechselt werden.
      Sie ist auch im Vollbildmodus erreichbar.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/gwenview-sidebar.png">
	<img src="/announcements/announce-4.1/gwenview-sidebar_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Die seitliche Leiste von Gwenview bietet Zugriff auf weitere Informationen und
      Aktionen zur Bildbearbeitung.</em>
</div>
<br/>

<a id="screenshots-marble"></a>

<h4>Marble</h4>

<div class="text-center">
	<a href="/announcements/announce-4.1/marble-globe.png">
	<img src="/announcements/announce-4.1/marble-globe_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Der Marble-Desktop-Globus.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/marble-osm.png">
	<img src="/announcements/announce-4.1/marble-osm_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Durch die neue Integration von OpenStreetMap in Marble werden auch Informationen zur Personenbeförderung angezeigt.</em>
</div>
<br/>

<a id="screenshots-kwin"></a>

<h4>KWin</h4>

<div class="text-center">
	<a href="/announcements/announce-4.1/kwin-desktopgrid.png">
	<img src="/announcements/announce-4.1/kwin-desktopgrid_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KWin's desktopgrid visualizes the concept of virtual desktops and
      makes it easier to remember where you left that window you're looking for.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/kwin-coverswitch.png">
	<img src="/announcements/announce-4.1/kwin-coverswitch_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The Coverswitcher makes switching applications with Alt+Tab a real eye-catcher. You can choose it in KWin's desktop effects settings.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/kwin-wobbly.png">
	<img src="/announcements/announce-4.1/kwin-wobbly_thumb.png" class="img-fluid">
	</a> <br/>
	<em>KWin now also has the mandatory wobbly windows (disabled by default).</em>
</div>
<br/>

<a id="screenshots-plasma"></a>

<h4>Plasma</h4>

<div class="text-center">
	<a href="/announcements/announce-4.1/plasma-folderview.png">
	<img src="/announcements/announce-4.1/plasma-folderview_thumb.png" class="img-fluid">
	</a> <br/> 
	<em>The new folderview applet lets you display the content of arbitrary
      directories on your desktop. Drop a directory onto your unlocked
      desktop to create a new folderview. A folderview can not only display local
      directories, but can also cope with locations on the network.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/panel-controller.png">
	<img src="/announcements/announce-4.1/panel-controller_thumb.png" class="img-fluid">
	</a> <br/>
	<em>The new panel controller lets you easily resize and reposition panels.
      You can also change the position of applets on the panel by dragging them
      to their new position.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/krunner-screenie.png">
	<img src="/announcements/announce-4.1/krunner-screenie_thumb.png" class="img-fluid">
	</a> <br/>
	<em>With KRunner, you can start applications, directly email your friends
      and accomplish various other small tasks.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/plasma-kickoff.png">
	<img src="/announcements/announce-4.1/plasma-kickoff_thumb.png" class="img-fluid">
	</a> <br/>
	<em>Plasma's Kickoff application launcher has had a facelift.</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/announce-4.1/switch-menu.png">
	<img src="/announcements/announce-4.1/switch-menu_thumb.png" class="img-fluid">
	</a> <br/>
	<em>You can choose between the Kickoff application launcher and the classic
      menu style.</em>
</div>
<br/>
</p>

<h4>
  Bekannte Probleme
</h4>
<p align="justify">
<ul>
    <li>Benutzer von <strong>NVidia</strong>-Karten mit dem Binär-Treiber, der von NVidia angeboten wird, leiden eventuell unter Geschindigkeitseinbußen beim Wechseln und Vergrößern oder Verkleinern von Fenstern. Wir haben die NVidia-Entwickler über dieses Problem unterrichtet, bislang wurde aber noch keine korrigierte Version des Treibers veröffentlicht. Nähere Informationen zum Verbessern der Geschwindigkeit bei der grafischen Darstellung finden Sie in der <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">„TechBase"</a>. Nichtsdestotrotz sind wir trotzdem davon abhängig, dass NVidia das das Problem ihres Treibers behebt.</li>
</ul>

</p>

<h4>
   Holen Sie sich’s, führen Sie’s aus, testen Sie’s
</h4>
<p align="justify">
  Freiwillige und Linux- beziehungsweise Unix-Betriebsystemhersteller haben freundlicherweise Binärpakete von KDE 4.0.80 (Beta 1) für die meisten Linux-Distributionen, Mac OS X und Windows, zur Verfügung gestellt. Werfen Sie einen Blick auf das Softwareverwaltungssystem Ihres Betriebssystemanbieters.
</p>
<h4>
 KDE 4.1.0 kompilieren
</h4>
<p align="justify">
  <a id="source_code"></a><em>Quelltext</em>.
  Der vollständige Quellstext von KDE 4.1.0 kann <a
  href="http://www.kde.org/info/4.1.0">frei heruntergeladen</a> werden.
Anleitungen zum Kompilieren und Installieren von KDE 4.1.0
  finden sich auf der <a href="/info/4.1.0">KDE-4.1.0-Informationsseite</a>
oder in der <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">"TechBase"</a>.
</p>

{{% include "/includes/about_kde.html" %}}
{{% include "content/includes/press_contacts.html" %}}
