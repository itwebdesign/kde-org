---
title: Plasma 5.13.4 Complete Changelog
version: 5.13.4
hidden: true
plasma: true
type: fulllog
---

### <a name='breeze-plymouth' href='https://commits.kde.org/breeze-plymouth'>Breeze Plymouth</a>

- Document the use of breeze.grub. <a href='https://commits.kde.org/breeze-plymouth/d2d2126e0d59b47a5f2ccc91a1b7da0db2ea291d'>Commit.</a> </li>
- Remove mention of plasma blue, it's not been used for years. <a href='https://commits.kde.org/breeze-plymouth/44aa0262f987c459245482e4944c40955784f166'>Commit.</a> </li>
- Ditch grub testing values for ubuntu. <a href='https://commits.kde.org/breeze-plymouth/9600842d9ba163e0f2e892d572d9c7b43db48397'>Commit.</a> </li>
- Set black as background color for grub on ubuntu systems. <a href='https://commits.kde.org/breeze-plymouth/5da41d73797de289816ec58bc9441e8e56b062b4'>Commit.</a> </li>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- KNS: Fix leak of resource ratings. <a href='https://commits.kde.org/discover/48b67cf568c6d427c6038fe763a3018b5b12d7c2'>Commit.</a> </li>
- When sorting by release date, show newer first. <a href='https://commits.kde.org/discover/b6a3d2bbf1a75bac6e48f5ef5e8ace8f770d535c'>Commit.</a> </li>
- Prevent crash. <a href='https://commits.kde.org/discover/ec234e949f5c4b1a41ac926c8e3128cb4a463324'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396564'>#396564</a></li>
- Readability. <a href='https://commits.kde.org/discover/1ce242f4a8211be7ae8f9e2d5cd560f64b698038'>Commit.</a> </li>
- Fix CMakeLists.txt to work with older versions of CMake. <a href='https://commits.kde.org/discover/d8ac72b45e9739fd6e1ba8a7d66f6cff197436cf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14112'>D14112</a></li>
- KNS: Don't offer the backends to add if we know they're broken. <a href='https://commits.kde.org/discover/b748b022ad2f3204df5378a0f00ee09d24285ab4'>Commit.</a> </li>
- FeaturedModel: Make sure the file is written before we read. <a href='https://commits.kde.org/discover/5d17e2ed9a7d047b8c88f3d8d1a2a6fc4132a7d6'>Commit.</a> </li>
- Fix featured json file warning. <a href='https://commits.kde.org/discover/8fb5c12d0c709eb5cd583cb4630a5a87fdda0139'>Commit.</a> </li>
- Fix warning. <a href='https://commits.kde.org/discover/37c3f8683f65b31df4c1a0338c6eef0fc2079b62'>Commit.</a> </li>
- Silence false positive warning on --mode. <a href='https://commits.kde.org/discover/e43a191d0e0175f228de7ebef50a0f00e02941ae'>Commit.</a> </li>
- Fix display if there's no source description. <a href='https://commits.kde.org/discover/b279d7172378be25f74296b309e619a17a37bd88'>Commit.</a> </li>
- Proper status when removing snaps. <a href='https://commits.kde.org/discover/fa66dbf50b06b75a8bf41a6683212f60010c8f71'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396351'>#396351</a></li>
- Properly check for pkqt. <a href='https://commits.kde.org/discover/470b3e232be2ae22da5a959f8c0f2bc3ad041a1c'>Commit.</a> </li>
- Use consistent spacing in the missing backends footer. <a href='https://commits.kde.org/discover/67bd517d3a563a92c2725dff9d1dd169f010ba22'>Commit.</a> </li>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [dict applet] Fix translation catalog name to match pluginid. <a href='https://commits.kde.org/kdeplasma-addons/076539340f5fb17c2836e3d9029980abb593ba4d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14398'>D14398</a></li>

### <a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a>

- Fix file indexer monitor Exec line. <a href='https://commits.kde.org/kinfocenter/11604cdf65d2d9d057af170fd6be2711750a9350'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396977'>#396977</a></li>
- Update kinfocenter docbook to 5.12. <a href='https://commits.kde.org/kinfocenter/e6759f109bacf36c0159d71461b226975cc6ad86'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11864'>D11864</a></li>

### <a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a>

- Fix preview widget appearing to scale twice. <a href='https://commits.kde.org/kscreen/5c45d27f00b4207d5db347350a81e8ecb6bdb108'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388218'>#388218</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14338'>D14338</a></li>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [kcmkwin/kwindecoration] Add missing QT include. <a href='https://commits.kde.org/kwin/d7a1e33683b0eb251674ccc80a364170f565984c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14173'>D14173</a></li>
- Don't remove outputs during page flip. <a href='https://commits.kde.org/kwin/a362a67989f1cea4606f9a564d9a51144a51c719'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396272'>#396272</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14210'>D14210</a></li>
- [wayland] Confine pointer to screen geometry. <a href='https://commits.kde.org/kwin/1e4703a719599d1587f5e43c0650bcac471450e7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374867'>#374867</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14036'>D14036</a></li>
- [effects/desktopgrid] Don't change activities. <a href='https://commits.kde.org/kwin/0c3047a8f1d4a70f39250c759224a7dfab72a76b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/301447'>#301447</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14046'>D14046</a></li>

### <a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a>

- Make TestInProcess skip out-of-process tests if D-Bus service uninstalled. <a href='https://commits.kde.org/libkscreen/2b5a4727ee344cde4390b7b13943ed07a567c30f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13871'>D13871</a></li>

### <a name='milou' href='https://commits.kde.org/milou'>Milou</a>

- Textplugin: Fix missing QTextStream include. <a href='https://commits.kde.org/milou/6b2036cb8a1d4725ad271760561aac9fa735a8c1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13874'>D13874</a></li>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Don't unintentionally change font rendering when rendering preview images. <a href='https://commits.kde.org/plasma-desktop/79a4bbc36cee399d71f3cfb05429939b0850db25'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14480'>D14480</a></li>
- [Folder View] Improve file name text rendering. <a href='https://commits.kde.org/plasma-desktop/5fca2b32ce5bb04fd6ce614c979c7665db84a4a5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14234'>D14234</a></li>
- Give file and folder names a bit more room in Folder View. <a href='https://commits.kde.org/plasma-desktop/02dadc40efb4fbe7425902fc236b06ca729d848b'>Commit.</a> See bug <a href='https://bugs.kde.org/379432'>#379432</a>. Phabricator Code review <a href='https://phabricator.kde.org/D11358'>D11358</a></li>
- Honor ghns KIOSK restriction in new KCMs. <a href='https://commits.kde.org/plasma-desktop/4e2a515bb34f6262e7d0c39c11ee35b6556a6146'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14041'>D14041</a></li>
- [Kicker] Use KFilePlaces::convertedUrl for ComputerModel. <a href='https://commits.kde.org/plasma-desktop/3fe808ef8e9763edd5c349f8556b7ee2c0645702'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/363337'>#363337</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14029'>D14029</a></li>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- Make GConf optional dependency. <a href='https://commits.kde.org/plasma-pa/c9fae1fb3f8e8a820fd480ce227d7fabf87bd045'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386665'>#386665</a>. Phabricator Code review <a href='https://phabricator.kde.org/D13734'>D13734</a></li>
- Fix wrong availability of profiles and ports. <a href='https://commits.kde.org/plasma-pa/b97846017df57698f9e77c8aab077d5a3f17e7ea'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13694'>D13694</a></li>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- Fix blocky text on splash screen when using non-integer scale factor. <a href='https://commits.kde.org/plasma-workspace/1467427b6569f07f9496e049351a48c2c09ff2b5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14347'>D14347</a></li>
- Disable script execution over DBus when scripting console is disabled. <a href='https://commits.kde.org/plasma-workspace/5c4a0acba4098f9a2bdb79b8a9ec13b20a59fdfb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14185'>D14185</a></li>
- [Notifications Engine] Never group notifications with URLs. <a href='https://commits.kde.org/plasma-workspace/c221f6315808a5c1287dcb095ad5043e49e9e4c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396741'>#396741</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14287'>D14287</a></li>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Check for window existence. <a href='https://commits.kde.org/systemsettings/6f032226cebeaf7459382664f8fbb549ac6d4e3c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355818'>#355818</a></li>
- [SubCategoryPage] Use binding for back button color. <a href='https://commits.kde.org/systemsettings/de80e8d30a2b74bcad461d29bb4f2afa14dfb799'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14303'>D14303</a></li>
- Fix sidebar search field with fractional scale factors. <a href='https://commits.kde.org/systemsettings/1cb8b2af21df94763f44cce757e84785ebf2d7ba'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14256'>D14256</a></li>
- Navigate with enter as well. <a href='https://commits.kde.org/systemsettings/b4efe102d38bf55c902d914287b153ddd75ba433'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394335'>#394335</a></li>
- Forget all kcms that don't exist anymore. <a href='https://commits.kde.org/systemsettings/b1f2a599f3e22d7b2abd22dddec1bbe3c57222e8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394553'>#394553</a></li>
- Hide view selection if there is only one plugin to choose. <a href='https://commits.kde.org/systemsettings/86f0f92caf0f28fdce3df9e30fc22a67738f92ea'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374865'>#374865</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14247'>D14247</a></li>
- Quit gracefully on sidebar QML errors. <a href='https://commits.kde.org/systemsettings/69c6654a385e12afc06f72348f750c88a54195f3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394902'>#394902</a></li>
- Update systemsettings docbook to 5.12. <a href='https://commits.kde.org/systemsettings/b3b57edf86f3c12cf5f6ff51a4ab9e55027a970e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D11607'>D11607</a></li>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Minor changes to macros. <a href='https://commits.kde.org/xdg-desktop-portal-kde/8a3516cdaa6351997f83ec038fda762d4c4a0462'>Commit.</a> </li>
- Support PipeWire 0.2.0. <a href='https://commits.kde.org/xdg-desktop-portal-kde/96abec69d2811c703e8de6445fa208ca90501dd1'>Commit.</a> </li>
