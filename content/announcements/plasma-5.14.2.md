---
title: KDE Plasma 5.14.2, Bugfix Release for October
release: plasma-5.14.2
version: 5.14.2
description: KDE Ships 5.14.2
date: 2018-10-23
changelog: plasma-5.14.1-5.14.2-changelog
---

{{<figure src="/announcements/plasma-5.14/plasma-5.14.png" alt="Plasma 5.14" class="text-center" width="600px" caption="KDE Plasma 5.14">}}

Tuesday, 23 October 2018.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.14.2." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.14" "October" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "week's" %}}

- Fix plasmashell freeze when trying to get free space info from mounted remote filesystem after losing connection to it. <a href="https://commits.kde.org/plasma-workspace/be3b80e78017cc6668f9227529ad429150c27faa">Commit.</a> Fixes bug <a href="https://bugs.kde.org/397537">#397537</a>. Phabricator Code review <a href="https://phabricator.kde.org/D14895">D14895</a>. See bug <a href="https://bugs.kde.org/399945">#399945</a>
- Add accessibility information to desktop icons. <a href="https://commits.kde.org/plasma-desktop/498c42fed65df76ca457955bab18a252d63ca409">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D16309">D16309</a>
- Fix bookmarks runner with Firefox 58 and newer version. <a href="https://commits.kde.org/plasma-workspace/99fa6ccc57c5038ffb16d2e999893d55dc91f5b1">Commit.</a> Fixes bug <a href="https://bugs.kde.org/398305">#398305</a>. Phabricator Code review <a href="https://phabricator.kde.org/D15305">D15305</a>
