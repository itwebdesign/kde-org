---
title: Plasma 5.16.2 Complete Changelog
version: 5.16.2
hidden: true
plasma: true
type: fulllog
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Make appstream files validate. <a href='https://commits.kde.org/discover/e9e46b6ab2a08051662a186f28e367a1e7c8c0b3'>Commit.</a>
- Odrs: delay QNAM initialization. <a href='https://commits.kde.org/discover/df5043d2df91af9626ae506fd7bf0284b0370a56'>Commit.</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [Calculator] Fix popup size and minimum size for expanded version. <a href='https://commits.kde.org/kdeplasma-addons/ba5d35dfe829facdd6e3d69278a67d0bc694f57b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21811'>D21811</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [placement] Avoid smart placement strategy with invalid client sizes. <a href='https://commits.kde.org/kwin/842b2ce51b6b9471f45f94e00a942c52648e9b60'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408754'>#408754</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21997'>D21997</a>
- Make sure we don't resize clients before they've been set up. <a href='https://commits.kde.org/kwin/22cbbca0432f173eeae57e51364a2b26638361b3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22054'>D22054</a>
- Avoid crash without XWayland. <a href='https://commits.kde.org/kwin/515f3cbb4e716a2bf13cca5d585d886d1df4445c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21965'>D21965</a>
- [wayland] Obey m_requestGeometryBlockCounter inside updateDecoration. <a href='https://commits.kde.org/kwin/bc97232dfd16c4fe88903d3b9fdb58166e26d544'>Commit.</a>
- [platforms/X11] Disable VSync for QtQuick Windows. <a href='https://commits.kde.org/kwin/0124b1ef191fcafe0d0f89287be66b36833586e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406180'>#406180</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21808'>D21808</a>

### <a name='milou' href='https://commits.kde.org/milou'>Milou</a>

- Don't give up if no results arrive after 500ms. <a href='https://commits.kde.org/milou/c918c2840bcbbc52da72f163c8494db227db3600'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389611'>#389611</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21605'>D21605</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Make appstream validate. <a href='https://commits.kde.org/plasma-desktop/595079a230c3fa38f6150275ea0f3db42c428b04'>Commit.</a>
- [Look and Feel and KSplash KCM] Set sourceSize for thumbnail. <a href='https://commits.kde.org/plasma-desktop/7c49e1498aab283c5996ec3645f991e73baf4cc6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18322'>D18322</a>
- More reliable Task Manager media controls for application bundles. <a href='https://commits.kde.org/plasma-desktop/4a6a518a8b4c8aec7968f3698191cc2daafcdec4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22060'>D22060</a>
- Fix merge screwup. <a href='https://commits.kde.org/plasma-desktop/2810ace0e30905cf3d53b94a830eed95552dfca4'>Commit.</a>
- [Kickoff/Dash] Show Calculator and Unit Converter runners. <a href='https://commits.kde.org/plasma-desktop/16eddf0503ba8e4b5129f966fbf45694bde17667'>Commit.</a> See bug <a href='https://bugs.kde.org/382760'>#382760</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21983'>D21983</a>
- Increment iterator before any potential continue. <a href='https://commits.kde.org/plasma-desktop/51d16037ee161a90884461fe21f2208fb5f92bad'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22004'>D22004</a>
- Reject duplicate events with identical coordinates. <a href='https://commits.kde.org/plasma-desktop/0628dab8f42ba0d4c4acdb6004d8e5ad98bfdb52'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21988'>D21988</a>
- Fix ocasional crash on the touchpad kded. <a href='https://commits.kde.org/plasma-desktop/920e39c2e5ccdc6c21e85516ab87732127549ac3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21980'>D21980</a>
- [Colors KCM] Assign properties in a binding. <a href='https://commits.kde.org/plasma-desktop/0b72df235a0a96e77025b20141079952a7c48850'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21909'>D21909</a>
- Touchpad: fix some easy warnings. <a href='https://commits.kde.org/plasma-desktop/eb993c6fedc5873c090f2d46520b9f6cbe780792'>Commit.</a>
- [Touchpad KCM] Do not crash in case there is no touchpad. <a href='https://commits.kde.org/plasma-desktop/b7127724309a136a1a8647341df9f5ad02b22698'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408325'>#408325</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21884'>D21884</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [Notifications] Swap configure and clear buttons in full representation header. <a href='https://commits.kde.org/plasma-workspace/11387ca37f9165429fd37e01e80db75073220971'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409140'>#409140</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22075'>D22075</a>
- Do not use long deprecated QString::toAscii. <a href='https://commits.kde.org/plasma-workspace/4cf47ad6818d72cb9cfd24a6171a13f49fdc4090'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21762'>D21762</a>
- Print a warning when invoking the scripting API's sleep(). <a href='https://commits.kde.org/plasma-workspace/3563f51309cc5d7c25983c05d5bdd54292cd962f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21895'>D21895</a>
- [Notifications] Don't keep non-configurable notifications in history. <a href='https://commits.kde.org/plasma-workspace/1f6050b1740cf800cb031e98fd89bc00ca20c55a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22048'>D22048</a>
- [Notifications] Make popup placement more resilient. <a href='https://commits.kde.org/plasma-workspace/c26fd34c7d601b5a231e464db95ef87282aa7d95'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22045'>D22045</a>
- [Notifications] Don't show remaining time when job is paused. <a href='https://commits.kde.org/plasma-workspace/48da3382b2bf163b40c7c3d296c5353af931e488'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D22037'>D22037</a>
- [Notifications] Let plasmashell be the only true owner of notification and job tracker services. <a href='https://commits.kde.org/plasma-workspace/52bec414aefadc3fa02dacc82c73ec31d4c00098'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408250'>#408250</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22017'>D22017</a>
- [Klipper] Always restore the last clipbboard item. <a href='https://commits.kde.org/plasma-workspace/3bd6ac34ed74e3b86bfb6b29818b726baf505f20'>Commit.</a>
- Handle Gimp 2.10. <a href='https://commits.kde.org/plasma-workspace/69adf0605419c30c5b7e7ac720ee20d6f10730fe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21954'>D21954</a>
- Media controls on the lock screen are now properly translated. <a href='https://commits.kde.org/plasma-workspace/588aa6be2984038f91167db9ab5bd1f62b9f47e5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21947'>D21947</a>
- [interactiveconsole] Fix initial load selection. <a href='https://commits.kde.org/plasma-workspace/15de7ceb3875f6b7be7bc55ec43941874fc43e44'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408787'>#408787</a>. Phabricator Code review <a href='https://phabricator.kde.org/D21852'>D21852</a>
- Improved notification identification for Snap applications. <a href='https://commits.kde.org/plasma-workspace/951551bc6d1ab61e4d06fe48830459f23879097c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21881'>D21881</a>
- Make the icon applet use the same shadow settings as desktop icons. <a href='https://commits.kde.org/plasma-workspace/e2764c8e4a9505007268f4c7dc3efbdc00542a32'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21546'>D21546</a>

### <a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a>

- [powerdevil] Return battery source monitoring. <a href='https://commits.kde.org/powerdevil/a1f9e365a5b4fcc384a8cff07246821d6dd9d2fb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D21918'>D21918</a>
