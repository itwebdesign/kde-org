---
title: KDE Ships Plasma 5.5.2, bugfix Release for December
description: KDE Ships Plasma 5.5.2
date: 2015-12-22
release: plasma-5.5.2
layout: plasma
changelog: plasma-5.5.1-5.5.2-changelog
---

{{<figure src="/announcements/plasma-5.5/plasma-5.5.png" alt="Plasma 5.5 " class="text-center" width="600px" caption="Plasma 5.5">}}

Tuesday, 22 December 2015.

Today KDE releases a bugfix update to Plasma 5, versioned 5.5.2. <a href='https://www.kde.org/announcements/plasma-5.5.0.php'>Plasma 5.5</a> was released a couple of weeks ago with many feature refinements and new modules to complete the desktop experience. We are experimenting with a new release schedule with bugfix releases started out frequent and becoming less frequent. The first two bugfix releases come out in the weeks following the initial release and future ones at larger increments.

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Task Manager: behave properly in popups again. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=8e4431a2c05ba12ffb8c771c1ea0c842ec18453b">Commit.</a>
- KWin: fix build with Qt 5.6. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=05c542ad602a114751f34ac9d03597f11e95470f">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/126234">#126234</a>
- Make initial default panel thickness scale with DPI. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=eed2f0206184a5066038a6dea7402f24634fb72e">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/126363">#126363</a>
