---
title: KDE Ships KDE Applications 19.04.3
major_version: "19.04"
version: "19.04.3"
release: "applications-19.04.3"
description: KDE Ships Applications 19.04.3.
date: 2019-07-11
layout: applications
changelog: fulllog_applications-19.04.3
---

July 11, 2019.

{{% i18n_var "Today KDE released the third stability update for <a href='%[1]s'>KDE Applications %[2]s</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-19.04.0" "19.04" %}}

Over sixty recorded bugfixes include improvements to Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular, Umbrello, among others.

Improvements include:

- Konqueror and Kontact no longer crash on exit with QtWebEngine 5.13
- Cutting groups with compositions no longer crashes the Kdenlive video editor
- The Python importer in Umbrello UML designer now handles parameters with default arguments
