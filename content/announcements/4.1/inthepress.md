---
title: KDE 4.1.0 in the Press
date: "2008-07-29"
hidden: true
---

<p>The release of KDE 4.1 has prompted many excellent reviews online.</p>
<h2>Users</h2>
<p>Blogs and Forums worldwide are full of positive comments and many are
  testing the different packages and livecd's gracefully provided by the
  distributions. The reviewers clearly recognize what we have archieved
  already but also see the future potential in KDE 4. We are very happy
  with this encouraging response from communities worldwide! Of course,
  we also urge everybody to report bugs and help us fix them by sending
  in patches and improvements.</p>
<h2>Developers</h2>
<p>Consider the <a href="http://techbase.kde.org/Development">KDE 4.x
  foundation</a> for your applications - by using our technology they
  can advance to a higher level. Our platform will help you not only in
  bringing a first-grade experience to your users, be it on any of the
  free operating systems as well as on Windows, Mac OS X and various
  mobile platforms, but allow you to spend more time on innovating and
  trying new ways of servicing the users. This wil bring the ultimate
  goal of creating a compelling user experience, both in terms of
intuitive usability and unique features, closer than ever.</p>
<p>Read on for an overview of the reviews. If we missed one, please drop us an email at kde-promo [at] kde [dot] org </p>
<h2>English</h2>
<ul>
  <li>KDE Rocks the Desktop: <a href="http://www.linux.com/feature/142661">linux.com</a> </li>
  <li>KDE 4.1 Review: The Rocky Road of the New KDE: <a href="http://itmanagement.earthweb.com/osrc/article.php/12068_3761781_2">itmanagement.earthweb.com</a>  </li>
  <li>KDE 4.1 Released With openSUSE Packages and Live CD: <a href="http://news.opensuse.org/2008/07/29/kde-41-released-with-opensuse-packages-and-live-cd/">news.opensuse.org</a>  </li>
  <li>KDE 4.1 release ups free desktop ante: <a href="http://www.techworld.com.au/article/254955/kde_4_1_release_ups_free_desktop_ante">techworld.com.au</a>  </li>
  <li> KDE 4.1 Released, Reviewed: <a href="http://tech.slashdot.org/tech/08/07/29/1534249.shtml">slashdot.org</a>  </li>
  <li>KDE Desktop 4.1 released:<a href="http://www.heise-online.co.uk/news/KDE-Desktop-4-1-released--/111205"> heise-online.co.uk</a>  </li>
  <li> KDE 4.1 hits the desktop: <a href="http://www.tectonic.co.za/?p=2724">tectonic.co.za</a>  </li>
  <li>KDE dedicates 4.1 release to Uwe Thiem: <a href="http://www.tectonic.co.za/?p=2722">tectonic.co.za</a>  </li>
  <li>KDE 4.1 released: <a href="http://news.zdnet.co.uk/software/0,1000000121,39454359,00.htm">news.zdnet.co.uk</a>  </li>
  <li>KDE 4.1 delivers a next-gen desktop Linux experience: <a href="http://arstechnica.com/news.ars/post/20080729-kde-4-1-delivers-a-next-gen-desktop-linux-experience.html">arstechnica.com</a>  </li>
  <li>KDE 4.1 Released: <a href="http://www.osnews.com/story/20125/KDE_4_1_Released">osnews.com</a>  </li>
  <li>With KDE 4.1, KDE4 starts to show its colors: <a href="http://www.downloadsquad.com/2008/07/30/with-kde-4-1-rc1-kde4-starts-to-show-its-colors/">downloadsquad.com</a>  </li>
  <li>K Desktop Environment 4.1 lands: <a href="http://www.theregister.co.uk/2008/07/30/kde_4_released/">theregister.co.uk</a>  </li>
  <li>Is KDE back? 4.1 launches: <a href="http://www.desktoplinux.com/news/NS3263291244.html">desktoplinux.com</a>  </li>
  <li>KDE 4.1: Better Than Any Mac Or Vista: <a href="http://www.efytimes.com/efytimes/fcreative.asp?edid=27844">efytimes.com</a>  </li>
  <li>KDE 4.1.0 released: <a href="http://stuff.techwhack.com/4257-kde-4.1.0">techwhack.com</a> </li>
  <li>KDE Community Delivers Version 4.1: <a href="http://ostatic.com/170174-blog/kde-community-delivers-version-4-1">ostatic.com</a>  </li>
  <li>KDE 4.1 - Wow!: <a href="http://sharplinux.blogspot.com/2008/07/kde-4-wow.html">sharplinux.blogspot.com </a>  </li>
  <li>KDE 4.1 delivers a next-gen desktop Linux experience: <a href="http://arstechnica.com/news.ars/post/20080729-kde-4-1-delivers-a-next-gen-desktop-linux-experience.html">arstechnica.com</a> </li>
  <li>KDE 4.1 - Are You Ready to Switch?: <a href="http://blog.linuxoss.com/2008/07/kde-41-are-you-ready-to-switch/">blog.linuxoss.com</a> </li>
  <li>Videos: Dolphin, Gwenview &amp; More in KDE 4.1: <a href="http://blog.linuxoss.com/2008/07/videos-dolphin-gwenview-kde-41-2/">blog.linuxoss.com</a></li>
  <li>KDE 4.1 is really nice:  <a href="http://reddragdiva.livejournal.com/484521.html">reddragdiva.livejournal.com</a> </li>
</ul>
<h2>Non-English</h2>
<ul>
  <li>[BRAZILIAN PORTUGUESE] Lan&ccedil;ado o KDE 4.1: uma an&aacute;lise do ambiente desktop: <a href="http://www.linuxmagazine.com.br/noticia/lancado_o_kde_41_uma_analise_do_ambiente_desktop">linuxmagazine.com.br</a>  <br />
</li>
</ul>
<ul>
  <li>[DUTCH] Ontwikkelaars geven KDE 4.1 vrij: <a href="http://tweakers.net/nieuws/54819/ontwikkelaars-geven-kde-41-vrij.html">tweakers.net</a> </li>
 </ul>
 <ul>
  <li>[GERMAN] Desktop im Aufwind: <a href="https://www.linux-community.de/Neues/story?storyid=26199">linux-community.de </a>  </li>
  <li>[GERMAN] KDE 4.1 erschienen: <a href="http://www.heise.de/newsticker/Unix-Linux-Desktop-KDE-in-Version-4-1-erschienen--/meldung/113517">heise.de</a>  </li>
  <li>[GERMAN] KDE 4.1 - neue Desktop-Generation f&uuml;r alle?: <a href="http://www.golem.de/0807/61328.html">golem.de</a>  </li>
  <li>[GERMAN] Interview: KDE 4 macht Fortschritte: <a href="http://www.golem.de/0807/61359.html">golem.de</a>  </li>
  <li>[GERMAN] KDE 4.1: Zahlreiche Verbesserungen f&uuml;r den Linux Desktop: <a href="http://derstandard.at/?url=/?id=1216918027468">derstandard.at </a>  </li>
  <li>[GERMAN] Don`t look back: KDE 4.1 erschienen: <a href="http://www.kubuntu-de.org/nachrichten/software/kde/dont-look-back-kde-4-1-erschienen">kubuntu-de.org</a>  </li>
  <li>[GERMAN] KDE 4.1 ver&ouml;ffentlicht: <a href="http://futurezone.orf.at/hardcore/stories/296506/">futurezone.orf.at</a></li>
  <li>[GERMAN]  Linux-Desktop im Aufwind: KDE 4.1 ist da: <a href="http://www.linux-magazin.de/news/linux_desktop_im_aufwind_kde_4_1_ist_da">linux-magazin.de</a> </li>
  <li>[GERMAN] KDE 4.1 ist fertig: <a href="http://www.pcwelt.de/start/software_os/linux/news/173332/kde_41_ist_fertig/">pcwelt.de</a></li>
  <li>[GERMAN] KDE 4.1 ver&ouml;ffentlicht : <a href="http://www.pro-linux.de/news/2008/12988.html">pro-linux.de</a></li>
  <li>[GERMAN] KDE 4.1 in Screenshots: <a href="http://www.pro-linux.de/berichte/kde41.html">pro-linux.de</a>  </li>
  <li>[GERMAN] KDE kehrt zu den Massen zur&uuml;ck: <a href=" http://www.silicon.de/software/business/0,39039006,39194234,00/kde+kehrt+zu+den+massen+zurueck.htm">silicon.de</a></li>
  <li>[GERMAN] Neue Features f&uuml;r KDE: <a href="http://www.testticker.de/news/2008/07/30/neue_features_f_r_kde">testticker.de</a></li>
</ul>
  <ul>
  <li>[ITALIAN] KDE 4.1 è qui, ecco il mio saluto: <a href="http://pollycoke.net/2008/07/30/kde-41-e-qui-ecco-il-mio-saluto/">punto-informatico.it</a></li>
  <li>[ITALIAN] Speciale/KDE 4.1 si sente pronto: <a href="http://punto-informatico.it/2374566/PI/News/speciale-kde-41-si-sente-pronto.aspx">punto-informatico.it</a></li>
  </ul>
  <ul>
  <li>[NORWEGIAN] KDE i ny versjon: <a href="http://www.hardware.no/artikler/kde_i_ny_versjon/54365">hardware.no</a> </li>
  <li>[NORWEGIAN] N&aring; kan du pr&oslash;ve KDE 4.1: <a href="http://www.itavisen.no/sak/781166/N&aring;_kan_du_pr&oslash;ve_KDE_4.1/">itavisen.no</a> </li>
  </ul>
  <ul>
  <li>[SWEDISH] KDE 4.1 ute nu - l&auml;s om nyheterna: <a href="http://www.idg.se/2.1085/1.172696">idg.se</a></li>
</ul>
