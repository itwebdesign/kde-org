---
title: KDE Plasma 5.10.5, Bugfix Release for August
release: plasma-5.10.5
version: 5.10.5
description: KDE Ships 5.10.5
date: 2017-08-22
layout: plasma
changelog: plasma-5.10.4-5.10.5-changelog
---

{{%youtube id="VtdTC2Mh070"%}}

{{<figure src="/announcements/plasma-5.10/plasma-5.10.png" alt="KDE Plasma 5.10 " class="text-center" width="600px" caption="KDE Plasma 5.10">}}

Tuesday, 22 August 2017.

{{% i18n_var "Today KDE releases a %[1]s update to KDE Plasma 5, versioned %[2]s" "Bugfix" "5.10.5." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in %[2]s with many feature refinements and new modules to complete the desktop experience." "5.10" "August" %}}

{{% i18n_var "This release adds a %[1]s worth of new translations and fixes from KDE's contributors.  The bugfixes are typically small but important and include:" "a month's" %}}

- A Plasma crash when certain taskbar applications group in a specific way
- Excluding OSD's from the desktop grid kwin effect
- Discover handling URL links to packages
