---
title: KDE 1.94 Release Announcement
date: "2000-09-15"
description: The KDE Team today announced the release of KDE 1.94, the fifth and final beta preview of KDE 2.0, KDE's next-generation, powerful, modular desktop. Following the release of KDE 1.93 on August 23, 2000, the release, code-named "Kandidat", is based on Trolltech's Qt 2.2.0 and will include the core KDE libraries, the core desktop environment, the KOffice suite, as well as the over 100 applications from the other core KDE packages&#58; Administration, Games, Graphics, Multimedia, Network, Personal Information Management (PIM), Toys and Utilities. The release is targeted at users who would like to help the KDE team make usability, speed and feature enhancements and fix the remaining set of bugs before the release of KDE 2.0, scheduled for early-fourth quarter 2000.
---

FOR IMMEDIATE RELEASE

Final Beta Preview of Leading Desktop for Linux<sup>&reg;</sup> and Other UNIXes<sup>&reg;</sup>

September 14, 2000 (The INTERNET). The <a href="/">KDE
Team</a> today announced the release of KDE 1.94, the fifth and final beta
preview of Kopernicus, KDE's next-generation, powerful, modular desktop.
Following the release of KDE 1.93 on August 23, 2000, the release,
code-named "Kandidat", is based on
<a href="http://www.trolltech.com/">Trolltech's</a><sup>tm</sup>
Qt<sup>&reg;</sup> 2.2.0 and will include the core KDE libraries,
the core desktop environment, the KOffice suite, as well
as the over 100 applications from the other core KDE packages:
Administration, Games, Graphics, Multimedia, Network, Personal
Information Management (PIM), Toys and Utilities.
The release is targeted at users who would like to help the
KDE team make usability, speed and feature enhancements and fix the remaining
set of bugs before the release of KDE 2.0 ("Kopernicus"), scheduled
for early-fourth quarter 2000.

The major changes since the Beta 3 release last month include:

<ul>
<li>General stability improvements throughout the packages.</li>
<li>Major performance enhancements to the HTML widget. It also now fully supports "mailto:" URLs and caches pages for fast subsequent retrieval.</li>
<li>The latest Qt release, qt-2.2.0, which for the first time is licensed under
the <a href="http://www.kde.com/legal/gpl_lic">GNU General Public
License</a>, as well as the
<a href="http://www.kde.com/legal/qpl1.0_lic">Q Public License</a>.</li>
<li>Fixes to FTP and HTTP proxy support and many improvements to FTP
support, including symlink support.</li>
<li>The Secure Socket Layer (SSL) code in <a href="http://konqueror.kde.org/">Konqueror</a> has been improved, although it is still undergoing heavy development.</li>
<li>Konqueror now features an "Open With" and "Save As" option for non-HTML links.</li>
<li>Useability enhancements, such as background colors for desktop icon text.</li>
</ul>

#### Highlights of Kandidat

<a href="http://konqueror.kde.org/">Konqueror</a> reigns as the
next-generation web browser, file manager and
document viewer for KDE 2.0. Widely acclaimed as a technological
break-through for the Unix desktop, Konqueror has a component-based
architecture which combines the features and functionality of Internet
Explorer<sup>&reg;</sup>/Netscape Communicator<sup>&reg;</sup> and
Windows Explorer<sup>&reg;</sup>. Konqueror will support
the full gamut of current Internet technologies, including
JavaScript, Java<sup>&reg;</sup>, HTML 4.0, CSS-2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator<sup>&reg;</sup> plug-ins (for
playing Flash<sup>TM</sup>,
RealAudio<sup>TM</sup>, RealVideo<sup>TM</sup> and similar technologies).
In addition,
Konqueror's network transparency offers seamless support for browsing
Linux<sup>&reg;</sup> NFS shares, Windows<sup>&reg;</sup> SMB shares,
HTTP pages, FTP directories as well as any other protocol for which
a KIO plug-in is available.

The <a href="http://koffice.kde.org/">KOffice suite</a> is one of the
most-anticipated
Open Source projects. The suite consists
of a spreadsheet application (KSpread), a vector drawing application
(KIllustrator), a frame-based word-processing application (KWord),
a presentation program (KPresenter), and a chart and diagram application
(KChart). Native file formats are XML-based, and work on
filters for proprietary binary file formats is progressing.
Combined with a powerful scripting language and the
ability to embed individuals components within each other using KDE's
KParts technology, the KOffice
suite will provide all the necessary functionality to all but the most
demanding power users, at an unbeatable price -- free.

KDE's customizability touches every aspect of this next-generation
desktop. <A id="Style engine">Kandidat benefits from Qt's
style engine, which permits developers and artists to create their
own widget designs down to the precise appearance of a scrollbar,
a button, a menu and more, combined with development tools which will
largely automate the creation of these widget sets. In addition, KDE
offers excellent support for GTK themes. Just to
mention a few of the legion configuration options,
users can choose among: numerous types of menu effects; a menu
bar atop the display (Macintosh<sup>&reg;</sup>-style) or atop each individual
window (Windows-style); icon styles; system sounds; key bindings;
languages; toolbar and menu composition; and much much more.

#### Downloading and Compiling the Final Beta

The source packages for Kandidat are available for free download at
<a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/src/">http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/src/</a> or in the
equivalent directory at one of the many KDE ftp server
<a href="../mirrors/ftp.php">mirrors</a>. Kandidat requires
qt-2.2.0, which is available from the above locations under the name
<a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/src/qt-x11-2.2.0.tar.gz">qt-x11-2.2.0.tar.gz</a>.
Please be advised that Kandidat will not work with any
older versions of Qt. Qt 2.2 is a stable product and not part of KDE's beta testing.

For further instructions on compiling and installing Kandidat, please consult
the <a href="http://developer.kde.org/build/index.html">installation
instructions</a> and, if you encounter problems, the
<a href="http://developer.kde.org/build/index.html">compilation FAQ</a>.

#### Installing Binary Packages of the Final Beta

The binary packages for Kandidat will be available for free download under
<a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/">http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="../mirrors/ftp.php">mirrors</a>. Kandidat requires
qt2.2.0, which is available from the above locations.
Please be advised that Kandidat will not work with any
older versions of Qt. Qt is not part of KDE's beta testing.

At the time of this release, pre-compiled packages are available for:

<ul>
<li><a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/Mandrake/RPMS/">Linux-Mandrake</a></li>
<li><a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/COL-2.4/RPMS/">Caldera Open Linux 2.4</a></li>
<li><a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/axp/6.4/">SuSE Linux AXP  6.4</a></li>
<li><a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/i386/6.4/">SuSE Linux i386 6.4</a></li>
<li><a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/i386/7.0/">SuSE Linux i386 7.0</a></li>
<li><a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/ppc/6.4/">SuSE Linux PPC  6.4</a></li>
<li><a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/rpm/SuSE/localized/">SuSE Linux noarch</a></li>
<li><a href="http://ftp.kde.org/pub/kde/unstable/distribution/2.0Beta5/tar/FreeBSD/">FreeBSD</a></li>
</ul>

Check the ftp servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days.

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
Currently development is focused on KDE 2, which will for the first time
offer a free, Open Source, fully-featured office suite and which promises to
make the Linux desktop as easy to use as Windows<sup>&reg;</sup> and
the Macintosh<sup>&reg;</sup>
while remaining loyal to open standards and empowering developers and users
with Open Source software. KDE is working proof of how the Open Source
software development model can create first-rate technologies on par with
and superior to even the most complex commercial software.

For more information about KDE, please visit KDE's <a href="http://www.kde.org/whatiskde/">web site</a>.

<table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
    Kurt Granroth <br>
    
  [granroth@kde.org](mailto:granroth@kde.org)
    <br>
    (1) 480 732 1752<br>&nbsp;<br>
    Andreas Pour<br>
    [pour@kde.org](mailto:pour@kde.org)<br>
    (1) 718-456-1165
  </td>
</tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Martin Konold<br>
    
  [konold@kde.org](mailto:konold@kde.org)
    <br>
    (49) 179 2252249
  </td>
</tr>
</table>
