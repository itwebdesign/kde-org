---
title: KDE Plasma 5.6.2, bugfix Release for April
release: "plasma-5.6.2"
description: KDE Ships Plasma 5.6.2.
date: 2016-04-05
layout: plasma
changelog: plasma-5.6.1-5.6.2-changelog
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma-5.6/plasma-5.6.png" alt="KDE Plasma 5.6 " class="text-center mt-4" width="600px" caption="KDE Plasma 5.6">}}

Tuesday, 05 April 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.6.2. <a href='https://www.kde.org/announcements/plasma-5.6.0.php'>Plasma 5.6</a> was released in March with many feature refinements and new modules to complete the desktop experience.

This release adds another week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Weather plasmoid, bbcukmet. Update to BBC's new json-based search and modified xml. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=84fe5785bd1520f17a801cfe2e263c8ba872b273">Commit.</a> Fixes bug <a href="https://bugs.kde.org/330773">#330773</a>
- Breeze and Oxygen widget themes: Add isQtQuickControl function and make it work with Qt 5.7. <a href="http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=f59ae8992d18718d596fd332389b3fe98ff21a10">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/127533">#127533</a>
- [calendar] Fix calendar applet not clearing selection when hiding. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d3beb0b647a543436d3d23ab82b39a2f98a384be">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/127456">#127456</a>. Fixes bug <a href="https://bugs.kde.org/360683">#360683</a>
