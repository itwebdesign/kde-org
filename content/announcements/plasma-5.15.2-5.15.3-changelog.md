---
title: Plasma 5.15.3 Complete Changelog
version: 5.15.3
hidden: true
plasma: true
type: fulllog
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Don't crash if the fwupd error is null. <a href='https://commits.kde.org/discover/1d8ab1561d0b328b641789fbafe581d7ddfdea58'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19561'>D19561</a>
- Flatpak: Fix look-up of updates on flatpak. <a href='https://commits.kde.org/discover/2b7e28d372c20d51913b4e82a1493caee3e5d998'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404434'>#404434</a>
- Flatpak: fix warning. <a href='https://commits.kde.org/discover/67ad21e9f8b0720d928aa5c4c63a03f79bd42e86'>Commit.</a>
- Flatpak: Report errors when running a transaction. <a href='https://commits.kde.org/discover/13557f71e486b5711d1bd686526a88b5219adf9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404913'>#404913</a>
- Set the sources label to textFormat: Text.StyledText. <a href='https://commits.kde.org/discover/1b569001aac76d1a733a7ed45cf249da18c10a7a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404755'>#404755</a>

### <a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a>

- Warning--. <a href='https://commits.kde.org/drkonqi/280dd67b3f7f9b5442ba3e6a0f3306bd4836da8e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19415'>D19415</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Fix crash when installing gtk themes locally. <a href='https://commits.kde.org/kde-gtk-config/1bdaa7db43f2645ff1e4607545048f4eb395e26d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398112'>#398112</a>
- Explicitly render svgs into images that are the right size. <a href='https://commits.kde.org/kde-gtk-config/301497fb33177ac305a7621a98de867dbb649074'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396586'>#396586</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Consistent arrow key handling in the Informative Alt+Tab skin. <a href='https://commits.kde.org/kdeplasma-addons/d7237df172e5a9065e387c94336cc38fd5c1fc13'>Commit.</a> See bug <a href='https://bugs.kde.org/370185'>#370185</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16093'>D16093</a>

### <a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a>

- Style++. <a href='https://commits.kde.org/kinfocenter/515575b9495b2c487032a6c4cca6d8f2e882e5fd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19704'>D19704</a>
- Single-clicks correctly activate modules again when the system is in double-click mode. <a href='https://commits.kde.org/kinfocenter/dbaf5ba7e8335f3a9c4577f137a7b1a23c6de33b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405373'>#405373</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19703'>D19703</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [platforms/x11] Properly unload effects on X11. <a href='https://commits.kde.org/kwin/38e22ce6d18ca3f71cc1b3213ce4fc7de9cc8ef4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399572'>#399572</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19178'>D19178</a>
- Properly restore current desktop from session. <a href='https://commits.kde.org/kwin/a9e469bf13f6d99c24eb2ae24c3a0f4ec0a79d61'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390295'>#390295</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19520'>D19520</a>
- [effects/screenshot] Set m_windowMode in screenshotWindowUnderCursor. <a href='https://commits.kde.org/kwin/2ea6fc2abefd810915c38daee95fe31205ca741b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374864'>#374864</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19552'>D19552</a>

### <a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a>

- Add break to fix tty information. <a href='https://commits.kde.org/libksysguard/dcb0901e543843cbeb9edec702aeadc72bde229b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19064'>D19064</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Make Ctrl+A work regardless of focus and visualize active selection in search heading. <a href='https://commits.kde.org/plasma-desktop/4d0f1a3e6ec76469b918b1c853004d2d6df4fd6d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19518'>D19518</a>
- [Task Manager] Fix virtual desktops subtext on task tooltip. <a href='https://commits.kde.org/plasma-desktop/d769b7aa8838e95e20aa3b52ae5a0db575111416'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19257'>D19257</a>
- Reset tooltip delegate label height to defaults. <a href='https://commits.kde.org/plasma-desktop/ef06af7c01a95339a8f44aafa82e0fd619a9efe9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401842'>#401842</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18640'>D18640</a>
- [kcolorschemeditor] Don't re-add existing tab. <a href='https://commits.kde.org/plasma-desktop/45f10322b8f33b7f7a38e3a1cafdbbc030a484c4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393282'>#393282</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19572'>D19572</a>
- [Task Manager] Make sure "Alternatives..." context menu item is always available. <a href='https://commits.kde.org/plasma-desktop/fd7b359f84617cbab6a232007789b2ed618c4395'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404969'>#404969</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19472'>D19472</a>
- [ConfigCategoryDelegate] Add horizontal padding to the label. <a href='https://commits.kde.org/plasma-desktop/ff0a0e30a28a0e34bcf87b719a3e58255a66e421'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19275'>D19275</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- Search our cmake dir first. <a href='https://commits.kde.org/plasma-pa/3f6a9d90123ba86e4e1799177c56342b78312746'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19381'>D19381</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [Task Manager] Fix sorting of tasks on last desktop in sort-by-desktop mode. <a href='https://commits.kde.org/plasma-workspace/53290043796c90207c5b7b6aad4a70f030514a97'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19259'>D19259</a>
- [notifications] Lift up notification content if one line of body text droops. <a href='https://commits.kde.org/plasma-workspace/23f3345a22878eadc58a54eed27ad9a231877bca'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404407'>#404407</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19036'>D19036</a>
- [OSD] Fix animation stutter. <a href='https://commits.kde.org/plasma-workspace/9a7de4e02399880a0e649bad32a40ad820fc87eb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19566'>D19566</a>
- [shell] Show kactivies warning only on error. <a href='https://commits.kde.org/plasma-workspace/70ef0829982e395ca1243bf9ff84d9368726239b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18922'>D18922</a>

### <a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a>

- Fix crash when mutating a container while iterating it. <a href='https://commits.kde.org/powerdevil/980156cda79e573d99a5bdc07fe379aac07cc42e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403776'>#403776</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Set colorSet on categories header. <a href='https://commits.kde.org/systemsettings/441697ca9b81e78a27075ef2b40dde170bc81259'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19682'>D19682</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Use high dpi pixmaps. <a href='https://commits.kde.org/xdg-desktop-portal-kde/90f655a146d1fd19a4ac6c14cf3452f9f7034b0b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405160'>#405160</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19580'>D19580</a>
- [File Chooser Portal] Confirm overwrite on saving. <a href='https://commits.kde.org/xdg-desktop-portal-kde/194fb4dee8cbfef9ae72968367f20182f517efa0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404719'>#404719</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19441'>D19441</a>
