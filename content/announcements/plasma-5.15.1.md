---
title: KDE Plasma 5.15.1, Bugfix Release for February
release: "plasma-5.15.1"
version: "5.15.1"
description: KDE Ships Plasma 5.15.1.
date: 2019-02-19
layout: plasma
changelopg: plasma-5.15.0-5.15.1-changelog
---

{{<figure src="/announcements/plasma-5.15/plasma-5.15-apps.png" alt="Plasma 5.15" class="text-center" width="600px" caption="KDE Plasma 5.15">}}

Tuesday, 19 February 2019.

{{% i18n_var "Today KDE releases a Bugfix update to KDE Plasma 5, versioned %[1]s" "5.15.1." %}}

{{% i18n_var "<a href='https://www.kde.org/announcements/plasma-%[1]s.0.php'>Plasma %[1]s</a> was released in February with many feature refinements and new modules to complete the desktop experience." "5.15" %}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Set parent on newly created fwupd resource. <a href="https://commits.kde.org/discover/baac08a40851699585e80b0a226c4fd683579a7b">Commit.</a> Fixes bug <a href="https://bugs.kde.org/402328">#402328</a>. Phabricator Code review <a href="https://phabricator.kde.org/D18946">D18946</a>
- Fix System Tray popup interactivity after changing item visibility. <a href="https://commits.kde.org/plasma-workspace/6fcf9a5e03ba573fd0bfe30125f4c739b196a989">Commit.</a> Fixes bug <a href="https://bugs.kde.org/393630">#393630</a>. Phabricator Code review <a href="https://phabricator.kde.org/D18805">D18805</a>
- [Digital Clock] Fix 24h tri-state button broken in port to QQC2. <a href="https://commits.kde.org/plasma-workspace/006c4f5f9ee8dfb3d95604a706d01b968c1e1c8a">Commit.</a> Fixes bug <a href="https://bugs.kde.org/404292">#404292</a>
