---
title: KDE Plasma 5.7.5, bugfix Release for September
release: plasma-5.7.5
description: KDE Ships Plasma 5.7.5.
date: 2016-09-13
layout: plasma
chnagelog: plasma-5.7.4-5.7.5-changelog
---

{{<figure src="/announcements/plasma-5.7/plasma-5.7.png" alt="KDE Plasma 5.7 " class="text-center" width="600px" caption="KDE Plasma 5.7">}}

Tuesday, 13 September 2016.

Today KDE releases a bugfix update to KDE Plasma 5, versioned 5.7.5. <a href='https://www.kde.org/announcements/plasma-5.7.0.php'>Plasma 5.7</a> was released in July with many feature refinements and new modules to complete the desktop experience.

This release adds month' worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Plasma Workspace: Fix some status notifier items not appearing. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=df4387a21f6eb5ede255ea148143122ae4d5ae9c">Commit.</a> Fixes bug <a href="https://bugs.kde.org/366283">#366283</a>. Fixes bug <a href="https://bugs.kde.org/367756">#367756</a>
- SDDM Config - Fix themes list with SDDM 0.14. <a href="http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=d4ca70001222c5b0a9f699c4639c891a6a5c0c05">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/128815">#128815</a>
- Make sure people are not trying to sneak invisible characters on the kdesu label. <a href="http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=5eda179a099ba68a20dc21dc0da63e85a565a171">Commit.</a>
