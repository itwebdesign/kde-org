---
title: KDE Official Response to Stallman Editorial
date: "2000-09-05"
description: KDE Response to Stallman Editorial In a recent editorial on Linux Today, Richard Stallman claimed that KDE is still in violation of the GPL even though Qt is now covered under the GPL and all KDE code is compatible with the GPL. His rather absurd reasoning is that since KDE once violated the GPL, it will always be in violation unless the individual copyright holders "grant forgiveness.
---

In a recent <a href="http://linuxtoday.com/news_story.php3?ltsn=2000-09-05-001-21-OP-LF-KE">editorial</a> on Linux Today, Richard Stallman claimed that
KDE is still in violation of the GPL even though Qt is now covered
under the GPL and all KDE code is compatible with the GPL. His rather
absurd reasoning is that since KDE once violated the GPL, it will
always be in violation unless the individual copyright holders "grant
forgiveness."

We maintain, as always, that there are no licensing problems with KDE.
Here's why:

All code in KDE is copyrighted and covered under a free software (Open
Source) license. The vast majority of the code was written
explicitely for KDE. A few bits were written elsewhere and
incorporated into KDE.

In the former case, Stallman himself has said that there is and never
was any problem with license compatibilities. In an email to the
debian-legal mailing list<a href="#1">[1]</a>, he writes "if the
authors of the program clearly intended it to be linked against Qt, I
would say they have given some kind of implicit permission for people
to do that."

In the latter case, his reasoning just doesn't make sense. There are
only two parts of KDE that have GPLed code not written explicitely for
KDE -- a small bit in kmidi and a few lines in kghostview. According
to Stallman, because we once linked that code to a library not
compatible with the GPL, we now have to beg forgiveness from the
copyright holders of that code or we will forever be in violation.

The "solution" to this is simple: we remove the "tainted" code from
kmidi and kghostview, release a "pure" version of each, then re-add
those files. Since adding non-tainted code is fine, we would be
cleared.

This entire thing is just too absurd and we refuse to play this game.

That said, if you or somebody you know has code in KDE whose copyright
we really are violating, please speak up and send an email to
[kde@kde.org](mailto:kde@kde.org) so that we may fix the issue. In case of doubt, we have
compiled three documents: one listing all modules in KDE and the
license it follows<a href="#2">[2]</a>, one listing all copyright
holders in all code inside of the KDE CVS<a href="#3">[3]</a>, and one
with all copyright holders email addresses<a href="#4">[4]</a>

<span id="1">[1]</span> <a href="http://lists.debian.org/debian-legal-0006/msg00062.html">http://lists.debian.org/debian-legal-0006/msg00062.html</a><br>
<span id="2">[2]</span> <a href="http://developer.kde.org/documentation/licensing/licensing.html">http://developer.kde.org/documentation/licensing/licensing.html</a><br>
<span id="3">[3]</span> <a href="http://developer.kde.org/documentation/licensing/copyright-lines.html">http://developer.kde.org/documentation/licensing/copyright-lines.html</a><br>
<span id="4">[4]</span> <a href="http://developer.kde.org/documentation/licensing/email-addresses.html">http://developer.kde.org/documentation/licensing/email-addresses.html</a><br>
