---
title: Improved Layout, Scriptability and Filters Highlight KOffice Beta Release for GNU/Linux and Other UNIXes
date: 2002-04-25
---

DATELINE APRIL 25, 2002<br />
FOR IMMEDIATE RELEASE

## KDE Ships Beta Test Release of Free Integrated Office Suite

<strong>Improved Layout, Scriptability and Filters Highlight KOffice
Beta Release for GNU/Linux and Other UNIXes</strong>
April 25, 2002 (The INTERNET).
The <a href="http://www.kde.org/">KDE Project</a> today announced the
immediate release of
<a href="http://www.koffice.org/">KOffice</a> 1.2beta1, the first preview
release for KOffice 1.2,
<a href="http://developer.kde.org/development-versions/koffice-1.2-release-plan.html">scheduled</a>
for release this September.
KOffice is an integrated office suite for KDE which utilizes free and
open standards for its document formats, component communication and
component embedding.
This release is principally a public testing and developer solicitation
release (more help is welcome!).

This release

includes
a frame-based, full-featured word processor
(<a href="http://www.koffice.org/kword/">KWord</a>);
a presentation application;
(<a href="http://www.koffice.org/kpresenter/">KPresenter</a>);
a spreadsheet application;
(<a href="http://www.koffice.org/kspread/">KSpread</a>);
a flowchart application;
(<a href="http://www.thekompany.com/projects/kivio/">Kivio</a>);
business quality reporting software;
(<a href="http://www.thekompany.com/projects/kugar/">Kugar</a>);
and two vector-drawing applications (<em>alpha</em>)
(<a href="http://www.koffice.org/kontour/">Kontour</a> and
<a href="http://www.koffice.org/karbon/">Karbon14</a>).
Additionally, KOffice includes robust embeddable charts
(<a href="http://www.koffice.org/kchart/">KChart</a>)
and formulas
(<a href="http://www.koffice.org/kformula/">KFormula</a>)
as well as a built-in thesaurus (KThesaurus)
and numerous import and export
<a href="http://www.koffice.org/filters/">filters</a>.
KOffice 1.2beta1 is available only in English; translations are expected
to have caught up by the next beta release.

KOffice 1.2beta1 complements the release of
<a href="http://www.kde.org/announcements/announce-3.0.php">KDE 3.0</a>
late last month. KDE is a powerful and easy-to-use Internet-enabled
desktop for GNU/Linux and other UNIXes, and together with KOffice constitutes the
only Open Source project to provide a complete desktop and productivity
environment for GNU/Linux - UNIX.

KOffice and all its components (including KDE) are available
<em><strong>for free</strong></em> under Open Source licenses from the KDE
<a href="http://download.kde.org/unstable/koffice-1.2-beta1/src/">http</a>
or <a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.

#### Principal Improvements

The principal improvements over KOffice 1.1.1 are as follows:

<u>General</u>.

<ol type="disc">
* <em>WYSIWYG</em>.
KWord, KPresenter and the formula objects have received an
overhaul in the presentation:  text layout and painting have
been redesigned around the popular and intuitive WYSIWYG (What
You See Is What You Get) design concept.

- <em>SVG Support</em>. KOffice components can now handle many
  SVG images (animation is not yet supported).

- <em>DCOP (Remote Control)</em>. All KOffice components have a
  greatly enhanced DCOP scriptability. For example, DCOP can be
  used to insert or reformat text in KWord and KPresenter.

- <em>Spell checking</em>.
  KPresenter and KSpread now have a spell checker.

- <em>Thesaurus</em>.
  KThesaurus is a new KOffice component. It is a thesaurus which
  can also be used on a stand-alone basis as a frontend to a local
  installation of WordNet.

- <em>Word Completion</em>.
  KWord and KPresenter offer a word completion feature. By using
  a key shortcut, these office applications will try to complete
  the word using a word-list, consisting of the words in the current
  document or compiled from another document.

- <em>File Format</em>. KOffice has switched to zip (instead of
  tar and gzip) for archiving/compressing its native file
  format and to using "relative paths" for objects included in
  the archive (such as images or embedded components). This provides
  greatly improved document compatability with other Open Source
  office suites, while enhancing performance. For backwards
  compatability, KOffice 1.2 will support saving in the KOffice
  1.1 format, and KOffice 1.1 users can download a utility to convert
  from the KOffice 1.2 to the KOffice 1.1 format.

- <em>Bi-Di</em>. KWord and KPresenter now feature full support for
  reading and writing bi-directional text, such as Arabic and Hebrew
  (<a href="http://www.koffice.org/kword/pics/kword-bidi.png">screenshot</a>).

- <em>Auto-hide Cursor</em>. Both KWord and KPresenter now hide
  the cursor when the mouse has been idle for some time.

</ol>

<u>KWord (Word Processor)</u>.

<ol type="disc">
* <em>Image handling</em>.  You can replace the image in a frame (rather
than having to create a new frame to replace an image).

- <em>Tables</em>. Table borders have been properly implemented.
  The sizing of variable width table cells has been greatly improved.

- <em>Frames</em>.
  You can now choose where to inline a new frame. Frames now have a
  "z-order", so they can be lowered and raised. Layered frames are
  currently opaque.

- <em>Text</em>.
  KWord now supports double underline, text shadow, soft hyphens,
  non-breaking spaces and a text background color. In addition,
  comments can be added to text.

- <em>Filters</em>.
  KWord has a number of new import/export filters for enhanced
  compatibility with other office suites.

- The new export/import filters are AbiWord (substantial support),
  PalmDoc (full support), Unicode or plain text (full support),
  WML (limited support) and Lotus AmiPro (limited support).

- The new import filters (with limited support) are MSWrite and
  WordPerfect.
- The new export filter is SGML (substantial support).

- HTML export/import has been greatly enhanced, though for this
  release HTML tables are ignored. Latex export has also
  greatly improved.

- <a href="http://www.w3.org/Style/XSL/">XSLT</a> support for
  mapping to other XML-based office formats.

* <em>Shortcuts</em>.
  You can now assign a style to a key shortcut. New shortcuts
  have been added for non-breakable spaces and soft hyphens.
  A triple-click now selects a paragraph. The
  <code>PageUp</code>/<code>PageDown</code> keys now default to
  moving only the scrollbars, not the cursor.

* <em>Miscellaneous</em>.
  KWord now supports hyperlinks (an Internet, email or file link).
  The handling of tabs in the ruler has been improved.

</ol>

<u>KPresenter (Presentation Tool)</u>.

<ol type="disc">
* <em>Sound</em>.
KPresenter has become a multimedia application with the addition
of sound support.

- <em>New tools</em>.
  A set of new tools for creating professional presentations: freehand,
  polyline, quadric bezier curve, cubic bezier curve and
  convex/concave polygon. A new "Configure Image" dialog supports a
  number of operations on images, such as flip/mirror, color depth,
  RGB to BGR conversion and brightness control.
-

<em>Page views</em>. A new thumbnail view of each page in the
presentation is now displayed in the sidebar, and a larger view of
the current page is displayed **\_**.

- <em>Note bar</em>.
  A new notebar allows you to add comments to each page of a
  presentation.

- <em>Zooming</em>.
  KPresenter now has zooming support. Besides the standard
  percentage zoom, you can zoom to a region selected with the mouse.

- <em>Layout</em>.
  Improved page layout, so changes in one page no longer cause other
  pages to misalign. The position and size of an object can now be
  set to fit on the page. A grid system has been implemented.

- <em>Transition effect</em>.
  New transition effects include Blinds, Box In/Out, Checkboard,
  Cover, Uncover, Dissolve, Random and more.

- <em>Styles</em>.
  KPresenter now supports styles for text and paragraph
  formatting.

- <em>Miscellaneous</em>.
  KPresenter now has a useful status bar. The undo/redo
  functionality was extended to include page insertion,
  deletion, pasting and duplication. The search and replace functions
  work over all pages. KPresenter has new "guide lines" and "guide
  points" for helping to layout objects on the page. It can also
  report the duration of a slide-show presentation.

</ol>

<u>KSpread (Spreadsheet)</u>.

<ol type="disc">
* <em>Columns/rows</em>.
KSpread can now handle over 32,000 rows and
over 32,000 columns.
* 
<em>Filters</em>.
KSpread has a new dBASE import filter.
* 
<em>Functions</em>.
Function names are now case-insensitive.  The
formula editor has a new "Related Function" feature.
Thirty-seven new built-in functions have been added:
integer/boolean/text conversion functions, 7 new math
functions, 5 new text functions, 4 new date/time functions, 4 new
financial functions (including a <code>EURO</code> function for
converting between the Euro and other currencies), and 20 new
statistical functions.

</ol>

<u>Karbon14</u>.
Karbon14 is a new vector drawing application, which, while still
immature, is being released in an <em>alpha</em> state in this
release. Its current features include: basic path based shapes;
basic affine transformations; anti-aliased rendering with transparency;
Illustrator file import; Encapsulated PostScript (EPS) import
and export; and SVG export.

A more complete
<a href="http://www.koffice.org/announcements/changelog-1.2beta1.phtml">list
of changes</a> and <a href="http://www.koffice.org/releases/">notes about
the release</a> are available at the KOffice
<a href="http://www.koffice.org/">web site</a>.

#### Installing KOffice 1.2beta1 Binary Packages

<u>Binary Packages</u>.
Some GNU/Linux - UNIX OS vendors have kindly provided binary packages of
KDE 3.0 for some versions of their distribution, and in other cases
community volunteers have done so.
Some of these binary packages are available for free download from KDE's
<a href="http://download.kde.org/unstable/koffice-1.2-beta1/">http</a> or
<a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
Additional binary packages, as well as updates to the packages now
available, may become available over the coming weeks.

Please note that the KDE Project makes these packages available from the
KDE web site as a convenience to KDE users. The KDE Project is not
responsible for these packages as they are provided by third
parties - typically, but not always, the distributor of the relevant
distribution - using tools, compilers, library versions and quality
assurance procedures over which KDE exercises no control. If you
cannot find a binary package for your OS, or you are displeased with
the quality of binary packages available for your system, please read
the <a href="http://www.kde.org/packagepolicy.html">KDE Binary Package
Policy</a> and/or contact your OS vendor.

<u>Library Requirements / Options</u>.
The library requirements for a particular binary package vary with the
system on which the package was compiled. Please bear in mind that
some binary packages may require a newer version of Qt and other libraries
than was shipped with the system (e.g., LinuxDistro X.Y
may have shipped with Qt-3.0.0 but the packages below may require
Qt-3.0.3). For general library requirements for KDE, please see the text at
<a href="#source_code-library_requirements">Source Code - Library
Requirements</a> below.

<a name="package_locations"><u>Package Locations</u></a>.
At the time of this release, pre-compiled packages are available for:

- <a href="http://www.conectiva.com/">Conectiva Linux</a> (<a href="http://download.kde.org/unstable/koffice-1.2-beta1/Conectiva/README">README</a>)

- 8.0: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/Conectiva/CL_8/RPMS/">Intel i386</a>

- 7.0: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/Conectiva/CL_7/RPMS/">Intel i386</a>

* <a href="http://www.linux-mandrake.com/en/">Mandrake Linux</a>

* 8.2: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/Mandrake/8.2/">Intel i586</a>
* 8.1: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/Mandrake/8.1/">Intel i586</a>
* 8.0: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/Mandrake/8.0/">Intel i586</a>

- <a href="http://www.suse.com/">SuSE Linux</a>:

- 8.0: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/SuSE/i386/8.0/">Intel i386</a>
- 7.3: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/SuSE/i386/7.3/">Intel i386</a>, <a href="http://download.kde.org/stable/koffice-1.2beta1/SuSE/ppc/7.3/">PowerPC</a> and <a href="http://download.kde.org/stable/koffice-1.2beta1/SuSE/sparc/7.3/">Sun Sparc</a>
- 7.2: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/SuSE/i386/7.2/">Intel i386</a>
- 7.1: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/SuSE/i386/7.1/">Intel i386</a>
- 7.0: <a href="http://download.kde.org/unstable/koffice-1.2-beta1/SuSE/i386/7.0/">Intel i386</a>, <a href="http://download.kde.org/stable/koffice-1.2beta1/SuSE/ppc/7.0/">PowerPC</a> and <a href="http://download.kde.org/stable/koffice-1.2beta1/SuSE/s390/7.0/">IBM S390</a>

Please check the servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days and weeks.

#### Compiling KOffice 1.2beta1

<u>Library
Requirements</u>.
KOffice 1.2beta1 requires the following libraries:

- kdelibs 3.0, which can be downloaded following the
  <a href="http://www.kde.org/announcements/announce-3.0.php#package_locations">instructions</a>
  at the KDE website (for more information on these kdelibs releases, please
  see the <a href="http://www.kde.org/announcements/announce-3.0.php">KDE
  3.0 press release</a>);

- Qt-3.0.3, which is available in source code from Trolltech as
  <a href="ftp://ftp.trolltech.com/qt/source/qt-x11-3.0.3.tar.gz">qt-x11-3.0.3.tar.gz</a>;
  and

- for reading help pages and other KOffice documentation,
  <a href="http://xmlsoft.org/">libxml2</a> &gt;= 2.4.9 and
  <a href="http://xmlsoft.org/XSLT/">libxslt</a> &gt;= 1.0.7.

<u>Compiler Requirements</u>.
Please note that some components of
KOffice 1.2beta1 (such as the Quattro Pro import filter
and <a href="http://www.koffice.org/kchart/">KChart</a>) will not
compile with older versions of <a href="http://gcc.gnu.org/">gcc/egcs</a>,
such as egcs-1.1.2 or gcc-2.7.2. At a minimum gcc-2.95-\* is required.

<u>Source Code/SRPMs</u>.
The complete source code for KOffice 1.2beta1 is available for free download
via one of the KDE
<a href="http://download.kde.org/unstable/koffice-1.2-beta1/src/">http</a>
or <a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
Additionally, source rpms are available for the following distributions:

- Conectiva <a href="http://download.kde.org/unstable/koffice-1.2-beta1/Conectiva/CL_7/SRPMS/">7.0</a>, <a href="http://download.kde.org/unstable/koffice-1.2-beta1/Conectiva/CL_8/SRPMS/">8.0</a>

- Mandrake Linux <a href="http://download.kde.org/unstable/koffice-1.2-beta1/Mandrake/SRPMS/">8.x</a>

<u>Further Information</u>.
For further instructions on compiling and installing KOffice, please consult
the KOffice <a href="http://www.koffice.org/install-source.phtml">installation
instructions</a>. For
problems with SRPMs, please contact the person listed in the applicable
.spec file.

#### Corporate KOffice Sponsors

Besides the valuable and excellent efforts by the
<a href="http://www.koffice.org/developers.phtml">KOffice developers</a>
themselves, significant support for KOffice development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> (which sponsors
KOffice developers <a href="http://perso.mandrakesoft.com/~david/">David
Faure</a> and Laurent Montel),
<a href="http://www.thekompany.com/">theKompany.com</a> (which has
contributed Kivio and Kugar to KOffice), and
<a href="http://www.klaralvdalens-datakonsult.se/">Klar&auml;lvdalens
Datakonsult AB</a> (which has contributed KChart to KOffice). In addition,
the members of the <a href="http://www.kdeleague.org/">KDE League</a>,
as well as <a href="http://www.kde.org/donations.html">individual
sponsors</a> (<a href="http://www.kde.org/support.html">donate</a>),
provide significant support for KDE and KOffice. Thanks!

#### About KOffice/KDE

KOffice is part of the KDE Project.
KDE is an independent project of hundreds of developers, translators,
artists and professionals worldwide collaborating over the Internet
to create and freely distribute a sophisticated, customizable and stable
desktop and office environment employing a flexible, component-based,
network-transparent architecture and offering an outstanding development
platform. KDE provides a stable, mature desktop, a full, component-based
office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
set of networking and administration tools and utilities, and an
efficient, intuitive development environment featuring the excellent IDE
<a href="http://www.kdevelop.org/">KDevelop</a>. KDE is working proof
that the Open Source "Bazaar-style" software development model can yield
first-rate technologies on par with and superior to even the most complex
commercial software.

Please visit the KDE family of web sites for the
<a href="http://www.kde.org/faq.html">KDE FAQ</a>,
<a href="http://www.kde.org/screenshots/kde3shots.html">screenshots</a>,
<a href="http://www.koffice.org/">KOffice information</a> and
<a href="http://developer.kde.org/documentation/kde3arch/">developer
information</a>.
Much more <a href="http://www.kde.org/whatiskde/">information</a>
about KDE is available from KDE's
<a href="http://www.kde.org/family.html">family of web sites</a>.

<hr>

<font size="2">
<em>Release Coordinator</em>:  Thanks to
<a href="http://perso.mandrakesoft.com/~david/">David Faure</a> for
his excellent services as release coordinator.
</font>

<font size="2">
<em>Press Release</em>:  Written by <a href="&#x6d;&#97;&#x69;lto:&#x70;&#0111;&#117;r&#64;&#107;de.&#x6f;&#x72;&#103;">Andreas
Pour</a> from the <a href="http://www.kdeleague.org/">KDE League</a>, with the
invaluable assistance of numerous gifted volunteers from the KDE Project.
</font>

<font size="2">
<em>Trademarks Notices.</em>

KDE, K Desktop Environment and KOffice are trademarks of KDE e.V.
Incorporated.

Adobe Illustrator and PostScript are registered trademarks of Adobe Systems

dBASE is a registered trademark of dBASE Inc.

Corel, Quattro Pro and WordPerfect are registered trademarks of Corel
Corporation or Corel Corporation Limited.

Linux is a registered trademark of Linus Torvalds.

Trolltech and Qt are trademarks of Trolltech AS.

UNIX is a registered trademark of The Open Group.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

<hr>
<table border="0" cellpadding="8" cellspacing="0">
<tr
<th colspan="2" align="left">
Press Contacts:
</th>
</tr>
<tr valign="top">
<td align="right" nowrap>
United&nbsp;States:
</td>
<td nowrap>
Andreas Pour<br />
KDE League, Inc.<br />
pour@kdeleague.org<br />
(1) 917 312 3122
</td>
</tr>
<tr valign="top">
<td align="right" nowrap>
Europe (French and English):
</td>
<td nowrap>
David Faure<br />
f&#x61;ure&#64;k&#100;&#101;&#046;&#00111;&#114;g<br />
(33) 4 3250 1445
</td>
</tr>
<tr valign="top">
<td align="right" nowrap>
Europe (German and English):
</td>
<td nowrap>
Ralf Nolden<br />
&#00110;o&#x6c;den&#0064;&#x6b;&#00100;&#101;&#x2e;o&#x72;g<br />
(49) 2421 502758
</td>
</tr>
<tr valign="top">
<td align="right" nowrap>
Europe (English):
</td>
<td nowrap>
Jono Bacon<br />
j&#111;n&#111;&#x40;kd&#x65;&#x2e;&#111;&#114;&#x67;
</td>
</tr>
</table>
