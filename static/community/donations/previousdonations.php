<div>
<?php
require("www_config.php");
function display_graph(int $year, $dbConnection)
{
    for ($i = 1; $i <= 12; $i++) {
        $i < 10 ? $i_s = "0".$i : $i_s = $i;
        $data[ $year."-".$i_s ] = 0;
    }
    $stmt = $dbConnection->prepare("SELECT SUM(amount) as don, DATE_FORMAT(date,\"%Y-%m\") as month from donations WHERE YEAR(date) = :year GROUP BY month ORDER BY month DESC");

    $stmt->execute([
        'year' => $year,
    ]);

    while ($row = $stmt->fetch()) {
        $data[ $row["month"] ] = $row["don"];
    } ?>
<div class="col-12 col-md-6 offset-md-3">
<canvas id="myChart<?= $year ?>" width="400" height="200"></canvas>
</div>
<script>
let ctx<?= $year ?> = document.getElementById('myChart<?= $year ?>');
new Chart(ctx<?= $year ?>, {
    type: 'bar',
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [{
            label: "Donation for the year <?= $year ?>",
            data: [<?php foreach ($data as $month) { echo($month . ","); } ?>],
            fill: false,
        }],
    },
    options: { scales: { yAxes: [{ ticks: { beginAtZero: true } }] } }
});
</script>
    <?php
}

$query = $dbConnection->prepare("SELECT *, UNIX_TIMESTAMP(date) AS date_t FROM donations WHERE date >= :beginn AND date <= :end ORDER BY date DESC;");
$count = $dbConnection->prepare("SELECT COUNT(*) WHERE date >= :beginn AND date <= :end ;");

for ($year = date("Y", time()); $year > 2001; $year--) {
    echo "<h3>$year</h3>";
    echo "<figure class='text-center'>";
    display_graph($year, $dbConnection);
    echo "</figure>";
    echo "<br><br>";
    for ($month = 12; $month >=1; $month--) {
        if (date("n", time()) + 1 <= $month) {
            continue;
        }
        $month < 10 ? $month_s = "0".$month : $month_s = $month;
        $count->execute([
            'beginn' => $year.'-'.$month_s.'-01',
            'end' => $year.'-'.$month_s.'-31 23:59:59',
        ]);
        if ($count->fetchColumn() === 0) {
            continue;
        }
        $query->execute([
            'beginn' => $year.'-'.$month_s.'-01',
            'end' => $year.'-'.$month_s.'-31 23:59:59',
        ]);
        $total = 0;
        echo "<table border=1 width=\"600\">";
        echo "<tr><th colspan=3>".date("Y - F", mktime(0,0,0,$month,1,$year))."</th></tr>";
        echo "<tr><th width=100>Date</th><th width=100>Amount</th><th width=400>Message</th></tr>";
        while ($row = $query->fetch()) {
            $msg = htmlentities($row["message"]);
            if ($msg == "") {
                $msg = "<i>Anonymous donation</i>";
            }
            $total += $row["amount"];

            echo "<tr>";
            echo "<td>".date("jS H:i", $row["date_t"])."</td>";
            echo "<td align=right>&euro;&nbsp;".number_format($row["amount"],2)."</td>";
            echo "<td>".$msg."</td>";
            echo "</tr>";
        }
        echo "<tr><th>Total</th><th>&euro;&nbsp;".number_format($total,2)."</th><th>&nbsp;</th></tr>";
        echo "</table><br>";
    }
}
?>
</div>
